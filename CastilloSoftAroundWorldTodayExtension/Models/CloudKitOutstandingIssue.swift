//
//  CloudKitAccount.swift
//  CastilloSoftAroundWorldTodayExtension
//
//  Created by Israel Gtz on 07/08/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CloudKit
import PromiseKit

class CloudKitOutstandingIssue {
  var outstandingIssueDescription: String?
  var isAlreadyDone: NSNumber?
  //  var audioDescription:
  var dateAlarm: Date?
  var rawDataFromServer: CKRecord?
  
  init(){}
}

extension CloudKitOutstandingIssue {
  
  @objc func value(forKey key: String) -> Any? {
    switch key {
    case "outstandingIssueDescription":
      return self.outstandingIssueDescription
    default:
      return nil
    }
  }
  
}

extension CloudKitModel {
  
  func getAllOutstandingIssuesFromLastWeek() -> Promise<[CloudKitOutstandingIssue]> {
    return Promise { result in
      let today = Date()
      let sevenDaysAgo = Calendar.current.date(byAdding: .day, value: -7, to: today)
      let predicateFromDate = NSPredicate(format: "creationDate > %@ ", sevenDaysAgo! as CVarArg)
      let predicateToDate = NSPredicate(format: "creationDate < %@", today as CVarArg )
      let predicateNotDone = NSPredicate(format: "\(CloudKitKeys.OutstandingIssue.isAlreadyDone.rawValue) == %i", 0)
      let finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateFromDate, predicateToDate, predicateNotDone])
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.OutstandingIssue.outstandingIssueDescription.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.OutstandingIssue.OutstandingIssue.rawValue, sortDescriptors: [sortDescriptor], predicate: finalPredicate).done({ (allOutstandingissues) in
        let arrayOfOutstandingIssues: [CloudKitOutstandingIssue] = allOutstandingissues.map{self.getOutstandingIssueCloudKitData(from: $0)}
        return result.fulfill(arrayOfOutstandingIssues)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  
  func getOutstandingIssueCloudKitData(from ckrecord: CKRecord) -> CloudKitOutstandingIssue {
    let newOutstandingIssue = CloudKitOutstandingIssue()
    newOutstandingIssue.outstandingIssueDescription = (ckrecord[CloudKitKeys.OutstandingIssue.outstandingIssueDescription.rawValue] as? String) ?? ""
    newOutstandingIssue.isAlreadyDone = (ckrecord[CloudKitKeys.OutstandingIssue.isAlreadyDone.rawValue] as? NSNumber) ?? 0
    newOutstandingIssue.dateAlarm = ckrecord[CloudKitKeys.OutstandingIssue.dateAlarm.rawValue] as? Date
    newOutstandingIssue.rawDataFromServer = ckrecord
    return newOutstandingIssue
  }
  
  
  
}
