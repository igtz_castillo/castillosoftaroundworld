//
//  CloudKitModel.swift
//  CastilloSoftAroundWorldTodayExtension
//
//  Created by Israel Gtz on 07/08/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CloudKit
import PromiseKit

class CloudKitModel {
  
  static let sharedInstance = CloudKitModel()
  
  // Represents the default container specified in the iCloud section of the Capabilities tab for the project.
  let container: CKContainer!
  let publicDB: CKDatabase!
  let privateDB: CKDatabase!
  let database = CKContainer(identifier: "iCloud.com.IsraelGutierrez.CastilloSoftAroundWorld").privateCloudDatabase
  
  init() {
    container = CKContainer.default()
    publicDB = container.publicCloudDatabase
    privateDB = container.privateCloudDatabase
  }
  
}

//General functions
extension CloudKitModel {
  
  func fetchRecords(forType type: String, sortDescriptors: [NSSortDescriptor]? = nil, predicate: NSPredicate? = nil) -> Promise<[CKRecord]> {
    return Promise { result in
      var firstLoopRecords = [CKRecord]()
      var query = CKQuery(recordType: type, predicate: NSPredicate(value: true))
      if predicate != nil {
        query = CKQuery(recordType: type, predicate: predicate!)
      }
      query.sortDescriptors = sortDescriptors
      let queryOperation = CKQueryOperation(query: query)
      //queryOperation.zoneID = CloudAssistant.shared.zone.zoneID
      queryOperation.recordFetchedBlock = { record in
        firstLoopRecords.append(record)
      }
      queryOperation.queryCompletionBlock = { cursor, error in
        self.fetchRecords(with: cursor, error: error).done({ (records) in
          let finalRecords = records + firstLoopRecords
          return result.fulfill(finalRecords)
        }).catch({ (error) in
          return result.reject(error)
        })
      }
      database.add(queryOperation)
    }
  }
  
  private func fetchRecords(with cursor: CKQueryCursor?, error: Swift.Error?) -> Promise<[CKRecord]> {
    return Promise { result in
      var currentRecords: Array<CKRecord> = [CKRecord]()
      if let cursor = cursor, error == nil {
        let queryOperation = CKQueryOperation(cursor: cursor)
        queryOperation.recordFetchedBlock = { record in
          currentRecords.append(record)
        }
        queryOperation.queryCompletionBlock = { cursor, error in
          self.fetchRecords(with: cursor, error: error).done({ (records) in
            return result.fulfill(currentRecords + records)
          }).catch({ (errorFetch) in
            return result.reject(errorFetch)
          })
        }
        database.add(queryOperation)
      } else if cursor == nil, error == nil {
        return result.fulfill(currentRecords)
      } else if let error = error {
        return result.reject(error)
      }
    }
  }
  
}
