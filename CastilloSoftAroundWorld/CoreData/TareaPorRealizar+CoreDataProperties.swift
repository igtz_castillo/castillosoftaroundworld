//
//  TareaPorRealizar+CoreDataProperties.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//
//

import Foundation
import CoreData


extension TareaPorRealizar {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TareaPorRealizar> {
        return NSFetchRequest<TareaPorRealizar>(entityName: "TareaPorRealizar")
    }

    @NSManaged public var descripcionTareaPorRealizar: String?
    @NSManaged public var objectIdParse: String?
    @NSManaged public var yaFueRealizado: String?

}
