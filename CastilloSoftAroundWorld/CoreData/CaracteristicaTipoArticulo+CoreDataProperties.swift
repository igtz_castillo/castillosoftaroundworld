//
//  CaracteristicaTipoArticulo+CoreDataProperties.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//
//

import Foundation
import CoreData


extension CaracteristicaTipoArticulo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CaracteristicaTipoArticulo> {
        return NSFetchRequest<CaracteristicaTipoArticulo>(entityName: "CaracteristicaTipoArticulo")
    }

    @NSManaged public var comentarioDeCaracteristicaDeTipoArticulo: String?
    @NSManaged public var nombreDeCaracteristicaDeTipoArticulo: String?
    @NSManaged public var objectIdParse: String?
    @NSManaged public var estaCaracteristicaEsDeEsteTipoArticulo: TipoArticulo?
    @NSManaged public var tieneMuchas: NSOrderedSet?

}

// MARK: Generated accessors for tieneMuchas
extension CaracteristicaTipoArticulo {

    @objc(insertObject:inTieneMuchasAtIndex:)
    @NSManaged public func insertIntoTieneMuchas(_ value: OpcionCaracteristica, at idx: Int)

    @objc(removeObjectFromTieneMuchasAtIndex:)
    @NSManaged public func removeFromTieneMuchas(at idx: Int)

    @objc(insertTieneMuchas:atIndexes:)
    @NSManaged public func insertIntoTieneMuchas(_ values: [OpcionCaracteristica], at indexes: NSIndexSet)

    @objc(removeTieneMuchasAtIndexes:)
    @NSManaged public func removeFromTieneMuchas(at indexes: NSIndexSet)

    @objc(replaceObjectInTieneMuchasAtIndex:withObject:)
    @NSManaged public func replaceTieneMuchas(at idx: Int, with value: OpcionCaracteristica)

    @objc(replaceTieneMuchasAtIndexes:withTieneMuchas:)
    @NSManaged public func replaceTieneMuchas(at indexes: NSIndexSet, with values: [OpcionCaracteristica])

    @objc(addTieneMuchasObject:)
    @NSManaged public func addToTieneMuchas(_ value: OpcionCaracteristica)

    @objc(removeTieneMuchasObject:)
    @NSManaged public func removeFromTieneMuchas(_ value: OpcionCaracteristica)

    @objc(addTieneMuchas:)
    @NSManaged public func addToTieneMuchas(_ values: NSOrderedSet)

    @objc(removeTieneMuchas:)
    @NSManaged public func removeFromTieneMuchas(_ values: NSOrderedSet)

}
