//
//  Articulo+CoreDataProperties.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//
//

import Foundation
import CoreData


extension Articulo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Articulo> {
        return NSFetchRequest<Articulo>(entityName: "Articulo")
    }

    @NSManaged public var costo: String?
    @NSManaged public var detallesPorBuscar: String?
    @NSManaged public var fechaDeCompraDeEsteArticulo: NSDate?
    @NSManaged public var nombreArticulo: String?
    @NSManaged public var nombreDeMarca: String?
    @NSManaged public var objectIdParse: String?
    @NSManaged public var selectedCharacteristics: NSObject?
    @NSManaged public var esteArticuloEsDeUntipoArticulo: TipoArticulo?
    @NSManaged public var esteArticuloEstaCargadoEnLaCuenta: Cuenta?

}
