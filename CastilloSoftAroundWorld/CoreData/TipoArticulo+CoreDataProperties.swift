//
//  TipoArticulo+CoreDataProperties.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//
//

import Foundation
import CoreData


extension TipoArticulo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TipoArticulo> {
        return NSFetchRequest<TipoArticulo>(entityName: "TipoArticulo")
    }

    @NSManaged public var comentarioDeTipoArticulo: String?
    @NSManaged public var nombreDeTipoArticulo: String?
    @NSManaged public var objectIdParse: String?
    @NSManaged public var tieneMuchas: NSOrderedSet?
    @NSManaged public var tieneMuchos: NSOrderedSet?

}

// MARK: Generated accessors for tieneMuchas
extension TipoArticulo {

    @objc(insertObject:inTieneMuchasAtIndex:)
    @NSManaged public func insertIntoTieneMuchas(_ value: CaracteristicaTipoArticulo, at idx: Int)

    @objc(removeObjectFromTieneMuchasAtIndex:)
    @NSManaged public func removeFromTieneMuchas(at idx: Int)

    @objc(insertTieneMuchas:atIndexes:)
    @NSManaged public func insertIntoTieneMuchas(_ values: [CaracteristicaTipoArticulo], at indexes: NSIndexSet)

    @objc(removeTieneMuchasAtIndexes:)
    @NSManaged public func removeFromTieneMuchas(at indexes: NSIndexSet)

    @objc(replaceObjectInTieneMuchasAtIndex:withObject:)
    @NSManaged public func replaceTieneMuchas(at idx: Int, with value: CaracteristicaTipoArticulo)

    @objc(replaceTieneMuchasAtIndexes:withTieneMuchas:)
    @NSManaged public func replaceTieneMuchas(at indexes: NSIndexSet, with values: [CaracteristicaTipoArticulo])

    @objc(addTieneMuchasObject:)
    @NSManaged public func addToTieneMuchas(_ value: CaracteristicaTipoArticulo)

    @objc(removeTieneMuchasObject:)
    @NSManaged public func removeFromTieneMuchas(_ value: CaracteristicaTipoArticulo)

    @objc(addTieneMuchas:)
    @NSManaged public func addToTieneMuchas(_ values: NSOrderedSet)

    @objc(removeTieneMuchas:)
    @NSManaged public func removeFromTieneMuchas(_ values: NSOrderedSet)

}

// MARK: Generated accessors for tieneMuchos
extension TipoArticulo {

    @objc(insertObject:inTieneMuchosAtIndex:)
    @NSManaged public func insertIntoTieneMuchos(_ value: Articulo, at idx: Int)

    @objc(removeObjectFromTieneMuchosAtIndex:)
    @NSManaged public func removeFromTieneMuchos(at idx: Int)

    @objc(insertTieneMuchos:atIndexes:)
    @NSManaged public func insertIntoTieneMuchos(_ values: [Articulo], at indexes: NSIndexSet)

    @objc(removeTieneMuchosAtIndexes:)
    @NSManaged public func removeFromTieneMuchos(at indexes: NSIndexSet)

    @objc(replaceObjectInTieneMuchosAtIndex:withObject:)
    @NSManaged public func replaceTieneMuchos(at idx: Int, with value: Articulo)

    @objc(replaceTieneMuchosAtIndexes:withTieneMuchos:)
    @NSManaged public func replaceTieneMuchos(at indexes: NSIndexSet, with values: [Articulo])

    @objc(addTieneMuchosObject:)
    @NSManaged public func addToTieneMuchos(_ value: Articulo)

    @objc(removeTieneMuchosObject:)
    @NSManaged public func removeFromTieneMuchos(_ value: Articulo)

    @objc(addTieneMuchos:)
    @NSManaged public func addToTieneMuchos(_ values: NSOrderedSet)

    @objc(removeTieneMuchos:)
    @NSManaged public func removeFromTieneMuchos(_ values: NSOrderedSet)

}
