//
//  Cuenta+CoreDataProperties.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//
//

import Foundation
import CoreData


extension Cuenta {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cuenta> {
        return NSFetchRequest<Cuenta>(entityName: "Cuenta")
    }

    @NSManaged public var deudaFinal: String?
    @NSManaged public var nombreDelClienteDeQuienEsCuenta: String?
    @NSManaged public var objectIdParse: String?
    @NSManaged public var cuentaDeEsteCliente: Cliente?
    @NSManaged public var tieneACuentaMuchos: NSOrderedSet?
    @NSManaged public var tieneMuchos: NSOrderedSet?

}

// MARK: Generated accessors for tieneACuentaMuchos
extension Cuenta {

    @objc(insertObject:inTieneACuentaMuchosAtIndex:)
    @NSManaged public func insertIntoTieneACuentaMuchos(_ value: Articulo, at idx: Int)

    @objc(removeObjectFromTieneACuentaMuchosAtIndex:)
    @NSManaged public func removeFromTieneACuentaMuchos(at idx: Int)

    @objc(insertTieneACuentaMuchos:atIndexes:)
    @NSManaged public func insertIntoTieneACuentaMuchos(_ values: [Articulo], at indexes: NSIndexSet)

    @objc(removeTieneACuentaMuchosAtIndexes:)
    @NSManaged public func removeFromTieneACuentaMuchos(at indexes: NSIndexSet)

    @objc(replaceObjectInTieneACuentaMuchosAtIndex:withObject:)
    @NSManaged public func replaceTieneACuentaMuchos(at idx: Int, with value: Articulo)

    @objc(replaceTieneACuentaMuchosAtIndexes:withTieneACuentaMuchos:)
    @NSManaged public func replaceTieneACuentaMuchos(at indexes: NSIndexSet, with values: [Articulo])

    @objc(addTieneACuentaMuchosObject:)
    @NSManaged public func addToTieneACuentaMuchos(_ value: Articulo)

    @objc(removeTieneACuentaMuchosObject:)
    @NSManaged public func removeFromTieneACuentaMuchos(_ value: Articulo)

    @objc(addTieneACuentaMuchos:)
    @NSManaged public func addToTieneACuentaMuchos(_ values: NSOrderedSet)

    @objc(removeTieneACuentaMuchos:)
    @NSManaged public func removeFromTieneACuentaMuchos(_ values: NSOrderedSet)

}

// MARK: Generated accessors for tieneMuchos
extension Cuenta {

    @objc(insertObject:inTieneMuchosAtIndex:)
    @NSManaged public func insertIntoTieneMuchos(_ value: Pago, at idx: Int)

    @objc(removeObjectFromTieneMuchosAtIndex:)
    @NSManaged public func removeFromTieneMuchos(at idx: Int)

    @objc(insertTieneMuchos:atIndexes:)
    @NSManaged public func insertIntoTieneMuchos(_ values: [Pago], at indexes: NSIndexSet)

    @objc(removeTieneMuchosAtIndexes:)
    @NSManaged public func removeFromTieneMuchos(at indexes: NSIndexSet)

    @objc(replaceObjectInTieneMuchosAtIndex:withObject:)
    @NSManaged public func replaceTieneMuchos(at idx: Int, with value: Pago)

    @objc(replaceTieneMuchosAtIndexes:withTieneMuchos:)
    @NSManaged public func replaceTieneMuchos(at indexes: NSIndexSet, with values: [Pago])

    @objc(addTieneMuchosObject:)
    @NSManaged public func addToTieneMuchos(_ value: Pago)

    @objc(removeTieneMuchosObject:)
    @NSManaged public func removeFromTieneMuchos(_ value: Pago)

    @objc(addTieneMuchos:)
    @NSManaged public func addToTieneMuchos(_ values: NSOrderedSet)

    @objc(removeTieneMuchos:)
    @NSManaged public func removeFromTieneMuchos(_ values: NSOrderedSet)

}
