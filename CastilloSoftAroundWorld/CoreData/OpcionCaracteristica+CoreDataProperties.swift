//
//  OpcionCaracteristica+CoreDataProperties.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//
//

import Foundation
import CoreData


extension OpcionCaracteristica {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OpcionCaracteristica> {
        return NSFetchRequest<OpcionCaracteristica>(entityName: "OpcionCaracteristica")
    }

    @NSManaged public var nombreDeOpcionDeCaracteristica: String?
    @NSManaged public var objectIdParse: String?
    @NSManaged public var opcionDeEstaCaracteristicaTipoArticulo: CaracteristicaTipoArticulo?

}
