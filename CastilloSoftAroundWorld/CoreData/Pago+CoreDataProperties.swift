//
//  Pago+CoreDataProperties.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//
//

import Foundation
import CoreData


extension Pago {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pago> {
        return NSFetchRequest<Pago>(entityName: "Pago")
    }

    @NSManaged public var cantidadPagada: String?
    @NSManaged public var comentarioDePago: String?
    @NSManaged public var fechaDePagoRealizado: NSDate?
    @NSManaged public var objectIdParse: String?
    @NSManaged public var estePagoEstaAsignadoAEstaCuenta: Cuenta?

}
