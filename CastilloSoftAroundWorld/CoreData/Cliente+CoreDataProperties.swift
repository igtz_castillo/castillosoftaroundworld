//
//  Cliente+CoreDataProperties.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//
//

import Foundation
import CoreData


extension Cliente {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cliente> {
        return NSFetchRequest<Cliente>(entityName: "Cliente")
    }

    @NSManaged public var apellidos: String?
    @NSManaged public var aunDebe: String?
    @NSManaged public var comentario: String?
    @NSManaged public var diaDeLaSemanaQuePaga: String?
    @NSManaged public var direccion: String?
    @NSManaged public var nombre: String?
    @NSManaged public var numeroCelular: String?
    @NSManaged public var numeroDeBodega: String?
    @NSManaged public var objectIdParse: String?
    @NSManaged public var seFue: String?
    @NSManaged public var siguienteFechaEnQueSeVisitara: String?
    @NSManaged public var telefonoCasa: String?
    @NSManaged public var yaFueVisitadoElDiaDeHoy: String?
    @NSManaged public var tieneMuchas: NSOrderedSet?

}

// MARK: Generated accessors for tieneMuchas
extension Cliente {

    @objc(insertObject:inTieneMuchasAtIndex:)
    @NSManaged public func insertIntoTieneMuchas(_ value: Cuenta, at idx: Int)

    @objc(removeObjectFromTieneMuchasAtIndex:)
    @NSManaged public func removeFromTieneMuchas(at idx: Int)

    @objc(insertTieneMuchas:atIndexes:)
    @NSManaged public func insertIntoTieneMuchas(_ values: [Cuenta], at indexes: NSIndexSet)

    @objc(removeTieneMuchasAtIndexes:)
    @NSManaged public func removeFromTieneMuchas(at indexes: NSIndexSet)

    @objc(replaceObjectInTieneMuchasAtIndex:withObject:)
    @NSManaged public func replaceTieneMuchas(at idx: Int, with value: Cuenta)

    @objc(replaceTieneMuchasAtIndexes:withTieneMuchas:)
    @NSManaged public func replaceTieneMuchas(at indexes: NSIndexSet, with values: [Cuenta])

    @objc(addTieneMuchasObject:)
    @NSManaged public func addToTieneMuchas(_ value: Cuenta)

    @objc(removeTieneMuchasObject:)
    @NSManaged public func removeFromTieneMuchas(_ value: Cuenta)

    @objc(addTieneMuchas:)
    @NSManaged public func addToTieneMuchas(_ values: NSOrderedSet)

    @objc(removeTieneMuchas:)
    @NSManaged public func removeFromTieneMuchas(_ values: NSOrderedSet)

}
