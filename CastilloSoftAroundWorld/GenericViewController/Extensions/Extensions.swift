//
//  Extensions.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 01/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CoreLocation

//MARK: - UIViewController

protocol PresentLoaderLogic: class {
  func showLoader()
  func hideLoader()
}

extension PresentLoaderLogic where Self: UIViewController {
  internal func showLoader() {
     UtilityManager.sharedInstance.showLoader()
  }
  
  internal func hideLoader() {
    UtilityManager.sharedInstance.hideLoader()
  }
}

enum PresentMessageModel {
  enum NormalMessage {
    struct Request {
      var title: String?
      var message: String?
      var image: UIImage?
      var actionsToDoWithTap: ((UIAlertAction)->Void)?
    }
  }
  
  enum ErrorMessage {
    struct Request {
      var error: Error?
      var title: String?
      var message: String?
      var image: UIImage?
      var actionsToDoWithTap: ((UIAlertAction)->Void)?
    }
  }
}

protocol PresentErrorLogic: class {
  func displayMessageError(request: PresentMessageModel.ErrorMessage.Request)
}

extension PresentErrorLogic where Self: UIViewController {
  internal func displayMessageError(request: PresentMessageModel.ErrorMessage.Request) {
    print(request.error ?? "No description error")
    let alert = UIAlertController(title: request.title ?? "", message: request.message ?? "", preferredStyle: UIAlertControllerStyle.alert)
    if let image = request.image {
      let scaleSze = CGSize(width: 245, height: 245/image.size.width*image.size.height)
      let rect = CGRect(x: 0.0, y: 0.0, width: scaleSze.width, height: scaleSze.height)
      UIGraphicsBeginImageContextWithOptions(scaleSze, false, 1.0)
      image.draw(in: rect)
      let newImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      let imageAlertAction = UIAlertAction(title: "Ok", style: .default, handler: request.actionsToDoWithTap)
      imageAlertAction.setValue(newImage!.withRenderingMode(.alwaysOriginal), forKey: "image")
      alert.addAction(imageAlertAction)
    } else {
      let action = UIAlertAction(title: "Ok", style: .default, handler: request.actionsToDoWithTap)
      alert.addAction(action)
    }
    self.present(alert, animated: true, completion: nil)
  }
}

protocol PresentMessageLogic: class {
  func displayMessage(request: PresentMessageModel.NormalMessage.Request)
}

extension PresentMessageLogic where Self: UIViewController {
  internal func displayMessage(request: PresentMessageModel.NormalMessage.Request) {
    let alert = UIAlertController(title: request.title ?? "", message: request.message ?? "", preferredStyle: UIAlertControllerStyle.alert)
    if let image = request.image {
      let scaleSze = CGSize(width: 245, height: 245/image.size.width*image.size.height)
      let rect = CGRect(x: 0.0, y: 0.0, width: scaleSze.width, height: scaleSze.height)
      UIGraphicsBeginImageContextWithOptions(scaleSze, false, 1.0)
      image.draw(in: rect)
      let newImage = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      let imageAlertAction = UIAlertAction(title: "Ok", style: .default, handler: request.actionsToDoWithTap)
      imageAlertAction.setValue(newImage!.withRenderingMode(.alwaysOriginal), forKey: "image")
      alert.addAction(imageAlertAction)
    } else {
      let action = UIAlertAction(title: "Ok", style: .default, handler: request.actionsToDoWithTap)
      alert.addAction(action)
    }
    self.present(alert, animated: true, completion: nil)
  }
}

//MARK: - Date

extension Date {
  
  func getZeroDay() -> Date {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-mm-yyyy"
    dateFormatter.timeZone = TimeZone(abbreviation: "GMT-5:00")
    let date = dateFormatter.date(from: "01-01-0001")
    return date!
  }
  
  func getStringFromDate(dateStyle: DateFormatter.Style = .full) -> String {
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "es_MX")
    formatter.dateStyle = dateStyle
    return formatter.string(from: self)
  }
  
  func getDateFromString(stringDate: String, dateStyle: DateFormatter.Style)  -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "es_MX")
    dateFormatter.dateStyle = dateStyle
    return dateFormatter.date(from: stringDate)
  }
  
  func howManyDaysFromThisDate(lastDateOfPayment: Date) -> Int {
    let calendar = NSCalendar.current
    let date1 = calendar.startOfDay(for: lastDateOfPayment)
    let components = calendar.dateComponents([.day], from: date1, to: self)
    return components.day ?? -1
  }

  static func today() -> Date {
    return Date()
  }
  
  func getTomorrow() -> Date {
    return Calendar.current.date(byAdding: .day, value: 1, to: self)!
  }
  
  func getYesterday() -> Date {
    return Calendar.current.date(byAdding: .day, value: -1, to: self)!
  }
  
  func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
    return get(.Next,
               weekday,
               considerToday: considerToday)
  }
  
  func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
    return get(.Previous,
               weekday,
               considerToday: considerToday)
  }
  
  func getTodaysName() -> String {
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "EEEE"
    return formatter.string(from: date)
  }
  
  func get(_ direction: SearchDirection, _ weekDay: Weekday, considerToday consider: Bool = false) -> Date {
    let dayName = weekDay.rawValue
    let weekdaysName = getWeekDaysInSpanish().map { $0.lowercased() }
    assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
    let searchWeekdayIndex = weekdaysName.index(of: dayName)! + 1
    let calendar = Calendar(identifier: .gregorian)
    if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
      return self
    }
    var nextDateComponent = DateComponents()
    nextDateComponent.weekday = searchWeekdayIndex
    let date = calendar.nextDate(after: self,
                                 matching: nextDateComponent,
                                 matchingPolicy: .nextTime,
                                 direction: direction.calendarSearchDirection)
    return date!
  }
  
  func getWeekDaysInSpanish() -> [String] {
    var calendar = Calendar(identifier: .gregorian)
    calendar.locale = Locale(identifier: "es_MX")
    return calendar.weekdaySymbols
  }
  
  enum Weekday: String {
    case lunes, martes, miércoles, jueves, viernes, sábado, domingo
  }
  
  enum SearchDirection {
    case Next
    case Previous
    
    var calendarSearchDirection: Calendar.SearchDirection {
      switch self {
      case .Next:
        return .forward
      case .Previous:
        return .backward
      }
    }
  }
  
}

extension UIImage {
  func resizeWithPercent(percentage: CGFloat) -> UIImage? {
    let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
    imageView.contentMode = .scaleAspectFit
    imageView.image = self
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
    guard let context = UIGraphicsGetCurrentContext() else { return nil }
    imageView.layer.render(in: context)
    guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
    UIGraphicsEndImageContext()
    return result
  }
}

extension Notification.Name {
  static let databaseSelectedChanged = Notification.Name("databaseSelectedChanged")
}
