//
//  OptionNavigationController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 24/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import UIKit
import NotificationCenter

class OptionNavigationController {
  
  static let sharedInstance = OptionNavigationController()
//  let createClientNavigation: UINavigationController! = UINavigationController.init(rootViewController: UIStoryboard.init(name: "ClientInfo", bundle: nil).instantiateViewController(withIdentifier: "ClientInfoViewController") as! ClientInfoViewController)
  let clientNavigation: UINavigationController! = UINavigationController.init(rootViewController: UIStoryboard.init(name: "FindingClients", bundle: nil).instantiateViewController(withIdentifier: "FindingClientsViewController") as! FindingClientsViewController)
  let articlesNavigation: UINavigationController! = UINavigationController.init(rootViewController: UIStoryboard.init(name: "ArticleTypeList", bundle: nil).instantiateViewController(withIdentifier: "ArticleTypeListViewController") as! ArticleTypeListViewController)
  let addNewArticleNavigation: UINavigationController! = UINavigationController.init(rootViewController: UIStoryboard.init(name: "CreateNewArticle", bundle: nil).instantiateViewController(withIdentifier: "CreateNewArticleViewController") as! CreateNewArticleViewController)
  let customersToVisitNavigation: UINavigationController! = UINavigationController.init()
  let outstandingIssuesNavigation: UINavigationController! = UINavigationController.init(rootViewController: UIStoryboard.init(name: "OutstandingIssueList", bundle: nil).instantiateViewController(withIdentifier: "OutstandingIssueListViewController") as! OutstandingIssueListViewController)
  let managementNavigation: UINavigationController! = UINavigationController.init(rootViewController: UIStoryboard.init(name: "Management", bundle: nil).instantiateViewController(withIdentifier: "ManagementViewController") as! ManagementViewController)
  let clientMapNavigation: UINavigationController! = UINavigationController.init(rootViewController: UIStoryboard.init(name: "ClientMap", bundle: nil).instantiateViewController(withIdentifier: "ClientMapViewController") as! ClientMapViewController)
  let optionsAppNavigation: UINavigationController! = UINavigationController.init(rootViewController: UIStoryboard.init(name: "OptionsApp", bundle: nil).instantiateViewController(withIdentifier: "OptionsAppViewController") as! OptionsAppViewController)
  
  init() {
    self.addToNotificationCenter()
  }
  
  private func addToNotificationCenter() {
    NotificationCenter.default.addObserver(self, selector: #selector(databaseSelectedChanged(_:)), name: .databaseSelectedChanged , object: nil)
  }

  @objc private func databaseSelectedChanged(_ notification: Notification) {
    self.cleanManagementNavigation(notification)
    self.cleanClientNavigation(notification)
  }
  
  private func cleanManagementNavigation(_ notification: Notification) {
    self.managementNavigation.popToRootViewController(animated: false)
    if let managementVC = self.managementNavigation.viewControllers.first! as? ManagementViewController {
      managementVC.interactor?.databaseSelected(request: Management.DatabaseSelectedChanged.Request(notification: notification))
    }
  }
  
  private func cleanClientNavigation(_ notification: Notification) {
    self.clientNavigation.popToRootViewController(animated: false)
    if let findingClientsVC = self.clientNavigation.viewControllers.first! as? FindingClientsViewController {
      findingClientsVC.interactor?.cleanViewController()
    }
  }
  
}
