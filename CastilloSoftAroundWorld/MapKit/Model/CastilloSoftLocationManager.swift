//
//  CastilloSoftLocationManager.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 31/07/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CoreLocation
import UserNotifications

class CastilloSoftLocationManager: NSObject {
  
  static let sharedInstance = CastilloSoftLocationManager(delegate: UIApplication.shared.delegate as! AppDelegate)
  
  var locationManager: CLLocationManager! = CLLocationManager()
  
  override init() {
    super.init()
  }
  
  init(delegate: CLLocationManagerDelegate) {
    super.init()
    self.initLocationManager(delegate: delegate)
  }
  
  private func initLocationManager(delegate: CLLocationManagerDelegate) {
    self.locationManager!.requestWhenInUseAuthorization()
    self.locationManager!.delegate = delegate
    self.locationManager!.allowsBackgroundLocationUpdates = true
    let centerOfHomeRegion = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: 25.804841)!, longitude: CLLocationDegrees(exactly: -100.284692)!)
    let homeCircularRegion = CLCircularRegion(center: centerOfHomeRegion, radius: 20, identifier: "Home")
    let centerOfJobRegion = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: 25.71359)!, longitude: CLLocationDegrees(exactly: -100.292779)!)
    let jobCircularRegion = CLCircularRegion(center: centerOfJobRegion, radius: 300, identifier: "Job")
    self.locationManager!.startMonitoring(for: homeCircularRegion)
    self.locationManager.startMonitoring(for: jobCircularRegion)
  }
  
  func createLocalNotificationToShow(title: String, subtitle: String) {
    let calendar = Calendar.current
    let date = calendar.date(byAdding: .second, value: 2, to: Date.init())
    let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date!)
    let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
    let identifier = "\(title)_\(subtitle)"
    let content = UNMutableNotificationContent()
    content.title = title
    content.subtitle = subtitle
    content.categoryIdentifier = identifier
    content.sound = UNNotificationSound(named: "grito_chiva.caf")
    let notificationRequest = UNNotificationRequest(
      identifier: identifier,
      content: content,
      trigger: trigger
    )
    UNUserNotificationCenter.current().add(notificationRequest, withCompletionHandler: nil)
  }
  
}
