//
//  ClientMKAnnotation.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 26/07/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

class ClientMKAnnotation: NSObject, MKAnnotation {
  var clientData: CloudKitClient
  var coordinate: CLLocationCoordinate2D { return getCLLocationCoordinate2D() }
  
  init(client: CloudKitClient) {
    self.clientData = client
    super.init()
  }
  
  var title: String? {
    return self.clientData.clientName
  }
 
  var subtitle: String? {
    return self.clientData.wareHouseNumber
  }
  
  private func getCLLocationCoordinate2D() -> CLLocationCoordinate2D {
    if self.clientData.location != nil {
      return self.clientData.location!.coordinate
    }
    return CLLocationCoordinate2D(latitude: CLLocationDegrees.init(0.0), longitude: CLLocationDegrees.init(0.0))
  }
}
