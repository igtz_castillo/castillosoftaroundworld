//
//  AppDelegate.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 02/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import CoreData
import UIKit
import UserNotifications
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
  let locationManager = CLLocationManager()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      self.preloadDBData()
      self.setupLocalNotifications()
      self.initCastilloSoftLocationMamager()
      
      if launchOptions?[UIApplicationLaunchOptionsKey.location] != nil {
        if let _ = launchOptions![UIApplicationLaunchOptionsKey.location] as? CLLocationManager {
          
        }
      }
      
      return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
  
  func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
    if launchOptions?[UIApplicationLaunchOptionsKey.location] != nil {
      if let _ = launchOptions![UIApplicationLaunchOptionsKey.location] as? CLLocationManager {
    
      }
    }
    
    return true
  }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "CoreDataParseManager")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func preloadDBData() {
        let sqlitePath = Bundle.main.path(forResource: "CoreDataParseManager", ofType: "sqlite")
        let sqlitePath_shm = Bundle.main.path(forResource: "CoreDataParseManager", ofType: "sqlite-shm")
        let sqlitePath_wal = Bundle.main.path(forResource: "CoreDataParseManager", ofType: "sqlite-wal")
        
        let URL1 = URL(fileURLWithPath: sqlitePath!)
        let URL2 = URL(fileURLWithPath: sqlitePath_shm!)
        let URL3 = URL(fileURLWithPath: sqlitePath_wal!)
        let URL4 = URL(fileURLWithPath: NSPersistentContainer.defaultDirectoryURL().relativePath + "/CoreDataParseManager.sqlite")
        let URL5 = URL(fileURLWithPath: NSPersistentContainer.defaultDirectoryURL().relativePath + "/CoreDataParseManager.sqlite-shm")
        let URL6 = URL(fileURLWithPath: NSPersistentContainer.defaultDirectoryURL().relativePath + "/CoreDataParseManager.sqlite-wal")
        
        if !FileManager.default.fileExists(atPath: NSPersistentContainer.defaultDirectoryURL().relativePath + "/CoreDataParseManager.sqlite") {
            // Copy 3 files
            do {
                try FileManager.default.copyItem(at: URL1, to: URL4)
                try FileManager.default.copyItem(at: URL2, to: URL5)
                try FileManager.default.copyItem(at: URL3, to: URL6)
                
                print("=======================")
                print("FILES COPIED")
                print("=======================")
                
            } catch {
                print("=======================")
                print("ERROR IN COPY OPERATION")
                print("=======================")
            }
        } else {
            print("=======================")
            print("FILES EXIST")
            print("=======================")
        }
    }
  
  func setupLocalNotifications() {
    let centerNotification = UNUserNotificationCenter.current()
    centerNotification.delegate = self
    centerNotification.requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
      if !success {
        let alert = UIAlertController(title: "Aviso...", message: "Recuerda que puedes dar permiso al uso de notificaciones desde la configuración del sistema.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
      }
    }
  }

  //MARK: - UserNotidicationDelegate
  
  func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    completionHandler()
  }
  
  func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    completionHandler([.alert, .badge, .sound])
  }
  
}

extension AppDelegate: CLLocationManagerDelegate {
  func initCastilloSoftLocationMamager() {
    CastilloSoftLocationManager.sharedInstance.locationManager.showsBackgroundLocationIndicator = true
  }
  
  func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
    switch region.identifier {
    case "Home":
      CastilloSoftLocationManager.sharedInstance.createLocalNotificationToShow(title: "Que tengas buen día", subtitle: "Échale ganas, ¡Tú puedes!")
    case "Job":
      CastilloSoftLocationManager.sharedInstance.createLocalNotificationToShow(title: "Recuerda tus pendientes", subtitle: "No se te olvide algo por hacer")
    default:
      print()
    }
  }
  
  func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
    switch region.identifier {
    case "Home":
      CastilloSoftLocationManager.sharedInstance.createLocalNotificationToShow(title: "Bienvenida a casa", subtitle: "Mereces un descanso")
    case "Job":
      CastilloSoftLocationManager.sharedInstance.createLocalNotificationToShow(title: "¡Ánimo!", subtitle: "El trabajo duro siempre da buenos resultados")
    default:
      print()
    }
  }
}

