//
//  CastilloTextField.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 14/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class CastilloTextField: UITextField {
  
  var obligatoryField: Bool = false {
    didSet {
      initObligatoryActions()
    }
  }
  
  var justNumericData: Bool = false {
    didSet {
      initObligatoryActions()
      initJustNumericData()
    }
  }
  
  var hasValidInformation: Bool {
    return thisTextFieldHasValidInformation()
  }
  
  override var isEnabled: Bool {
    willSet {
      textColor = newValue ? UIColor.black : UIColor.lightGray
    }
  }

  private func initObligatoryActions() {
    if self.obligatoryField {
      if justNumericData {
        self.changeAppearanceOfPlaceHolderForObligatoryValue(with: "Campo obligatorio y numérico")
      } else {
        self.changeAppearanceOfPlaceHolderForObligatoryValue(with: "Campo obligatorio")
      }
      self.initListener()
    } else {
      self.changeAppearanceOfPlaceHolderForNotObligatoryValue()
      self.removeListener()
    }
  }
  
  private func initJustNumericData() {
    if self.justNumericData {
      self.keyboardType = .decimalPad
    }
  }
  
  private func initListener() {
    self.addTarget(self, action: #selector(valueDidChange(_:)), for: UIControlEvents.editingChanged)
  }
  
  private func removeListener() {
    self.removeTarget(self, action: #selector(valueDidChange(_:)), for: UIControlEvents.editingChanged)
  }
  
  private func changeAppearanceOfPlaceHolderForObligatoryValue(with message: String) {
    self.attributedPlaceholder = NSAttributedString(string: message,
                                                attributes: [NSAttributedStringKey.foregroundColor: UIColor.init(red: 1.0, green: 0, blue: 0, alpha: 0.4)])
  }
  
  private func changeAppearanceOfPlaceHolderForNotObligatoryValue() {
    self.attributedPlaceholder = NSAttributedString(string: "",
                                                attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
  }

  @objc private func valueDidChange(_ textField: UITextField) {
    if !UtilityManager.sharedInstance.isValidText(testString: textField.text!) {
      self.changeAppearanceOfPlaceHolderForObligatoryValue(with:"Campo obligatorio")
    }
    if self.justNumericData {
      if !UtilityManager.sharedInstance.isValidNumeric(string: textField.text!) {
        self.changeAppearanceOfPlaceHolderForObligatoryValue(with:"Campo obligatorio y numérico")
      }
    }
  }
  
  private func thisTextFieldHasValidInformation() -> Bool {
    if obligatoryField {
      if self.justNumericData {
        return UtilityManager.sharedInstance.isValidNumeric(string: self.text!)
      }
      return UtilityManager.sharedInstance.isValidText(testString: self.text!)
    } else {
      return true
    }
  }
  
}
