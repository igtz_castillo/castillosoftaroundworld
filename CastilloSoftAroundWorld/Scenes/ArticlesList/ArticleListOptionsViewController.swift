//
//  ArticleListOptionsViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 29/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class ArticleListOptionsViewController: UIViewController {
  
  var interactor: (ArticleListBusinessLogic & ArticleListDataStore & ArticleTypeListViewControllerDelegate)?
  @IBOutlet weak var editArticleTypeButton: UIButton!
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.changeEditArticleTypeButtonText()
  }
  
  private func changeEditArticleTypeButtonText() {
    let nameButton: String = "Editar tipo de artículo: \(self.interactor?.lastArticleTypeSelected?.articleTypeName ?? "")"
    self.editArticleTypeButton.setTitle(nameButton, for: .normal)
    self.editArticleTypeButton.sizeToFit()
  }
  
  @IBAction func createNewArticleButtonPressed(_ sender: Any) {
    self.interactor?.hideRightRevealViewController()
    self.interactor?.presentCreateNewArticleViewController()
  }
  
  @IBAction func editArticleTypeButtonPressed(_ sender: Any) {
    self.interactor?.hideRightRevealViewController()
    self.interactor?.presentEditArticleTypeViewController()
  }
  
}
