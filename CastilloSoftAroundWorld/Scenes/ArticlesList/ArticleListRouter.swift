//
//  ArticleListRouter.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 22/05/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ArticleListRoutingLogic {
  func popMyself()
  func routeTo(viewModel: CreateNewArticle.DisplayCreateNewArticleViewController.ViewModel)
  func routeToEditArticleType(viewModel: ArticleListOptions.EditArticleType.ViewModel)
}

protocol ArticleListDataPassing {
  var dataStore: ArticleListDataStore? { get }
}

class ArticleListRouter: NSObject, ArticleListRoutingLogic, ArticleListDataPassing {
  weak var viewController: ArticleListViewController?
  var dataStore: ArticleListDataStore?
 
  func popMyself() {
    self.viewController?.navigationController?.popViewController(animated: true)
  }
  
  func routeTo(viewModel: CreateNewArticle.DisplayCreateNewArticleViewController.ViewModel) {
    self.viewController?.navigationController?.pushViewController(viewModel.createNewArticleVC, animated: true)
  }
  
  func routeToEditArticleType(viewModel: ArticleListOptions.EditArticleType.ViewModel) {
    self.viewController?.navigationController?.pushViewController(viewModel.editArticleTypeVC, animated: true)
  }
  
}
