//
//  ArticleListViewCell.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 25/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class ArticleListViewCell: UITableViewCell {
  
  private var articleData: CloudKitArticle = CloudKitArticle.init()
  @IBOutlet weak var articleNameLabel: UILabel!
  @IBOutlet weak var articlePriceLabel: UILabel!
  @IBOutlet weak var articleBrandLabel: UILabel!
  
  func updateInfo(newArticleData: CloudKitArticle) {
    self.articleData = newArticleData
    self.articleNameLabel.text = self.articleData.nameArticle
    self.articlePriceLabel.text = self.articleData.cost
    self.articleBrandLabel.text = self.articleData.brandName
  }
  
}
