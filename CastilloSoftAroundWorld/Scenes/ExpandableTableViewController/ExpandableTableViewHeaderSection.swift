//
//  ExpandableTableViewHeaderSection.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 20/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

protocol ExpandableTableViewHeaderSectionDelegate {
    func headerPressed(header: ExpandableTableViewHeaderSection)
}

class ExpandableTableViewHeaderSection: UITableViewCell {
    
  var delegate: ExpandableTableViewHeaderSectionDelegate?
  var isExpanded: Bool = false
  
  @IBOutlet weak var labelTitle: UILabel!

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    self.addTapGesture()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.addTapGesture()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  private func addTapGesture() {
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(headerTapped))
    tapGesture.numberOfTapsRequired = 1
    self.addGestureRecognizer(tapGesture)
  }
    
  func changeTitle(newTitle: String) {
      self.labelTitle.text = newTitle
  }
  
  @objc private func headerTapped() {
    self.delegate?.headerPressed(header: self)
  }
    
    
  
}
