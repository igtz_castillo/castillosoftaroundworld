//
//  ExpandableTableViewCell.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 19/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class ExpandableTableViewCell: UITableViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  
  func changeTitle(_ newTitle: String) {
    titleLabel.text = newTitle
  }
  
}
