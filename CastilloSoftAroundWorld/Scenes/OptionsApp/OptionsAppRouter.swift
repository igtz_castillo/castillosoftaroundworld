//
//  OptionsAppRouter.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 14/08/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol OptionsAppRoutingLogic {
  
}

protocol OptionsAppDataPassing {
  var dataStore: OptionsAppDataStore? { get }
}

class OptionsAppRouter: NSObject, OptionsAppRoutingLogic, OptionsAppDataPassing {
  weak var viewController: OptionsAppViewController?
  var dataStore: OptionsAppDataStore?
  
  
}
