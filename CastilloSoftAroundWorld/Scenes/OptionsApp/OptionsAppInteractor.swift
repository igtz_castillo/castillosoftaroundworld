//
//  OptionsAppInteractor.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 14/08/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol OptionsAppBusinessLogic {
  func setSwitches(request: OptionsApp.SetSwitches.Request)
  func privateDatabaseSwitchChanged(request: OptionsApp.SwitchChanged.Request)
  func publicDatabaseSwitchChanged(request: OptionsApp.SwitchChanged.Request)
}

protocol OptionsAppDataStore {
  
}

class OptionsAppInteractor: OptionsAppBusinessLogic, OptionsAppDataStore {
  var presenter: OptionsAppPresentationLogic?
  var worker: OptionsAppWorker? = OptionsAppWorker()
  
  func setSwitches(request: OptionsApp.SetSwitches.Request) {
    self.worker?.setSwitches(request: request)
  }
  
  func privateDatabaseSwitchChanged(request: OptionsApp.SwitchChanged.Request) {
    self.worker?.privateDatabaseSwitchChanged(request: request)
  }
  
  func publicDatabaseSwitchChanged(request: OptionsApp.SwitchChanged.Request) {
    self.worker?.publicDatabaseSwitchChanged(request: request)
  }
  
}
