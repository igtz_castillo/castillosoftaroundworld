//
//  CreateNewArticleViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 30/05/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol CreateNewArticleDisplayLogic: class {
  func displayDisplayAllCharacteristics(response: CreateNewArticle.PresentAllCharacteristicsAndOptions.Response)
  func displayAllOptionesOfLabelSelected(viewModel: CreateNewArticle.PresentAllCharacteristicsAndOptions.ViewModel)
  func displayCleaningScreenForNewArticle()
  func displayHiddingOptionsViewController()
  func displayShowCreateArticleButton(response: CreateNewArticle.ShowCreateButton.Response)
  func displayPopMyself()
}

class CreateNewArticleViewController: UIViewController, CreateNewArticleDisplayLogic, PresentErrorLogic, PresentMessageLogic, PresentLoaderLogic {
  var interactor: (CreateNewArticleBusinessLogic & CreateNewArticleDataStore & CharacteristicOptionViewDelegate)?
  var router: (NSObjectProtocol & CreateNewArticleRoutingLogic & CreateNewArticleDataPassing)?
  
  @IBOutlet weak var contentViewOfScrollView: UIView!
  @IBOutlet weak var articleNameTextField: CastilloTextField!
  @IBOutlet weak var brandNameTextField: CastilloTextField!
  @IBOutlet weak var articleCostTextField: CastilloTextField!
  @IBOutlet weak var articleDetailsTextField: CastilloTextField!
  @IBOutlet weak var numberOfArticlesToCreateTextField: CastilloTextField!
  @IBOutlet weak var menuButton: UIBarButtonItem!
  @IBOutlet weak var optionsButton: UIBarButtonItem!
  @IBOutlet weak var createButton: UIButton!
  @IBOutlet weak var stackViewOfButtons: UIStackView!
  private var lastConstraint: NSLayoutConstraint?
    
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  private func setup() {
    let viewController = self
    let interactor = CreateNewArticleInteractor()
    let presenter = CreateNewArticlePresenter()
    let router = CreateNewArticleRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    self.sideMenu()
    self.nameViewController()
    self.setUnableTextFields()
    self.initFields()
    self.modifyCreateButtonAppearance()
    self.interactor?.loadArticleTypeInfoToCreateNewArticle()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.sideMenu()
  }
    
  private func sideMenu() {
    if revealViewController() != nil {
      menuButton.target = revealViewController()
      menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
      revealViewController().rearViewRevealWidth = UIScreen.main.bounds.size.width / 2.0
//      revealViewController().rightViewRevealWidth = UIScreen.main.bounds.size.width / 2.0
//      let storyboard = UIStoryboard(name: "CreateNewArticle", bundle: nil)
//      let optionsCreateNewArticleVC = storyboard.instantiateViewController(withIdentifier: "CreateNewArticleOptionsViewController") as! CreateNewArticleOptionsViewController
//      optionsCreateNewArticleVC.interactor = self.interactor
      revealViewController().rightViewController = nil
//      optionsButton.target = revealViewController()
//      optionsButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
  }
  
  private func nameViewController() {
    self.title = "Creación de artículo \(self.interactor?.articleTypeSelected?.articleTypeName ?? "nuevo")"
  }
  
  private func setUnableTextFields() {
    self.articleNameTextField.isEnabled = false
    self.brandNameTextField.isEnabled = false
    self.articleCostTextField.isEnabled = false
    self.articleDetailsTextField.isEnabled = false
    self.numberOfArticlesToCreateTextField.isEnabled = false
  }
  
  private func setEnableTextFields() {
    self.articleNameTextField.isEnabled = true
    self.brandNameTextField.isEnabled = true
    self.articleCostTextField.isEnabled = true
    self.articleDetailsTextField.isEnabled = true
    self.numberOfArticlesToCreateTextField.isEnabled = true
  }
  
  private func initFields() {
    //mandatory and type of data
    self.articleNameTextField.obligatoryField = true
    self.articleCostTextField.obligatoryField = true
    //
    self.articleCostTextField.justNumericData = true
    //
    self.numberOfArticlesToCreateTextField.obligatoryField = true
    self.numberOfArticlesToCreateTextField.justNumericData = true
    
    //delegates
    self.articleNameTextField.delegate = self
    self.brandNameTextField.delegate = self
    self.articleCostTextField.delegate = self
    self.articleDetailsTextField.delegate = self
    self.numberOfArticlesToCreateTextField.delegate = self
  }
  
  private func modifyCreateButtonAppearance() {
    self.createButton.isHidden = true
  }
  
  @IBAction func createButtonPressed(_ sender: Any) {
    self.interactor?.articleNameTextField = self.articleNameTextField
    self.interactor?.brandNameTextField = self.brandNameTextField
    self.interactor?.articleCostTextField = self.articleCostTextField
    self.interactor?.articleDetailsTextField = self.articleDetailsTextField
    self.interactor?.numberOfArticlesToCreateTextField = self.numberOfArticlesToCreateTextField
    self.interactor?.createNewArticle()
  }
    
  @IBAction func cancelButtonPressed(_ sender: Any) {
    self.router?.routeToPopMyself()
  }
    
}

//MARK: - functions from Presenter

extension CreateNewArticleViewController {
  
  func displayHiddingOptionsViewController() {
    if self.revealViewController() != nil {
      revealViewController().rightRevealToggle(animated: true)
    }
  }
  
  func displayDisplayAllCharacteristics(response: CreateNewArticle.PresentAllCharacteristicsAndOptions.Response) {
    self.setEnableTextFields()
    var arrayOfConstraints: [NSLayoutConstraint] = Array()
    for characteristic in response.allCharactristicsWithOptions {
      let lastView: UIView = self.interactor?.arrayOfCharacteristics.last ?? self.numberOfArticlesToCreateTextField
      let newCharacteristicView = CharacteristicWithOptionLabelView.initView()
      newCharacteristicView.translatesAutoresizingMaskIntoConstraints = false
      self.contentViewOfScrollView.addSubview(newCharacteristicView)
      self.interactor?.arrayOfCharacteristics.append(newCharacteristicView)
      let firstConstraint = newCharacteristicView.leadingAnchor.constraint(equalTo: self.contentViewOfScrollView.leadingAnchor, constant: 0.0)
      firstConstraint.isActive = true
      let secondConstraint = newCharacteristicView.trailingAnchor.constraint(equalTo: self.contentViewOfScrollView.trailingAnchor, constant: 0.0)
      secondConstraint.isActive = true
      let thirdConstraint = newCharacteristicView.topAnchor.constraint(equalTo: lastView.topAnchor, constant: 85.0)
      thirdConstraint.isActive = true
      arrayOfConstraints = arrayOfConstraints + [firstConstraint, secondConstraint, thirdConstraint]
      newCharacteristicView.changeCharacteristicData(newData: characteristic)
      newCharacteristicView.optionNameLabel.delegate = self.interactor
    }
    if lastConstraint != nil {
      lastConstraint?.isActive = false
      self.contentViewOfScrollView.removeConstraint(lastConstraint!)
    }
    let lastView: UIView = self.interactor?.arrayOfCharacteristics.last ?? self.numberOfArticlesToCreateTextField
    lastConstraint = lastView.bottomAnchor.constraint(equalTo: self.stackViewOfButtons.bottomAnchor, constant: -70.0)
    lastConstraint!.isActive = true
    arrayOfConstraints.append(lastConstraint!)
    self.contentViewOfScrollView.addConstraints(arrayOfConstraints)
  }
  
  func displayCleaningScreenForNewArticle() {
    self.setUnableTextFields()
    self.displayShowCreateArticleButton(response: CreateNewArticle.ShowCreateButton.Response(isHidden: true))
    self.articleNameTextField.text = ""
    self.brandNameTextField.text = ""
    self.articleCostTextField.text = ""
    self.articleDetailsTextField.text = ""
    self.numberOfArticlesToCreateTextField.text = ""
  }
  
  func displayAllOptionesOfLabelSelected(viewModel: CreateNewArticle.PresentAllCharacteristicsAndOptions.ViewModel) {
    let alertController = UIAlertController(title: "Elige una opción", message: nil, preferredStyle: .actionSheet)
    for option in viewModel.selectedOptionLabel.options {
      let action = UIAlertAction.init(title: option.articleTypeCharacteristicOptionName, style: .default) { (alertAction) in
        viewModel.selectedOptionLabel.optionSelected = option
        viewModel.selectedOptionLabel.changeTextToShowWithThisOptionSelected(newOptionSelected: option)
        self.validateInfoToShowCreateButton()
      }
      alertController.addAction(action)
    }
    if let popoverController = alertController.popoverPresentationController {
      popoverController.permittedArrowDirections = .init(rawValue: 0)
      popoverController.sourceView = viewModel.selectedOptionLabel
      let rect = CGRect.init(x: viewModel.selectedOptionLabel.frame.origin.x - 100.0,
                             y: viewModel.selectedOptionLabel.frame.origin.y + 0.0,
                             width: viewModel.selectedOptionLabel.frame.size.width,
                             height: viewModel.selectedOptionLabel.frame.size.height)
      popoverController.sourceRect = rect
    }
    self.present(alertController, animated: true, completion: nil)
  }
  
  func displayShowCreateArticleButton(response: CreateNewArticle.ShowCreateButton.Response) {
    self.createButton.isHidden = response.isHidden
  }
  
  func displayPopMyself() {
    self.router?.routeToPopMyself()
  }
  
  private func validateInfoToShowCreateButton() {
    let textFieldArray: [CastilloTextField] = [
      self.articleNameTextField,
      self.brandNameTextField,
      self.articleCostTextField,
      self.articleDetailsTextField,
      self.numberOfArticlesToCreateTextField
    ]
    let request = CreateNewArticle.ShowCreateButton.Request(arrayOfTextFields: textFieldArray, characteristicOption: self.interactor?.arrayOfCharacteristics ?? Array())
    self.interactor?.valiadteInformationToShowCreateProductButton(request: request)
  }
  
}

extension CreateNewArticleViewController: UITextFieldDelegate {
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    self.validateInfoToShowCreateButton()
  }
  
}
