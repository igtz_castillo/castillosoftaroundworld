//
//  CharacteristicWithOptionLabelView.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 30/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

protocol CharacteristicWithOptionLabelViewDelegate {
  func justCharacteristicTapped(sender: CharacteristicWithOptionLabelView)
}

class CharacteristicWithOptionLabelView: UIView {
  
  private var characteristicData: CloudKitArticleTypeCharacteristic = CloudKitArticleTypeCharacteristic()
  var characteristicStringValue: String {
    get {
      return valueOfCharacteristic()
    }
  }
  
  @IBOutlet weak var characteristicNameLabel: UILabel!
  @IBOutlet weak var optionNameLabel: CharacteristicOptionView!
  @IBOutlet weak var optionTextField: UITextField!
  var usingForEditingCharacteristic: Bool = false
  var characteristicLabelDelegate: CharacteristicWithOptionLabelViewDelegate?
  
//  override init(frame: CGRect) {
//    super.init(frame: frame)
//    self.initInterface()
//  }
//  
//  required init?(coder aDecoder: NSCoder) {
//    super.init(coder: aDecoder)
//    self.initInterface()
//  }
  
  class func initView() -> CharacteristicWithOptionLabelView {
    let myClassNib = UINib.init(nibName: "CharacteristicView", bundle: nil)
    return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! CharacteristicWithOptionLabelView
  }
  
  func initTapGestureJustForCharacteristicWhenEditArticleType() {
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(characteristicTapped))
    tapGesture.numberOfTapsRequired = 1
    self.characteristicNameLabel.isUserInteractionEnabled = true
    self.characteristicNameLabel.addGestureRecognizer(tapGesture)
  }
  
  @objc private func characteristicTapped() {
    self.characteristicLabelDelegate?.justCharacteristicTapped(sender: self)
  }
  
  private func setTextFieldHidden() {
    self.optionTextField.isHidden = true
  }
  
  func changeCharacteristicData(newData: CloudKitArticleTypeCharacteristic) {
    self.characteristicData = newData
    self.characteristicNameLabel.text = self.characteristicData.articleTypeCharacteristicName + ":"
    if (self.characteristicData.thisCharacteristicHasTheseOptions != nil) && (self.characteristicData.thisCharacteristicHasTheseOptions!.count > 0) {
      if self.usingForEditingCharacteristic {
        self.optionNameLabel.text = "Ésta característica tiene opciones"
      }
      self.optionNameLabel.isHidden = false
      self.optionTextField.isHidden = true
      optionNameLabel.options = self.characteristicData.thisCharacteristicHasTheseOptions ?? Array()
    } else {
      if usingForEditingCharacteristic {
        self.optionNameLabel.text = "El valor de ésta característica se ingresará manualmente"
        self.optionNameLabel.isHidden = false
        self.optionTextField.isHidden = true
      } else {
        self.optionNameLabel.isHidden = true
        self.optionTextField.isHidden = false
      }
    }
  }
  
  func getCharacteristicData() -> CloudKitArticleTypeCharacteristic {
    return characteristicData
  }
  
  private func valueOfCharacteristic() -> String {
    if (self.characteristicData.thisCharacteristicHasTheseOptions != nil) && (self.characteristicData.thisCharacteristicHasTheseOptions!.count > 0) {
      return self.optionNameLabel.optionSelected?.articleTypeCharacteristicOptionName ?? ""
    } else {
      return self.optionTextField.text ?? ""
    }
  }
  
}
