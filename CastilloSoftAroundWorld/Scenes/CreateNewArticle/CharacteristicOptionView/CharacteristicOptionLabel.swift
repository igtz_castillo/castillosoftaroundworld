//
//  CharacteristicOptionLabel.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 30/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

protocol CharacteristicOptionViewDelegate {
  func labelOptionTapped(sender: CharacteristicOptionView)
}

class CharacteristicOptionView: UILabel {
  
  var options: [CloudKitArticleTypeCharacteristicOption] = Array()
  var optionSelected: CloudKitArticleTypeCharacteristicOption? = nil
  var delegate: CharacteristicOptionViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.initGesture()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.initGesture()
  }
  
  private func initGesture() {
    self.isUserInteractionEnabled = true
    let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(optionViewTapped))
    tapGesture.numberOfTapsRequired = 1
    self.addGestureRecognizer(tapGesture)
  }
  
  @objc private func optionViewTapped(sender: Any) {
    self.delegate?.labelOptionTapped(sender: self)
  }
  
  func changeTextToShowWithThisOptionSelected(newOptionSelected: CloudKitArticleTypeCharacteristicOption) {
    self.text = newOptionSelected.articleTypeCharacteristicOptionName
    self.optionSelected = newOptionSelected
  }
  
}
