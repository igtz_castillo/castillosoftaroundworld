//
//  CreateNewArticleModels.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 30/05/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import CloudKit

enum CreateNewArticle {
  enum PresentArticleTypeList {
    struct ViewModel {
      var articleTypeListVC: ArticleTypeListViewController
    }
  }
  
  enum PresentAllCharacteristicsAndOptions {
    struct Response {
      var allCharactristicsWithOptions: [CloudKitArticleTypeCharacteristic]
    }
    struct ViewModel {
      var selectedOptionLabel: CharacteristicOptionView
    }
  }
  
  enum ShowCreateButton {
    struct Request {
      var arrayOfTextFields: [CastilloTextField]
      var characteristicOption: [CharacteristicWithOptionLabelView]
    }
    struct Response {
      var isHidden: Bool
    }
  }
  
  enum CreationOfNewArticle {
    struct Request {
      var articleType: CloudKitArticleType
      var articleNameString: String!
      var brandNameString: String?
      var articleCostString: String!
      var articlesDetailsString: String?
      var searchingWords: String!
      var arrayOfStringCharacteristicKey: [String]?
      var arrayOfStringCharacteristicValueSelected: [String]?
      
      var numberOfArticlesToCreate: String!
    }
    
    struct Response {
      var newArticleRecord: CKRecord?
      var error: Error?
    }
    
  }
  
  enum DisplayCreateNewArticleViewController {
    struct Request {
      var articleType: CloudKitArticleType
    }
    struct ViewModel {
      var createNewArticleVC: CreateNewArticleViewController
    }
  }
  
}

enum CloudKitArticleTypeCharacteristicsAndOptions {
  enum GetAllCharacteristicsAndOptions {
    struct Response {
      var allCharacteristicsWithOptions: [CloudKitArticleTypeCharacteristic]
    }
  }
}
