//
//  CreateNewArticleOptionsViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 31/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class CreateNewArticleOptionsViewController: UIViewController {
  
  var interactor: (CreateNewArticleBusinessLogic & CreateNewArticleDataStore)?
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.checkForRevealViewControllerFront()
  }
  
  private func setup() {
    let interactor = CreateNewArticleInteractor()
    self.interactor = interactor
  }
  
  private func checkForRevealViewControllerFront() {
    if self.revealViewController() != nil {
      if self.revealViewController().frontViewController as? UINavigationController != nil {
        if (self.revealViewController().frontViewController as! UINavigationController).viewControllers.last as? CreateNewArticleViewController != nil {
          self.interactor?.thereIsRevealViewControllerAndFrontIsCreateNewArticleViewController = true
        }
      }
    }
  }
  
  @IBAction func createOtherArticleButtonPressed(_ sender: Any) {
//    self.interactor?.createNewArticleButtonPressed()
  }
  
}
