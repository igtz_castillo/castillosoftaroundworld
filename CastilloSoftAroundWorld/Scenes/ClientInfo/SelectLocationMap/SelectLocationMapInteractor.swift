//
//  SelectLocationMapInteractor.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 27/07/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import CoreLocation

protocol SelectLocationMapDelegate {
  func positionSelected(newLocation: CLLocationCoordinate2D)
}

protocol SelectLocationMapBusinessLogic {
  func selectClientPositionButtonPressed(request: SelectLocationMap.SelectClientPositionButtonPressed.Request)
}

protocol SelectLocationMapDataStore {
  var delegateSelectedNewLocation: SelectLocationMapDelegate? {get set}
}

class SelectLocationMapInteractor: SelectLocationMapBusinessLogic, SelectLocationMapDataStore {
  var presenter: SelectLocationMapPresentationLogic?
  var worker: SelectLocationMapWorker? = SelectLocationMapWorker()
  var delegateSelectedNewLocation: SelectLocationMapDelegate?
  
  func selectClientPositionButtonPressed(request: SelectLocationMap.SelectClientPositionButtonPressed.Request) {
    self.delegateSelectedNewLocation?.positionSelected(newLocation: request.newLocation)
    self.presenter?.presentToPopViewController()
  }

}
