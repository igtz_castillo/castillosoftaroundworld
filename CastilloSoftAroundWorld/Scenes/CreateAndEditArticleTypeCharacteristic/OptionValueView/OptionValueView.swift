//
//  OptionValueView.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 18/06/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class OptionValueView: UIView {
  
  @IBOutlet weak var optionNameLabel: UILabel!
  @IBOutlet weak var optionValueTextField: UITextField!
  var optionData: CloudKitArticleTypeCharacteristicOption? = nil {
    didSet {
      changeTextFieldValue()
    }
  }
  
  class func initView() -> OptionValueView {
    let myClassNib = UINib.init(nibName: "OptionValueView", bundle: nil)
    return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! OptionValueView
  }
  
  private func changeTextFieldValue() {
    self.optionValueTextField.text = optionData?.articleTypeCharacteristicOptionName ?? ""
  }
  
}
