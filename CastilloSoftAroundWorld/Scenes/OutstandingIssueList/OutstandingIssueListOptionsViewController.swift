//
//  OutstandingIssueListOptionsViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 05/07/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class OutstandingIssueListOptionsViewController: UIViewController {
  
  var interactor: (OutstandingIssueListBusinessLogic & OutstandingIssueListDataStore)?
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.checkForRevealViewControllerFront()
  }
  
  private func setup() {
    let interactor = OutstandingIssueListInteractor()
    self.interactor = interactor
  }
  
  private func checkForRevealViewControllerFront() {
    if self.revealViewController() != nil {
      if self.revealViewController().frontViewController as? UINavigationController != nil {
        if (self.revealViewController().frontViewController as! UINavigationController).viewControllers.last as? OutstandingIssueListViewController != nil {
          self.interactor?.thereIsRevealViewControllerAndFrontIsOutstandingIssueListViewController = true
        }
      }
    }
  }

  @IBAction func createNewOutstandingIssueButtonPressed(_ sender: Any) {
    self.interactor?.createNewOutstandingIssueButtonPressed()
  }
}
