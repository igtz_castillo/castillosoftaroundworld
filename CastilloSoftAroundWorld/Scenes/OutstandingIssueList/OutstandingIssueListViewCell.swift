//
//  OutstandingIssueListViewCell.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 04/07/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class OutstandingIssueListViewCell: UITableViewCell {
  
  @IBOutlet weak var outstandingIssueDescriptionLabel: UILabel!
  private var outstandingIssueData: CloudKitOutstandingIssue = CloudKitOutstandingIssue()
  
  func changeData(newData: CloudKitOutstandingIssue) {
    self.outstandingIssueData = newData
    self.updateInfo()
  }
  
  private func updateInfo() {
    self.changeLabelText()
    self.changeBackgroundColor()
  }
  
  private func changeLabelText() {
    outstandingIssueDescriptionLabel.text = (self.outstandingIssueData.outstandingIssueDescription) ?? ""
  }
  
  private func changeBackgroundColor() {
    if self.outstandingIssueData.isAlreadyDone != nil {
      if self.outstandingIssueData.isAlreadyDone! == 1 {
        self.backgroundColor = UIColor(red: 36.0/255.0, green: 175.0/255.0, blue: 213.0/255.0, alpha: 0.3)
      } else {
        self.backgroundColor = UIColor(red: 255.0/255.0, green: 0.0, blue: 0.0, alpha: 0.3)
      }
    } else {
      self.backgroundColor = UIColor(red: 255.0/255.0, green: 0.0, blue: 0.0, alpha: 0.3)
    }
  }
  
}
