//
//  ArticleDetailRouter.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 12/06/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol ArticleDetailRoutingLogic {
  func routeToClientListToAddArticle()
  func routeToDismiss()
  func routeToPopViewController()
}

protocol ArticleDetailDataPassing {
  var dataStore: ArticleDetailDataStore? { get }
}

class ArticleDetailRouter: NSObject, ArticleDetailRoutingLogic, ArticleDetailDataPassing {
  weak var viewController: ArticleDetailViewController?
  var dataStore: ArticleDetailDataStore?
  
  func routeToClientListToAddArticle() {
    let storyboard = UIStoryboard(name: "ClientListForAddingArticle", bundle: nil)
    let clientListForAddingArticleVC = storyboard.instantiateViewController(withIdentifier: "ClientListForAddingArticleViewController") as! ClientListForAddingArticleViewController
    clientListForAddingArticleVC.interactor?.articleToAddToClientAccount = self.dataStore?.articleSelected!
    clientListForAddingArticleVC.interactor?.actionsToDoWhenFinishAddingArticleToClient = self.dataStore?.actionToDoWhenFinishAddingArticle
    self.viewController?.navigationController?.pushViewController(clientListForAddingArticleVC, animated: true)
  }

  func routeToDismiss() {
    self.viewController?.dismiss(animated: true, completion: nil)
  }
  
  func routeToPopViewController() {
    self.viewController?.navigationController?.popViewController(animated: true)
  }
  
}
