//
//  ArticleDetailWorker.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 12/06/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import PromiseKit
import CloudKit

class ArticleDetailWorker {
  
  func createOptionViews(articleSelected: CloudKitArticle) -> Array<CharacteristicWithOptionLabelView> {
    var arrayOfCharacteristicsAndOptions: Array<CharacteristicWithOptionLabelView> = Array()
    for index in  0...articleSelected.selectedCharacteristicsDictionaryKeys.count - 1 {
      let newCharacteristicView = CharacteristicWithOptionLabelView.initView()
      newCharacteristicView.optionTextField.isHidden = true
      newCharacteristicView.characteristicNameLabel.text = articleSelected.selectedCharacteristicsDictionaryKeys[index]
      if index < articleSelected.selectedCharacteristicsDictionaryValues.count {
        newCharacteristicView.optionNameLabel.text = articleSelected.selectedCharacteristicsDictionaryValues[index]
      } else {
        newCharacteristicView.optionNameLabel.text = ""
      }
      arrayOfCharacteristicsAndOptions.append(newCharacteristicView)
    }
    return arrayOfCharacteristicsAndOptions
  }
  
  func addArticleToAccount(request: ArticleListForAddingArticleToAccount.AddingArticleToAccount.Request) -> Promise<[CKRecord]> {
    return Promise { result in
      CloudKitModel.sharedInstance.add(this: request.articleSelected, toThisAccount: request.accountToAddArticle).done({ (modifiedRecords) in
        return result.fulfill(modifiedRecords)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
}
