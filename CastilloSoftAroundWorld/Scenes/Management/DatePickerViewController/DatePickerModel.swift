//
//  DatePickerModels.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 12/07/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class DatePickerModel{
  enum DateSelected {
    struct Request {
      var dateSelectedFromPickerView: Date
    }
  }
  
  enum InitValuesForPickerView {
    struct Request {
      var datePickerView: UIDatePicker
    }
  }
  
}
