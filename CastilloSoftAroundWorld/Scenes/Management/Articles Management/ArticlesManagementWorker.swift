//
//  ArticlesManagementWorker.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 10/07/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import PromiseKit
import CloudKit

class ArticlesManagementWorker {
  
  var numberOfAllArticleType: Int = -1
  var numberOfSuccessfulRequest: Int = 0 {
    didSet {
      successfulRequest()
    }
  }
  var actionsToDoWhenGetAllArticlesSuccessful: ((_ allResponses: [ArticleTypeManagementResponse])->Void)?
  var actionsToDoWhenGetAllArticlesFailed: ((Error)->Void)?
  var allResponses: [ArticleTypeManagementResponse] = Array()
  var waitingTimerForError: Timer = Timer.init()
  
  func getAllArticles(fromDate: Date, toDate: Date?) {
    self.allResponses.removeAll()
    self.numberOfSuccessfulRequest = 0
    self.numberOfAllArticleType = -1
    self.getAllArticleTypes().done({ (allArticleTypes) in
      if allArticleTypes.count > 0 {
        self.numberOfAllArticleType = allArticleTypes.count
        for index in 0...allArticleTypes.count - 1 {
          let newResponse = ArticleTypeManagementResponse(articleType: allArticleTypes[index], articlesInThisArticleType: Array())
          self.allResponses.append(newResponse)
          self.getAllArticles(fromDate: fromDate, toDate: toDate, indexOfArrayOfResponses: index)
        }
      }
    }).catch { (error) in
      self.actionsToDoWhenGetAllArticlesFailed?(error)
    }
    waitingTimerForError = Timer.scheduledTimer(withTimeInterval: 15, repeats: false) { (timer) in
      self.numberOfAllArticleType = -1
      let error = NSError(domain: "Error en tiempo de espera", code: -1, userInfo: nil)
      self.actionsToDoWhenGetAllArticlesFailed?(error)
    }
  }
  
  private func getAllArticleTypes() -> Promise<[CloudKitArticleType]> {
    return Promise { result in
      CloudKitModel.sharedInstance.fetchRecords(forType: CloudKitKeys.ArticleType.ArticleType.rawValue).done({ (allArticleType) in
        let allArticleTypeData = allArticleType.map({CloudKitModel.sharedInstance.getCloudKitArticleTypeFromServerData(recordFromCloudKit: $0)})
        return result.fulfill(allArticleTypeData)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  private func getAllArticles(fromDate: Date, toDate: Date?, indexOfArrayOfResponses: Int) {
    
    let articleTypeReference = CKReference(recordID: self.allResponses[indexOfArrayOfResponses].articleType.rawDataFromServer!.recordID, action: .none)
    let articleTypePredicate = NSPredicate(format: "\(CloudKitKeys.Article.thisArticleIsThisArticleType.rawValue) = %@", articleTypeReference)
    let buyedPredicate = NSPredicate(format: "\(CloudKitKeys.Article.isAddedInAnAccount.rawValue) == %i", 1)
    var finalPredicate = NSCompoundPredicate.init()
    var firstPredicate: NSPredicate = NSPredicate.init()
    var secondPredicate: NSPredicate? = nil
    if toDate != nil {
      firstPredicate = NSPredicate(format: "\(CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue) > %@", fromDate as CVarArg)
      secondPredicate = NSPredicate(format: "\(CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue) <= %@", toDate!.getTomorrow() as CVarArg)
      finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [articleTypePredicate, buyedPredicate, firstPredicate, secondPredicate!])
    } else {
      firstPredicate = NSPredicate(format: "\(CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue) > %@", fromDate as CVarArg)
      secondPredicate = NSPredicate(format: "\(CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue) < %@", fromDate.getTomorrow() as CVarArg)
      finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [articleTypePredicate, buyedPredicate, firstPredicate, secondPredicate!])
    }
    let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue, ascending: true)
    CloudKitModel.sharedInstance.fetchRecords(forType: CloudKitKeys.Article.Article.rawValue, sortDescriptors: [sortDescriptor], predicate: finalPredicate).done({ (allArticles) in
      if indexOfArrayOfResponses < self.allResponses.count {
        let arrayOfArticles = allArticles.map({ CloudKitModel.sharedInstance.getCloudKitArticleFromServerData(recordFromCloudKit: $0)})
        self.allResponses[indexOfArrayOfResponses].articlesInThisArticleType = arrayOfArticles
        self.numberOfSuccessfulRequest = self.numberOfSuccessfulRequest + 1
      }
    }).catch({ (error) in
      self.actionsToDoWhenGetAllArticlesFailed?(error)
    })
  }
  func getCellToShow(request: ArticlesManagement.CellForTableView.Request) -> ArticlesManagement.CellForTableView.Response {
    let cell = request.tableView.dequeueReusableCell(withIdentifier: request.identifier, for: request.indexPath) as! DetailClientTableViewCellArticle
    let paymentData = request.arrayOfArticles [request.indexPath.row]
    cell.changeArticleData(newData: paymentData)
    return ArticlesManagement.CellForTableView.Response(cell: cell)
  }
  
  func getCellToShowForCollectionView(request: ArticlesManagement.CellForCollectionView.Request) -> ArticlesManagement.CellForCollectionView.Response {
    let cell = request.collectionView.dequeueReusableCell(withReuseIdentifier: request.identifier, for: request.indexPath) as! ArticleTypeCollectionViewCell
    let responseData = request.arrayOfResponses[request.indexPath.row]
    cell.changeResponseData(newData: responseData)
    return ArticlesManagement.CellForCollectionView.Response(collectionCell: cell)
  }
  
  private func successfulRequest() {
    if self.numberOfAllArticleType == self.numberOfSuccessfulRequest {
      self.waitingTimerForError.invalidate()
      if self.actionsToDoWhenGetAllArticlesSuccessful != nil {
        
        var filteredResponses = Array<ArticleTypeManagementResponse>()
        for response in self.allResponses {
          if response.articlesInThisArticleType.count > 0 {
            filteredResponses.append(response)
          }
        }
        self.actionsToDoWhenGetAllArticlesSuccessful!(filteredResponses)
      }
    }
  }
  
  func getAccountAndClientInfoOfThisArticle(articleData: CloudKitArticle) -> Promise<ArticlesManagement.DidSelectRowAt.Response> {
    return Promise { result in
      var response = ArticlesManagement.DidSelectRowAt.Response(articleData: articleData, accountData: CloudKitAccount(), clientData: CloudKitClient())
      CloudKitModel.sharedInstance.getAccountWhichIsAddedThisArticle(articleData: articleData).then({ (accountData) -> Promise<CloudKitClient> in
        response.accountData = accountData
        return CloudKitModel.sharedInstance.getClientOfThisAccount(account: response.accountData)
      }).done({ (clientData) in
        response.clientData = clientData
        return result.fulfill(response)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
}
