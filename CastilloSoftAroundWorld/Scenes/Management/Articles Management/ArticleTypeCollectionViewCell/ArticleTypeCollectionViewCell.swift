//
//  ArticleTypeCollectionViewCell.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 17/07/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class ArticleTypeCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var articleTypeNameLabel: UILabel!
    var responseFromServer: ArticleTypeManagementResponse?
    
    func changeResponseData(newData: ArticleTypeManagementResponse) {
        self.responseFromServer = newData
        self.articleTypeNameLabel.text = self.responseFromServer!.articleType.articleTypeName
    }
  
}
