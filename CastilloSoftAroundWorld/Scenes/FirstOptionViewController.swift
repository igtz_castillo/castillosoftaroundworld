//
//  FirstOptionViewController.swift
//  SideMenuExample
//
//  Created by Israel on 28/03/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import UIKit

class FirstOptionViewController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var pendingsButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.sideMenu()
      
//      CloudKitModel.sharedInstance.getAllCloudKitClients().done { (clients) in
//        print(clients)
//        }.catch { (error) in
//          print("Error in FirstOptionViewController. Error: \(error)")
//      }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func sideMenu() {
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            revealViewController().rearViewRevealWidth = UIScreen.main.bounds.size.width / 2.0
            revealViewController().rightViewRevealWidth = UIScreen.main.bounds.size.width / 4.0
            
            pendingsButton.target = revealViewController()
            pendingsButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
