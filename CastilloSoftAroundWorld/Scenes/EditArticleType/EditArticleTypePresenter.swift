//
//  EditArticleTypePresenter.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 16/06/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol EditArticleTypePresentationLogic {
  func presentAllArticleTypeInfo(response: EditArticleType.LoadAndShowInfo.Response)
  func presentCharacteristicAndOptions(request: EditArticleType.ShowCharacteristicAndOptions.Request)
  func presentAddNewCharacteristicButtonPressed(request: EditArticleType.AddNewCharacteristic.Request)
  func presentPopViewController()
  func presentMessage(message: String)
  func presentError(error: Error, message: String)
  func presentLoader()
  func presentHiddingLoader()
}

class EditArticleTypePresenter: EditArticleTypePresentationLogic {
  weak var viewController: (EditArticleTypeDisplayLogic & PresentLoaderLogic & PresentMessageLogic & PresentErrorLogic & CreateAndEditArticleTypeCharacteristicViewControllerDelegate)?
  
  func presentAllArticleTypeInfo(response: EditArticleType.LoadAndShowInfo.Response) {
    self.viewController?.displayAllArticleTypeInfo(response: response)
  }
  
  func presentCharacteristicAndOptions(request: EditArticleType.ShowCharacteristicAndOptions.Request) {
    let storyboard = UIStoryboard(name: "CreateAndEditArticleTypeCharacteristic", bundle: nil)
    let editCharacteristicVC = storyboard.instantiateViewController(withIdentifier: "CreateAndEditArticleTypeCharacteristicViewController") as! CreateAndEditArticleTypeCharacteristicViewController
    editCharacteristicVC.modalPresentationStyle = .formSheet
    editCharacteristicVC.interactor?.articleTypeCharacteristicToEdit = request.characteristicData
    editCharacteristicVC.interactor?.articleTypeToEdit = request.articleTypeItsFrom
    editCharacteristicVC.interactor?.creatingNewCharacteristic = request.creatingNewCharacteristic
    editCharacteristicVC.interactor?.delegate = self.viewController
    let viewModel = EditArticleType.ShowCharacteristicAndOptions.ViewModel(editCharacteristicVC: editCharacteristicVC)
    self.viewController?.displayEditCharacteristicViewController(viewModel: viewModel)
  }
  
  func presentAddNewCharacteristicButtonPressed(request: EditArticleType.AddNewCharacteristic.Request) {
    self.viewController?.displayNewCharacteristic(request: request)
  }
  
  func presentPopViewController() {
    self.viewController?.displayPopViewController()
  }
  
  func presentMessage(message: String) {
    //change the "actionsToDoWithTap"
    let request = PresentMessageModel.NormalMessage.Request(title: "Todo en orden", message: message, image: #imageLiteral(resourceName: "successful"), actionsToDoWithTap: nil)
    self.viewController?.displayMessage(request: request)
  }
  
  func presentError(error: Error, message: String) {
    let request = PresentMessageModel.ErrorMessage.Request(error: error, title: "ERROR", message: message, image: #imageLiteral(resourceName: "error"), actionsToDoWithTap: nil)
    self.viewController?.displayMessageError(request: request)
  }
  
  func presentLoader() {
    self.viewController?.showLoader()
  }
  
  func presentHiddingLoader() {
    self.viewController?.hideLoader()
  }
}
