//
//  PaymentDetailPresenter.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 03/07/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol PaymentDetailPresentationLogic {
  func presentMessage(request: PresentMessageModel.NormalMessage.Request)
  func presentErrorMessage(request: PresentMessageModel.ErrorMessage.Request)
  func presentLoader()
  func presentHiddingLoader()
  func presentDismiss()
}

class PaymentDetailPresenter: PaymentDetailPresentationLogic {
  weak var viewController: (PaymentDetailDisplayLogic & PresentMessageLogic & PresentLoaderLogic & PresentErrorLogic)?
  
  func presentMessage(request: PresentMessageModel.NormalMessage.Request) {
    self.viewController?.displayMessage(request: request)
  }
  
  func presentErrorMessage(request: PresentMessageModel.ErrorMessage.Request) {
    self.viewController?.displayMessageError(request: request)
  }
  
  func presentLoader() {
    self.viewController?.showLoader()
  }
  
  func presentHiddingLoader() {
    self.viewController?.hideLoader()
  }
  
  func presentDismiss() {
    self.viewController?.displayDismiss()
  }
  
}
