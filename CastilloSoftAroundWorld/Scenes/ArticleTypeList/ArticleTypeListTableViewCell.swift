//
//  ArticleTypeListTableViewCell.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 14/06/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class ArticleTypeListTableViewCell: UITableViewCell {
  
  @IBOutlet weak var articleTypeNameLabel: UILabel!
  
}
