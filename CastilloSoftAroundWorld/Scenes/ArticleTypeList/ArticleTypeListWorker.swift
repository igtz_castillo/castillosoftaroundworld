//
//  ArticleTypeListWorker.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 22/05/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import PromiseKit
import CloudKit

class ArticleTypeListWorker {

  func getAllArticleTypeFromServer() -> Promise<ArticleTypeList.GetArticleTypeFromServer.Response> {
    return Promise { result in
      CloudKitModel.sharedInstance.getAllCloudKitArticleType().done { (allArticleType) in
        let response = ArticleTypeList.GetArticleTypeFromServer.Response(allArticleType: allArticleType)
        result.fulfill(response)
        }.catch({ (error) in
          result.reject(error)
        })
    }
  }
  
  func getFilteredArticleTypes(request: ArticleTypeList.UpdateSearchBar.Request) -> ArticleTypeList.UpdateSearchBar.Response {
    
    let searchText = request.findingText
    var searchResults = request.allArticleType
    let strippedString = searchText.trimmingCharacters(in: .whitespaces)
    var searchItems: [String] = Array()
    if strippedString.count > 0 {
      searchItems = strippedString.components(separatedBy: " ")
    }
    var andMatchPredictaes: [NSCompoundPredicate] = Array()
    for searchString in searchItems {
      var searchItemsPredicate: [NSPredicate] = Array()
      
      var lhs: NSExpression = NSExpression(forKeyPath: "articleTypeComment")
      var rhs: NSExpression = NSExpression(forConstantValue: searchString)
      var finalPredicate: NSComparisonPredicate = NSComparisonPredicate(leftExpression: lhs,
                                                                        rightExpression: rhs,
                                                                        modifier: .direct,
                                                                        type: .contains,
                                                                        options: NSComparisonPredicate.Options.caseInsensitive)
      searchItemsPredicate.append(finalPredicate)
      
      lhs = NSExpression(forKeyPath: "articleTypeName")
      rhs = NSExpression(forConstantValue: searchString)
      finalPredicate = NSComparisonPredicate(leftExpression: lhs,
                                             rightExpression: rhs,
                                             modifier: .direct,
                                             type: .contains,
                                             options: NSComparisonPredicate.Options.caseInsensitive)
      searchItemsPredicate.append(finalPredicate)
      
      let orMatchPredicates: NSCompoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: searchItemsPredicate)
      andMatchPredictaes.append(orMatchPredicates)
    }
    
    let finalCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: andMatchPredictaes)
    
    let nsArraySearchResults = searchResults as NSArray
    searchResults = (nsArraySearchResults.filtered(using: finalCompoundPredicate) as? [CloudKitArticleType]) ?? Array()
    
    return ArticleTypeList.UpdateSearchBar.Response(filteredArticleTypes: searchResults)
  }
  
  func createNewArticleType(request: ArticleTypeListOptions.CreateNewArticleType.Request) -> Promise<CloudKitArticleType> {
    return Promise { result in
      CloudKitModel.sharedInstance.createNewArticleType(articleTypeName: request.nameOfNewArticleType, articleTypeComment: request.commentOfNewArticleType).done({ (newArticleType) in
        return result.fulfill(newArticleType)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
}
