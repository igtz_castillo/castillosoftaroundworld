//
//  ArticleTypeListOptionsViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 15/06/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class ArticleTypeListOptionsViewController: UIViewController {
  
  var interactor: (ArticleTypeListBusinessLogic & ArticleTypeListDataStore)?
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.checkForRevealViewControllerFront()
  }
  
  private func setup() {
    let interactor = ArticleTypeListInteractor()
    self.interactor = interactor
  }
  
  private func checkForRevealViewControllerFront() {
    if self.revealViewController() != nil {
      if self.revealViewController().frontViewController as? UINavigationController != nil {
        if (self.revealViewController().frontViewController as! UINavigationController).viewControllers.last as? ArticleTypeListViewController != nil {
          self.interactor?.thereIsRevealViewControllerAndFrontIsArticleTypeListViewController = true
        }
      }
    }
  }
  
  @IBAction func createNewArticleTypeButtonPressed(_ sender: Any) {
    self.interactor?.createNewArticleTypeButtonPressed()
  }
}
