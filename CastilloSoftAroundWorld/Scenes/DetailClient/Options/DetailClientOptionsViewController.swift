//
//  DetailClientOptionsViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 02/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class DetailClientOptionsViewController: UIViewController {

  @IBOutlet weak var editClientDataButton: UIButton!
  @IBOutlet weak var backNextWeekButton: UIButton!
  @IBOutlet weak var backUnitlNextDayButton: UIButton!
  @IBOutlet weak var backIn45MinutesButton: UIButton!
  @IBOutlet weak var backInToThisTimeButton: UIButton!
  var interactor: (DetailClientBusinessLogic & DetailClientDataStore)?
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.checkForRevealViewControllerFront()
  }
  
  private func setup() {
    let interactor = DetailClientInteractor()
    self.interactor = interactor
  }
  
  private func checkForRevealViewControllerFront() {
    if self.revealViewController() != nil {
      if self.revealViewController().frontViewController as? UINavigationController != nil {
        if (self.revealViewController().frontViewController as! UINavigationController).viewControllers.last as? DetailClientViewController != nil {
          self.interactor?.thereIsRevealViewControllerAndFrontIsDetailClientViewController = true
        }
      }
    }
  }
  
  @IBAction func editClientButtonPressed(_ sender: Any) {
    if self.interactor!.thereIsRevealViewControllerAndFrontIsDetailClientViewController {
      self.interactor!.showClientInfoButtonPressed()
    }
  }
  
//  Vistar la próxima semana
  @IBAction func backNextWeekButtonPressed(_ sender: Any) {
    if self.interactor!.thereIsRevealViewControllerAndFrontIsDetailClientViewController {
      self.interactor!.backNextWeekButtonPressed()
    }
  }
  
//  Visitar el próximo...
  @IBAction func backUntilNextDayButtonPressed(_ sender: Any) {
    if self.interactor!.thereIsRevealViewControllerAndFrontIsDetailClientViewController {
      self.interactor!.backUntilNextDayButtonPressed()
    }
  
  }
  
//  Regresar en 45 minutos
  @IBAction func backIn45MinutesButtonPressed(_ sender: Any) {
    if self.interactor!.thereIsRevealViewControllerAndFrontIsDetailClientViewController {
      self.interactor!.createBackIn45MinutesAlertButtonPressed()
    }
  }
  
//  Regresar a las...
  @IBAction func backIntoThisTimeButtonPressed(_ sender: Any) {
    if self.interactor!.thereIsRevealViewControllerAndFrontIsDetailClientViewController {
      self.interactor!.backIntoThisTimeButtonPressed()
    }
  }
  
}
