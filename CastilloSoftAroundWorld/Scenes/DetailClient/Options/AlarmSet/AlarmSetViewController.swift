//
//  AlarmSetViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 17/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class AlarmSetViewController: UIViewController {
  @IBOutlet weak var datePickerView: UIDatePicker!
  var interactor: (DetailClientBusinessLogic & DetailClientDataStore)?
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  private func setup() {
    let interactor = DetailClientInteractor()
    self.interactor = interactor
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.datePickerView.minimumDate = Date.today()
  }
  
  
  @IBAction func setAlarmCancelButtonPressed(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  
  @IBAction func setAlarmAcceptButtonPressed(_ sender: Any) {
    let dateSelected = self.datePickerView.date
    let request = DetailClientOptions.CreateLocalNotification.RequestForSpecificDate(dateForAlarm: dateSelected)
    self.dismiss(animated: true, completion: nil)
    self.interactor!.setAlarm(request: request)
  }
}
