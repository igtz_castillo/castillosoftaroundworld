//
//  SelectNextVisitDateViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 16/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class SelectNextVisitDateViewController: UIViewController {
  
  @IBOutlet weak var datePickerView: UIDatePicker!
  var interactor: (DetailClientBusinessLogic & DetailClientDataStore)?
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  private func setup() {
    let interactor = DetailClientInteractor()
    self.interactor = interactor
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.datePickerView.minimumDate = Date.today()
  }
  
  
  @IBAction func cancelButtonPressed(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func acceptButtonPressed(_ sender: Any) {
    let request = DetailClientOptions.SetNextDateFromPicker.Request(alarmDate: self.datePickerView.date, nextDateToVisit: self.getDateFromPicker())
    self.interactor!.backUntilNextDaySelectedFromPickerView(request: request)
    self.dismiss(animated: true, completion: nil)
    
  }
  
  private func getDateFromPicker() -> String {
    return self.datePickerView.date.getStringFromDate(dateStyle: .full)
  }
  
  
}
