//
//  DetailClientTableViewCellPayment.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 04/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class DetailClientTableViewCellPayment: UITableViewCell {
  
  private var paymentData: CloudKitPayment!
  @IBOutlet weak var quantityOfPaymentLabel: UILabel!
  @IBOutlet weak var dateOfPaymentLabel: UILabel!
  @IBOutlet weak var hasCommentLabel: UILabel!
    
  
  func changePaymentData(newData: CloudKitPayment) {
    self.paymentData = newData
    self.quantityOfPaymentLabel.text = " - $\(self.paymentData.ammountPaid)"
    self.dateOfPaymentLabel.text = self.paymentData.dayOfPayment.getStringFromDate()
    self.hasCommentLabel.isHidden = true
    if self.paymentData.paymentComment.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
        self.hasCommentLabel.isHidden = false
    }
  }
  
}
