//
//  DetailClientTableViewCellArticle.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 04/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class DetailClientTableViewCellArticle: UITableViewCell {
  
  private var articleData: CloudKitArticle!
  @IBOutlet weak var productNameLabel: UILabel!
  @IBOutlet weak var productPriceLabel: UILabel!
  @IBOutlet weak var buyingDateLabel: UILabel!
  
  func changeArticleData(newData: CloudKitArticle) {
    self.articleData = newData
    self.productNameLabel.text = articleData.nameArticle
    self.productPriceLabel.text = ("$\(articleData.cost)")
    self.buyingDateLabel.text = articleData.purchaseDateOfThisArticle?.getStringFromDate() ?? ""
  }
  
}
