//
//  DetailClientTableViewCellAddition.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 07/05/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class DetailClientTableViewCellAddition: UITableViewCell {
  
  @IBOutlet weak var additionValueLabel: UILabel!
  
  func changeInfo(newAddition: NSNumber) {
    self.additionValueLabel.text = String(format: "$%.2f", newAddition.doubleValue)
  }
  
}
