//
//  OptionsFindingClientsViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 30/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class OptionsFindingClientsViewController: UIViewController {
  
  var interactor: (FindingClientsBusinessLogic & FindingClientsDataStore)?
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  private func setup() {
    self.interactor = FindingClientsInteractor()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.checkForRevealViewControllerFront()
  }
  
  private func checkForRevealViewControllerFront() {
    if self.revealViewController() != nil {
      if self.revealViewController().frontViewController as? UINavigationController != nil {
        if (self.revealViewController().frontViewController as! UINavigationController).viewControllers.last as? FindingClientsViewController != nil {
          self.interactor?.thereIsRevealViewControllerAndFrontIsFindingClientsViewController  = true
        }
      }
    }
  }
    
  @IBAction func createNewClientButtonPressed(_ sender: Any) {
    if self.interactor?.thereIsRevealViewControllerAndFrontIsFindingClientsViewController ?? false {
      self.interactor?.createNewClientButtonPressed()
    }
  }
    
    @IBAction func searchingDirectlyInServerButtonPressed(_ sender: Any) {
        if self.interactor?.thereIsRevealViewControllerAndFrontIsFindingClientsViewController ?? false {
            self.interactor?.searchingClientsDirectFromServerButtonPressed()
        }
    }
    
    
    @IBAction func showActiveClientsButtonPressed(_ sender: Any) {
        if self.interactor?.thereIsRevealViewControllerAndFrontIsFindingClientsViewController ?? false {
            self.interactor?.filterActiveClientsButtonPressed()
        }
    }
    
    @IBAction func showClientsThatDontPaySinceSevenDaysAgoButtonPressed(_ sender: Any) {
        if self.interactor?.thereIsRevealViewControllerAndFrontIsFindingClientsViewController ?? false {
            self.interactor?.filterClientsThatDontPaySinceSevenDaysAgoButtonPressed()
        }
        
    }
    
  @IBAction func showAllClientsButtonPressed(_ sender: Any) {
    if self.interactor?.thereIsRevealViewControllerAndFrontIsFindingClientsViewController ?? false {
        self.interactor?.filterAllClientsButtonPressed()
    }
  }
    
    
}
