//
//  FindingClientsTableViewCell.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 30/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

class FindingClientsTableViewCell: UITableViewCell {
  
  @IBOutlet weak var clientNameLabel: UILabel!
  @IBOutlet weak var clientWarehouseNumberLabel: UILabel!
  @IBOutlet weak var clientSurenameLabel: UILabel!
  private var clientData: CloudKitClient!
  
  func changeData(newClientData: CloudKitClient) {
    self.clientData = newClientData
    self.clientNameLabel.text = clientData.clientName
    self.clientSurenameLabel.text = clientData.surnames
    self.clientWarehouseNumberLabel.text = clientData.wareHouseNumber
  }
  
}
