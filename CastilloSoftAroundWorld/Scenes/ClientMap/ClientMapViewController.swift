//
//  ClientMapViewController.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 26/07/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import MapKit

protocol ClientMapDisplayLogic: class {
  func displayallClientsWithAnnotation(response: ClientMap.LoadAllClientsAnnotations.Response)
  func displayDidUpdateLocations(response: ClientMap.DidUpdateLocations.Response)
  func displayAnnotationTransition(response: ClientMap.DidSelectRowAt.Response)
  func displayReloadTableForVisibleAnnotations()
}

class ClientMapViewController: UIViewController, ClientMapDisplayLogic, PresentMessageLogic, PresentErrorLogic, PresentLoaderLogic {
  var interactor: (ClientMapBusinessLogic & ClientMapDataStore)?
  var router: (NSObjectProtocol & ClientMapRoutingLogic & ClientMapDataPassing)?
  var locationManager: CLLocationManager!
  let kCellIdentifier = "clientCellID"
  let kMapViewIdentifier = "mapViewID"

  @IBOutlet weak var menuButton: UIBarButtonItem!
  @IBOutlet weak var mainMapView: MKMapView!
  @IBOutlet weak var mainTableView: UITableView!
  @IBOutlet weak var transportationTypeSegmentedControl: UISegmentedControl!
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  private func setup() {
    let viewController = self
    let interactor = ClientMapInteractor()
    let presenter = ClientMapPresenter()
    let router = ClientMapRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.changeTitle()
    self.sideMenu()
    self.initMainTableView()
    self.initMainMapView()
    self.initTransportationTypeSegmentedControl()
    self.initLocationManager()
    self.interactor?.loadAllClientAnnotations()
    self.centerMapInInUserLocation(manager: self.interactor!.locationManager)
  }
  
  private func changeTitle() {
    self.title = "Clientes a visitar hoy"
  }
  
  private func sideMenu() {
    if revealViewController() != nil {
      menuButton.target = revealViewController()
      menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
      revealViewController().rearViewRevealWidth = UIScreen.main.bounds.size.width / 2.0
      revealViewController().rightViewController = nil
      
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
  }
  
  private func initLocationManager() {
    self.interactor?.initLocationManager(request: ClientMap.InitLocationManager.Request(delegate: self))
  }
  
  @IBAction func reloadButtonPressed(_ sender: Any) {
    self.interactor?.reloadButtonPressed()
  }
    
    
  func displayallClientsWithAnnotation(response: ClientMap.LoadAllClientsAnnotations.Response) {
    self.mainMapView.removeAnnotations(self.mainMapView.annotations)
    for annotation in response.allClientAnnotations {
      self.mainMapView.addAnnotation(annotation)
    }
  }
  
  func displayDidUpdateLocations(response: ClientMap.DidUpdateLocations.Response) {
//    self.centerMapInInUserLocation(manager: response.locationManager)
  }
  
  func displayAnnotationTransition(response: ClientMap.DidSelectRowAt.Response) {
    if response.annotation != nil {
      self.selectAnnotationInMap(annotation: response.annotation!)
    }
  }
  
  func displayReloadTableForVisibleAnnotations() {
    self.mainTableView.reloadSections(IndexSet(integer: 0) , with: .fade)
  }
  
  private func centerMapInInUserLocation(manager: CLLocationManager) {
    if manager.location != nil {
      self.mainMapView.setCenter(manager.location!.coordinate, animated: true)
      let visibleRegion = MKCoordinateRegionMakeWithDistance(manager.location!.coordinate, 75, 75)
      self.mainMapView.setRegion(self.mainMapView.regionThatFits(visibleRegion), animated: true)
    }
  }
  
}

extension ClientMapViewController: MKMapViewDelegate {
  
  private func initMainMapView() {
    self.mainMapView.showsScale = true
    self.mainMapView.showsCompass = true
    self.mainMapView.showsUserLocation = true
    self.mainMapView.delegate = self
    self.mainMapView.userTrackingMode = .followWithHeading
//    self.mainMapView.register(MKMapView.self, forAnnotationViewWithReuseIdentifier: kMapViewIdentifier)
  }
  
  private func initTransportationTypeSegmentedControl() {
    self.transportationTypeSegmentedControl.selectedSegmentIndex = 0
  }
    
  func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
    let request = ClientMap.RegionDidChange.Request(mapView: mapView)
    self.interactor?.regionDidChange(request: request)
  }
  
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    if annotation is MKUserLocation { return nil }
    var annotationView: MKAnnotationView?
    if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: kMapViewIdentifier) {
      annotationView = dequeuedAnnotationView
      annotationView?.annotation = annotation
    } else {
      annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: kMapViewIdentifier)
    }
    annotationView?.canShowCallout = true
    annotationView?.image = #imageLiteral(resourceName: "successful").resizeWithPercent(percentage: 0.1)
    annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    let detailCallOutAccessoryView = ClientAnnotationView.initView()
    annotationView?.detailCalloutAccessoryView = detailCallOutAccessoryView
    annotationView?.rightCalloutAccessoryView = UIView()
    
    return annotationView
  }
  
  func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
    
    if view.annotation is MKUserLocation {return}
    
    let request = ClientMap.AnnotationDidSelect.Request(mapView: self.mainMapView,
                                                locationManager: self.interactor!.locationManager,
                                             annotationSelected: view.annotation as? ClientMKAnnotation,
                                                    callOutView: view.detailCalloutAccessoryView as! ClientAnnotationView,
                                             transportationType: self.transportationTypeSegmentedControl.selectedSegmentIndex)
    self.interactor?.annotationViewDidSelect(request:  request)
  }
  
  func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    if overlay is MKPolyline {
      let polylineRenderer = MKPolylineRenderer(overlay: overlay)
      polylineRenderer.strokeColor = UIColor.blue
      polylineRenderer.lineWidth = 5
      return polylineRenderer
    }
    return MKOverlayRenderer.init()
  }
  
  func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
    
  }
    
}

extension ClientMapViewController: UITableViewDelegate, UITableViewDataSource {
  
  private func initMainTableView() {
    self.mainTableView.register(UITableViewCell.self, forCellReuseIdentifier: kCellIdentifier)
    self.mainTableView.delegate = self
    self.mainTableView.dataSource = self
  }
    
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.interactor?.visibleAnnotations.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 60.0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.mainTableView.dequeueReusableCell(withIdentifier: kCellIdentifier, for: indexPath)
    cell.textLabel?.text = self.interactor!.visibleAnnotations[indexPath.row].clientData.clientName
    cell.detailTextLabel?.text = self.interactor!.visibleAnnotations[indexPath.row].clientData.wareHouseNumber
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let request = ClientMap.DidSelectRowAt.Request(annotationSelected: self.interactor!.visibleAnnotations[indexPath.row], mapView: self.mainMapView)
    self.interactor?.didSelectRowAt(request: request)
  }
  
  private func selectAnnotationInMap(annotation: ClientMKAnnotation) {
    self.mainMapView.selectAnnotation(annotation, animated: true)
    if CLLocationCoordinate2DIsValid(annotation.coordinate) {
      self.mainMapView.setCenter(annotation.coordinate, animated: true)
    }
  }
  
    
}

extension ClientMapViewController: CLLocationManagerDelegate {
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let response = ClientMap.DidUpdateLocations.Response(locationManager: manager)
    self.interactor?.didUpdateLocations(response: response)
  }
  
}
