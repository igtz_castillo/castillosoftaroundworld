//
//  ClientMapPresenter.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 26/07/18.
//  Copyright (c) 2018 IsraelGutierrez. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ClientMapPresentationLogic {
  func presentAllClientsWithAnnotation(response: ClientMap.LoadAllClientsAnnotations.Response)
  func presentDidUpdateLocations(response: ClientMap.DidUpdateLocations.Response)
  func presentAnnotationTransition(response: ClientMap.DidSelectRowAt.Response)
  func presentShowVisibleAnnotations()
  
  func presentMessage(request: PresentMessageModel.NormalMessage.Request)
  func presentError(request: PresentMessageModel.ErrorMessage.Request)
  func presentShowLoader()
  func presentHiddingLoader()
}

class ClientMapPresenter: ClientMapPresentationLogic {
  weak var viewController: (ClientMapDisplayLogic & PresentMessageLogic & PresentErrorLogic & PresentLoaderLogic)?
  
  func presentAllClientsWithAnnotation(response: ClientMap.LoadAllClientsAnnotations.Response) {
    self.viewController?.displayallClientsWithAnnotation(response: response)
  }
  
  func presentDidUpdateLocations(response: ClientMap.DidUpdateLocations.Response) {
    self.viewController?.displayDidUpdateLocations(response: response)
  }
  
  func presentAnnotationTransition(response: ClientMap.DidSelectRowAt.Response) {
    self.viewController?.displayAnnotationTransition(response: response)
  }
  
  func presentShowVisibleAnnotations() {
    self.viewController?.displayReloadTableForVisibleAnnotations()
  }
  
  func presentMessage(request: PresentMessageModel.NormalMessage.Request) {
    self.viewController?.displayMessage(request: request)
  }
  
  func presentError(request: PresentMessageModel.ErrorMessage.Request) {
    self.viewController?.displayMessageError(request: request)
  }
  
  func presentShowLoader() {
    self.viewController?.showLoader()
  }
  
  func presentHiddingLoader() {
    self.viewController?.hideLoader()
  }
  
}
