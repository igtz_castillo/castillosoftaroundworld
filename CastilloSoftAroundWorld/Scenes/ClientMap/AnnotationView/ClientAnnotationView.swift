//
//  ClientAnnotationView.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 30/07/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import MapKit

class ClientAnnotationView: UIView {
  
  @IBOutlet weak var warehouseLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var timeTravelLabel: UILabel!
  
  class func initView() -> ClientAnnotationView {
    let myClassNib = UINib.init(nibName: "ClientAnnotationView", bundle: nil)
    return myClassNib.instantiate(withOwner: nil, options: nil)[0] as! ClientAnnotationView
  }
  
}
