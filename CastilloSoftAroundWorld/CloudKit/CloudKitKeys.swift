//
//  CloudKitKeys.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 05/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation

enum CloudKitKeys {

  enum Account: String {
    case Account
    case clientNameWhoIsOwner
    case clientOwner
    case clientWarehouseNumber
    case coreDataID
    case finalDebt
    case hasPayments
    case thisAccountHasThisArticles
  }
    
  enum Article: String {
    case Article
    case brandName
    case coreDataID
    case cost
    case nameArticle
    case purchaseDateOfThisArticle
    case searchingWords
    case selectedCharacteristicsDictionaryKeys
    case selectedCharacteristicsDictionaryValues
    case thisArticleIsAddedIntoThisAccount
    case thisArticleIsThisArticleType
    case isAddedInAnAccount
  }
  
  enum ArticleType: String {
    case ArticleType
    case articleTypeComment
    case articleTypeName
    case articleTypeOfManyArticles
    case coreDataID
    case thisArticleTypeHasTheseCharacteristics
  }
  
  enum ArticleTypeCharacteristic: String {
    case ArticleTypeCharacteristic
    case articleTypeCharacteristicComment
    case articleTypeCharacteristicName
    case coreDataID
    case thisCharacteristicHasTheseOptions
    case thisCharacteristicIsForThisArticleType
  }
  
  enum ArticleTypeCharacteristicOption: String {
    case ArticleTypeCharacteristicOption
    case articleTypeCharacteristicOptionName
    case coreDataID
    case optionOfThisArticleTypeCharacteristic
  }
  
  enum Client: String {
    case Client
    case address
    case cellphoneNumber
    case clientName
    case comment
    case coreDataID
    case dayOfTheWeekThatPays
    case dateOfLastPayment
    case hasOneAccount
    case isGone
    case location
    case nextDateToVisit
    case phoneNumber
    case stillHasDebt
    case surnames
    case todayWasAlreadyVisited
    case warehouseNumber
  }
  
  enum Payment: String {
    case Payment
    case ammountPaid
    case coreDataID
    case dayOfPayment
    case paymentComment
    case thisPaymentIsAssociatedWithThisAccount
  }
  
  enum OutstandingIssue: String {
    case OutstandingIssue
    case outstandingIssueDescription
    case isAlreadyDone
    case dateAlarm
    case audioDescription
    case createdAt
  }
  
}

