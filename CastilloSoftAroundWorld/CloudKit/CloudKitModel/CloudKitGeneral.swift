//
//  CloudKitGeneral.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 09/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CloudKit
import PromiseKit

extension CloudKitModel {
    
    func uploadAllDatabase() -> Promise<[CKRecord]>{
        return Promise { result in
            var allRecordsIDToDelete = [CKRecordID]()
            var allCreatedRecords = [CKRecord]()
            self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue)
                .then { (clients) -> Promise<[CKRecord]> in
                    print("Fetched \(clients.count) clients")
                    allRecordsIDToDelete = allRecordsIDToDelete + clients.map({client in client.recordID})
                    return self.fetchRecords(forType: CloudKitKeys.Account.Account.rawValue)
                }.then { (accounts) -> Promise<[CKRecord]> in
                    print("Fetched \(accounts.count) accounts")
                    allRecordsIDToDelete = allRecordsIDToDelete + accounts.map({account in account.recordID})
                    return self.fetchRecords(forType: CloudKitKeys.Payment.Payment.rawValue)
                }.then { (payments) -> Promise<[CKRecord]> in
                    print("Fetched \(payments.count) payments")
                    allRecordsIDToDelete = allRecordsIDToDelete + payments.map({payment in payment.recordID})
                    return self.fetchRecords(forType: CloudKitKeys.ArticleType.ArticleType.rawValue)
                }.then { (articleTypes) -> Promise<[CKRecord]> in
                    print("Fetched \(articleTypes.count) articleTypes")
                    allRecordsIDToDelete = allRecordsIDToDelete + articleTypes.map({articleType in articleType.recordID})
                    return CloudKitModel.sharedInstance.fetchRecords(forType: CloudKitKeys.ArticleTypeCharacteristic.ArticleTypeCharacteristic.rawValue)
                }.then { (articleTypesCharacteristics) -> Promise<[CKRecord]> in
                    print("Fetched \(articleTypesCharacteristics.count) characteristics of articleType")
                    allRecordsIDToDelete = allRecordsIDToDelete + articleTypesCharacteristics.map({articleTypeCharacteristic in articleTypeCharacteristic.recordID})
                    return CloudKitModel.sharedInstance.fetchRecords(forType: CloudKitKeys.ArticleTypeCharacteristicOption.ArticleTypeCharacteristicOption.rawValue)
                }.then { (characteristicOptions) -> Promise<[CKRecord]> in
                    print("Fetched \(characteristicOptions.count) characteristic options")
                    allRecordsIDToDelete = allRecordsIDToDelete + characteristicOptions.map({option in option.recordID})
                    return CloudKitModel.sharedInstance.fetchRecords(forType: CloudKitKeys.Article.Article.rawValue)
                }.then { (allArticles) -> Promise<[CKRecord]> in
                    print("Fetched \(allArticles.count) articles")
                    allRecordsIDToDelete = allRecordsIDToDelete + allArticles.map({article in article.recordID})
                    return CloudKitModel.sharedInstance.fetchAllOutstandingIssues()
                }.then { (allOutstandingIssues) -> Promise<[CKRecordID]> in
                    allRecordsIDToDelete = allRecordsIDToDelete + allOutstandingIssues.map({outstangingIssue in outstangingIssue.recordID})
                    return CloudKitModel.sharedInstance.deleteAllTheseRecordsPromising(initialValue: 0, rangeToUpdate: 350, recordsToDelete: allRecordsIDToDelete)
                }.then { (allRecordsDeleted) -> Promise<[CKRecord]> in
                    print("There were deleted \(allRecordsDeleted.count) records")
                    return CloudKitModel.sharedInstance.uploadClientsWithAccountsAndPaymentsWithPromise()
                }.then({ (allClientsAccountsAndPaymentsCreated) -> Promise<[CKRecord]> in
                    allCreatedRecords = allCreatedRecords + allClientsAccountsAndPaymentsCreated
                    print("There were created \(allClientsAccountsAndPaymentsCreated.count) records while creating all the Clients and their information")
                    return CloudKitModel.sharedInstance.uploadTypeArticleWithCharacteristicsAndOptionsWithPromises()
                }).then({ (allArticlePaymentsWithAllTheirInformationRecordsCreated) -> Promise<[CKRecord]> in
                    allCreatedRecords = allCreatedRecords + allArticlePaymentsWithAllTheirInformationRecordsCreated
                    print("There were created \(allArticlePaymentsWithAllTheirInformationRecordsCreated.count) records while creating all the articleTypes and their information")
                    return CloudKitModel.sharedInstance.uploadAllArticlesWithPromises()
                }).then({ (allArticlesWithAllTheirInformationCreated) -> Promise<[CKRecord]> in
                    allCreatedRecords = allCreatedRecords + allArticlesWithAllTheirInformationCreated
                    return CloudKitModel.sharedInstance.uploadOutstandingIssues()
                }).done({ allUploadOutstanding in
                    allCreatedRecords = allCreatedRecords + allUploadOutstanding
                    print("There were created \(allCreatedRecords.count) records while creating all the articles and their information")
                    return result.fulfill(allCreatedRecords)
                }).catch { (error) in
                    print(error)
                    return result.reject(error)
            }
        }
    }
  
  func uploadJustArticles() -> Promise<[CKRecord]> {
    return Promise { result in
      self.uploadAllArticlesWithPromises().done({ (recordsCreated) in
        return result.fulfill(recordsCreated)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  func createOnlyNotCreatedDatabaseBegginingInClientNumber(clientNumber: Int) -> Promise<[CKRecord]> {
    return Promise {  result in
      var allCreatedRecords = [CKRecord]()
      CloudKitModel.sharedInstance.uploadClientsWithAccountsAndPaymentsWithPromise()
        .then { (allClientsAccountsAndPaymentsCreated) -> Promise<[CKRecord]> in
          allCreatedRecords = allCreatedRecords + allClientsAccountsAndPaymentsCreated
          print("There were created \(allClientsAccountsAndPaymentsCreated.count) records while creating all the Clients and their information")
          return CloudKitModel.sharedInstance.uploadTypeArticleWithCharacteristicsAndOptionsWithPromises()
        }.then { (allTypeArticleWithAllTheirInformationRecordsCreated) -> Promise<[CKRecord]> in
          allCreatedRecords = allCreatedRecords + allTypeArticleWithAllTheirInformationRecordsCreated
          print("There were created \(allTypeArticleWithAllTheirInformationRecordsCreated.count) records while creating all the articleTypes and their information")
          return CloudKitModel.sharedInstance.uploadAllArticlesWithPromises()
        }.then { (allArticlesWithAllTheirInformationCreated) -> Promise<[CKRecord]> in
          allCreatedRecords = allCreatedRecords + allArticlesWithAllTheirInformationCreated
          return CloudKitModel.sharedInstance.uploadOutstandingIssues()
        }.done({ allUploadOutstanding in
          allCreatedRecords = allCreatedRecords + allUploadOutstanding
          print("There were created \(allCreatedRecords.count) records while creating all the articles and their information")
          return result.fulfill(allCreatedRecords)
        }).catch { (error) in
          print(error)
          return result.reject(error)
      }
    }
    
  }
  
  func deleteAllElementsOf(type: String) -> Promise<[CKRecordID]> {
    return Promise { result in
      self.fetchRecords(forType: type).done({ (recordsToDelete) in
        let recordsIDToDelete = recordsToDelete.map({ $0.recordID })
        CloudKitModel.sharedInstance.deleteAllTheseRecordsPromising(initialValue: 0, rangeToUpdate: 350, recordsToDelete: recordsIDToDelete).done({ (recordsDeleted) in
          return result.fulfill(recordsDeleted)
          
        }).catch({ (error) in
          return result.reject(error)
        })
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  
  
  
  
  
  
  
  
  
  
  
    
  func fetchRecords(forType type: String, sortDescriptors: [NSSortDescriptor]? = nil, predicate: NSPredicate? = nil) -> Promise<[CKRecord]> {
    return Promise { result in
      var firstLoopRecords = [CKRecord]()
      var query = CKQuery(recordType: type, predicate: NSPredicate(value: true))
      if predicate != nil {
        query = CKQuery(recordType: type, predicate: predicate!)
      }
      query.sortDescriptors = sortDescriptors
      let queryOperation = CKQueryOperation(query: query)
      //queryOperation.zoneID = CloudAssistant.shared.zone.zoneID
      queryOperation.recordFetchedBlock = { record in
        firstLoopRecords.append(record)
      }
      queryOperation.queryCompletionBlock = { cursor, error in
          self.fetchRecords(with: cursor, error: error).done({ (records) in
          let finalRecords = records + firstLoopRecords
          return result.fulfill(finalRecords)
        }).catch({ (error) in
          return result.reject(error)
        })
      }
      self.selectedDatabase.add(queryOperation)
    }
  }
    
  private func fetchRecords(with cursor: CKQueryCursor?, error: Swift.Error?) -> Promise<[CKRecord]> {
    return Promise { result in
      var currentRecords: Array<CKRecord> = [CKRecord]()
      if let cursor = cursor, error == nil {
        let queryOperation = CKQueryOperation(cursor: cursor)
        queryOperation.recordFetchedBlock = { record in
          currentRecords.append(record)
        }
        queryOperation.queryCompletionBlock = { cursor, error in
          self.fetchRecords(with: cursor, error: error).done({ (records) in
            return result.fulfill(currentRecords + records)
          }).catch({ (errorFetch) in
            return result.reject(errorFetch)
          })
        }
        self.selectedDatabase.add(queryOperation)
      } else if cursor == nil, error == nil {
        return result.fulfill(currentRecords)
      } else if let error = error {
        return result.reject(error)
      }
    }
  }
  
  func updateTheseRecords(recordsToUpdate: [CKRecord]) -> Promise<[CKRecord]> {
    return Promise { result in
      let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: recordsToUpdate, recordIDsToDelete: nil)
      operationToSaveSomeRecords.queuePriority = .veryHigh
      operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
        if let error = error {
          return result.reject(error)
        } else
          if let recordsChanged = recordsChanged {
            return result.fulfill(recordsChanged)
        }
      }
      self.selectedDatabase.add(operationToSaveSomeRecords)
    }
  }
  
    
}
