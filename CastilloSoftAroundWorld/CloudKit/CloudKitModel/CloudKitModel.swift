//
//  CloudKitModel.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 04/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CloudKit
import PromiseKit

enum DatabaseTypeToUse {
  case Private
  case Public
}

class CloudKitModel {
    
  static let sharedInstance = CloudKitModel()
  
  let kPrivateDatabase = "privateDatabaseSelected"
  let kPublicDatabase = "publicDatabaseSelected"
  
  // Represents the default container specified in the iCloud section of the Capabilities tab for the project.
  let container: CKContainer!
  let publicDB: CKDatabase!
  let privateDB: CKDatabase!
  var selectedDatabase: CKDatabase! {
    didSet {
      self.notificateToObserver()
    }
  }
  let database = CKContainer(identifier: "iCloud.com.IsraelGutierrez.CastilloSoftAroundWorld").privateCloudDatabase
  
  init() {
    container = CKContainer.default()
    publicDB = container.publicCloudDatabase
    privateDB = container.privateCloudDatabase
    selectedDatabase = getDatabaseSelected()
  }
  
  private func getDatabaseSelected() -> CKDatabase {
    if UserDefaults.standard.bool(forKey: kPrivateDatabase) {
      return self.privateDB
    } else
    if UserDefaults.standard.bool(forKey: kPublicDatabase) {
      return self.publicDB
    } else {
     return self.privateDB
    }
  }
  
  func setDatabaseTypeToUse(type: DatabaseTypeToUse) {
    switch type {
    case .Private:
      self.selectedDatabase = self.privateDB
      UserDefaults.standard.set(true, forKey: kPrivateDatabase)
      UserDefaults.standard.set(false, forKey: kPublicDatabase)
    case .Public:
      self.selectedDatabase = self.publicDB
      UserDefaults.standard.set(true, forKey: kPublicDatabase)
      UserDefaults.standard.set(false, forKey: kPrivateDatabase)
    }
  }
  
  private func notificateToObserver() {
    NotificationCenter.default.post(name: .databaseSelectedChanged, object: nil, userInfo: nil)
  }
    
}
