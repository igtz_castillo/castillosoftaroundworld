//
//  CloudKitOutstandingIssue.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel Gtz on 04/07/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import PromiseKit
import CoreData
import CloudKit

extension CloudKitModel {
  
  func createOutstandingIssue(with description: String) -> Promise<CKRecord> {
    return Promise { result in
      let newOutstanding = CKRecord(recordType: CloudKitKeys.OutstandingIssue.OutstandingIssue.rawValue)
      newOutstanding[CloudKitKeys.OutstandingIssue.outstandingIssueDescription.rawValue] = description as CKRecordValue
      newOutstanding[CloudKitKeys.OutstandingIssue.isAlreadyDone.rawValue] = 0 as CKRecordValue
      
      let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: [newOutstanding], recordIDsToDelete: nil)
      operationToSaveSomeRecords.queuePriority = .veryHigh
      operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
        if let error = error {
          print("Error when updating: \(error)")
          return result.reject(error)
        } else if let recordsChanged = recordsChanged, recordsChanged.last != nil {
          return result.fulfill(recordsChanged.last!)
        }
      }
      self.selectedDatabase.add(operationToSaveSomeRecords)
    }
  }
  
  func fetchAllOutstandingIssues() -> Promise<[CKRecord]> {
    return Promise { result in
      self.fetchRecords(forType: CloudKitKeys.OutstandingIssue.OutstandingIssue.rawValue).done { (allOutstandingRecords) in
        return result.fulfill(allOutstandingRecords)
      }.catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  func uploadOutstandingIssues() -> Promise<[CKRecord]> {
    return Promise { result in
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let context = appDelegate.persistentContainer.viewContext
      
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TareaPorRealizar")
      request.returnsObjectsAsFaults = false
      do {
        let dataArray = try context.fetch(request)
        let outstandingIssues = dataArray as! [TareaPorRealizar]
        let index = 0
        self.outstandingIssueToCreate(index: index, outstandingIssues: outstandingIssues, createdOutstandingIssues: [CKRecord]()).done({ (createdOutstandingIssues) in
          return result.fulfill(createdOutstandingIssues)
        }).catch({ (error) in
          return result.reject(error)
        })
      } catch {
        let error = NSError.init(domain: "Failed to load clients from coredata", code: -1, userInfo: nil)
        print("Failed to load clientes from coredata")
        return result.reject(error)
      }
    }
  }
  
  private func outstandingIssueToCreate(index: Int, outstandingIssues: [TareaPorRealizar], createdOutstandingIssues: [CKRecord]) -> Promise<[CKRecord]> {
    return Promise { result in
      var finalCreatedOutstandingIssues = createdOutstandingIssues
      if index <= outstandingIssues.count - 1 {
        self.createOutstandingIssueWith(standingIssueData: outstandingIssues[index]).done({ (recordCreated) in
          print("outstandingIssue number \(index + 1) created")
          finalCreatedOutstandingIssues.append(recordCreated)
          self.outstandingIssueToCreate(index: index + 1, outstandingIssues: outstandingIssues, createdOutstandingIssues: finalCreatedOutstandingIssues).done({ (outstandingCreated) in
            return result.fulfill(outstandingCreated)
          }).catch({ (error) in
            print("Error creating outstandingIssue number \(index). Last outstandingIssue created was: \(finalCreatedOutstandingIssues.last?.description ?? "No outstandingIssue created")")
            return result.reject(error)
          })
        }).catch({ (error) in
          print("Error creating outstandingIssues. Last outstandingIssue created was: \(finalCreatedOutstandingIssues.last?.description ?? "No outstandingIssue created")")
          return result.reject(error)
        })
      } else {
        return result.fulfill(createdOutstandingIssues)
      }
    }
  }
  
  private func createOutstandingIssueWith(standingIssueData: TareaPorRealizar) -> Promise<CKRecord> {
    return Promise { result in
      let isAlreadyDoneNumber: NSNumber = (standingIssueData.yaFueRealizado != nil && standingIssueData.yaFueRealizado != "" &&  (standingIssueData.yaFueRealizado?.lowercased() == "si" || standingIssueData.yaFueRealizado?.lowercased() == "sí")) ? 1 : 0
      let newOutstandingIssue = CKRecord(recordType: CloudKitKeys.OutstandingIssue.OutstandingIssue.rawValue)
      newOutstandingIssue[CloudKitKeys.OutstandingIssue.outstandingIssueDescription.rawValue] = ((standingIssueData.descripcionTareaPorRealizar) ?? "") as CKRecordValue
      newOutstandingIssue[CloudKitKeys.OutstandingIssue.isAlreadyDone.rawValue] = isAlreadyDoneNumber as CKRecordValue
      
      let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: [newOutstandingIssue], recordIDsToDelete: nil)
      operationToSaveSomeRecords.queuePriority = .veryHigh
      operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
        if let error = error {
          print("Error when updating: \(error)")
          return result.reject(error)
        } else if let recordsChanged = recordsChanged, recordsChanged.last != nil {
          return result.fulfill(recordsChanged.last!)
        }
      }
      self.selectedDatabase.add(operationToSaveSomeRecords)
    }
  }
  
  func getOutstandingIssueCloudKitData(from ckrecord: CKRecord) -> CloudKitOutstandingIssue {
    let newOutstandingIssue = CloudKitOutstandingIssue()
    newOutstandingIssue.outstandingIssueDescription = (ckrecord[CloudKitKeys.OutstandingIssue.outstandingIssueDescription.rawValue] as? String) ?? ""
    newOutstandingIssue.isAlreadyDone = (ckrecord[CloudKitKeys.OutstandingIssue.isAlreadyDone.rawValue] as? NSNumber) ?? 0
    newOutstandingIssue.dateAlarm = ckrecord[CloudKitKeys.OutstandingIssue.dateAlarm.rawValue] as? Date
    newOutstandingIssue.rawDataFromServer = ckrecord
    return newOutstandingIssue
  }
  
  func markLikeFinishedOutstandingIssue(outstandingIssueData: CloudKitOutstandingIssue) -> Promise<[CKRecord]> {
    return Promise { result in
      self.selectedDatabase.fetch(withRecordID: outstandingIssueData.rawDataFromServer!.recordID, completionHandler: { (fetchedOutstandingIssueRecord, error) in
        if let fetchedOutstandingIssueRecord = fetchedOutstandingIssueRecord {
          fetchedOutstandingIssueRecord[CloudKitKeys.OutstandingIssue.isAlreadyDone.rawValue] = 1 as CKRecordValue
          self.updateTheseRecords(recordsToUpdate: [fetchedOutstandingIssueRecord]).done({ (updatedOutstandingIssuerecords) in
            return result.fulfill(updatedOutstandingIssuerecords)
          }).catch({ (error) in
            return result.reject(error)
          })
        } else
        if let error = error {
         return result.reject(error)
        }
      })
    }
  }
  
}

class CloudKitOutstandingIssue {
  var outstandingIssueDescription: String?
  var isAlreadyDone: NSNumber?
//  var audioDescription:
  var dateAlarm: Date?
  var rawDataFromServer: CKRecord?
  
  init(){}
}

extension CloudKitOutstandingIssue {
  
  @objc func value(forKey key: String) -> Any? {
    switch key {
    case "outstandingIssueDescription":
      return self.outstandingIssueDescription
    default:
      return nil
    }
  }
  
}
