//
//  CloudKitPayment.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 10/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CloudKit
import PromiseKit

extension CloudKitModel {
    //MARK: - Payments Section
    func getAllPaymentsFromCloudKit() -> Promise<[CKRecord]> {
        return Promise { result in
            let predicate = NSPredicate(value: true)
            let query = CKQuery(recordType: CloudKitKeys.Payment.Payment.rawValue, predicate: predicate)
            self.selectedDatabase.perform(query, inZoneWith: nil) { (payments, error) in
                if let payments = payments {
                    result.fulfill(payments)
                } else if let error = error {
                    result.reject(error)
                }
            }
        }
    }
  
  func getAllPaymentsInThisAccount(account: CKReference) -> Promise<[CKRecord]> {
    return Promise { result in
      let predicate = NSPredicate(format: "\(CloudKitKeys.Payment.thisPaymentIsAssociatedWithThisAccount.rawValue) == %@", account)
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Payment.dayOfPayment.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Payment.Payment.rawValue, sortDescriptors: [sortDescriptor], predicate: predicate).done({ (allArticles) in
        return result.fulfill(allArticles)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  func getCloudKitPaymentFromServerData(recordFromCloudKit: CKRecord) -> CloudKitPayment {
    let newPayment = CloudKitPayment()
    newPayment.ammountPaid = recordFromCloudKit[CloudKitKeys.Payment.ammountPaid.rawValue] as! String
    newPayment.coreDataID = recordFromCloudKit[CloudKitKeys.Payment.coreDataID.rawValue] as! String
    newPayment.dayOfPayment = recordFromCloudKit[CloudKitKeys.Payment.dayOfPayment.rawValue] as! Date
    newPayment.paymentComment = recordFromCloudKit[CloudKitKeys.Payment.paymentComment.rawValue] as! String
    newPayment.thisPaymentIsAssociatedWithThisAccount = recordFromCloudKit[CloudKitKeys.Payment.thisPaymentIsAssociatedWithThisAccount.rawValue] as? CKReference
    newPayment.rawDataFromServer = recordFromCloudKit
    return newPayment
  }
  
}

class CloudKitPayment {
  
  var ammountPaid: String = ""
  var coreDataID: String = ""
  var dayOfPayment: Date!
  var paymentComment: String = ""
  var thisPaymentIsAssociatedWithThisAccount: CKReference?
  var rawDataFromServer: CKRecord?
  
  init() {}
  
}
