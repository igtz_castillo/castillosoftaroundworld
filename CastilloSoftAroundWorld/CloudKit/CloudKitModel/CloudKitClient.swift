//
//  CloudKitClient.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 09/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CoreData
import CloudKit
import PromiseKit
import CoreLocation

extension CloudKitModel {
    
  //MARK: - Clients Section
  
  func getAllCloudKitClients() -> Promise<[CloudKitClient]> {
    return Promise { result in
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Client.clientName.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor]).done({ (recordsFromCloudKit) in
        let allCloudKitClients: [CloudKitClient] = recordsFromCloudKit.map({ (recordFromCloudKit) -> CloudKitClient in
          return self.getCloudKitClientFromServerData(recordFromCloudKit: recordFromCloudKit)
        })
        return result.fulfill(allCloudKitClients)
      }).catch({ (error) in
        print("Error traying fetching all clients. Error: \(error) ")
        return result.reject(error)
      })
    }
  }
  
  func getClientsThatDontPaySinceSevenDaysAgo() -> Promise<[CloudKitClient]> {
    return Promise { result in
      let today = Date()
      let sevenDaysAgo = Calendar.current.date(byAdding: .day, value: -7, to: today)
      let predicateSinceDay = NSPredicate(format: "\(CloudKitKeys.Client.dateOfLastPayment.rawValue) < %@", sevenDaysAgo! as CVarArg )
      let predicateNotGone = NSPredicate(format: "\(CloudKitKeys.Client.isGone.rawValue) == %i", 0)
      let finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateSinceDay, predicateNotGone])
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Client.clientName.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: finalPredicate).done({ (allFilteredClients) in
        let arrayOfOutstandingIssues: [CloudKitClient] = allFilteredClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
        return result.fulfill(arrayOfOutstandingIssues)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  //  func getAllClientsThatPayToday() -> Promise<[CloudKitClient]> {
  //    return Promise { result in
  //      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Client.clientName.rawValue, ascending: true)
  //      let predicateNextDateToVisit = NSPredicate(format: "\(CloudKitKeys.Client.nextDateToVisit.rawValue) == %@ ", Date.today().getStringFromDate(dateStyle: .full))
  //      let predicateClientsTodaysNamePay = NSPredicate(format: "\(CloudKitKeys.Client.dayOfTheWeekThatPays.rawValue) == %@ ", Date().getTodaysName())
  //      let finalPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [predicateNextDateToVisit, predicateClientsTodaysNamePay])
  //      self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: finalPredicate).done({ (recordsFromCloudKit) in
  //        let allCloudKitClients: [CloudKitClient] = recordsFromCloudKit.map({ (recordFromCloudKit) -> CloudKitClient in
  //          return self.getCloudKitClientFromServerData(recordFromCloudKit: recordFromCloudKit)
  //        })
  //        return result.fulfill(allCloudKitClients)
  //      }).catch({ (error) in
  //        print("Error traying fetching all clients. Error: \(error) ")
  //        return result.reject(error)
  //      })
  //    }
  //  }
  
  private func getCloudKitClientFromServerData(recordFromCloudKit: CKRecord) -> CloudKitClient {
    let newClient = CloudKitClient()
    newClient.address = recordFromCloudKit[CloudKitKeys.Client.address.rawValue] as! String
    newClient.cellphoneNumber = recordFromCloudKit[CloudKitKeys.Client.cellphoneNumber.rawValue] as! String
    newClient.clientName = recordFromCloudKit[CloudKitKeys.Client.clientName.rawValue] as! String
    newClient.comment = recordFromCloudKit[CloudKitKeys.Client.comment.rawValue] as! String
    newClient.coreDataID = recordFromCloudKit[CloudKitKeys.Client.coreDataID.rawValue] as! String
    newClient.dayOfTheWeekThatPays = recordFromCloudKit[CloudKitKeys.Client.dayOfTheWeekThatPays.rawValue] as! String
    newClient.dateOfLastPayment = recordFromCloudKit[CloudKitKeys.Client.dateOfLastPayment.rawValue] as? Date
    newClient.hasOneAccount = recordFromCloudKit[CloudKitKeys.Client.hasOneAccount.rawValue] as? CKReference ?? nil
    newClient.isGone = (recordFromCloudKit[CloudKitKeys.Client.isGone.rawValue] as? NSNumber) ?? 0
    newClient.location = recordFromCloudKit[CloudKitKeys.Client.location.rawValue] as? CLLocation ?? nil
    newClient.nextDateToVisit = recordFromCloudKit[CloudKitKeys.Client.nextDateToVisit.rawValue] as! String
    newClient.phoneNumber = recordFromCloudKit[CloudKitKeys.Client.phoneNumber.rawValue] as! String
    newClient.stillHasDebt = recordFromCloudKit[CloudKitKeys.Client.stillHasDebt.rawValue] as! String
    newClient.surnames = recordFromCloudKit[CloudKitKeys.Client.surnames.rawValue] as! String
    newClient.todayWasAlreadyVisited = recordFromCloudKit[CloudKitKeys.Client.todayWasAlreadyVisited.rawValue] as! String
    newClient.wareHouseNumber = recordFromCloudKit[CloudKitKeys.Client.warehouseNumber.rawValue] as! String
    newClient.rawDataFromServer = recordFromCloudKit
    return newClient
  }
  
  func getAllClientsFromCloudKit() -> Promise<[CKRecord]> {
    return Promise { result in
      let predicate = NSPredicate(value: true)
      let query = CKQuery(recordType: CloudKitKeys.Client.Client.rawValue, predicate: predicate)
      self.selectedDatabase.perform(query, inZoneWith: nil) { (clients, error) in
        if let clients = clients {
          result.fulfill(clients)
        } else if let error = error {
          result.reject(error)
        }
      }
    }
  }
  
  private func uploadClientsWithAccountsAndPayments() {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = appDelegate.persistentContainer.viewContext
    
    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
    //request.predicate = NSPredicate(format: "age = %@", "12")
    request.returnsObjectsAsFaults = false
    do {
      let result = try context.fetch(request)
      let clientes = result as! [Cliente]
      for i in 0...19 {
        print("begin")
        let cliente = clientes[i]
        let cuentas = cliente.tieneMuchas?.array as! [Cuenta]
        let pagos = cuentas[0].tieneMuchos?.array as! [Pago]
        self.createClientWith(clientData: cliente, accountData: cuentas[0], associatedPayments: pagos).done({ (allRecordsChanged) in
          print(allRecordsChanged)
        }).ensure({
          print("finished everything")
        }).catch({ (error) in
          print(error)
        })
        usleep(500000)
        print("end")
      }
    } catch {
      print("Failed")
    }
  }
  
  func uploadClientsWithAccountsAndPaymentsWithPromise() -> Promise<[CKRecord]> {
    return Promise { result in
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
      let context = appDelegate.persistentContainer.viewContext
      
      let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Cliente")
      request.returnsObjectsAsFaults = false
      do {
        let dataArray = try context.fetch(request)
        let clientes = dataArray as! [Cliente]
        let index = 0 //clientes.count - 50
        self.clientToCreate(index: index, clients: clientes, createdClients: [CKRecord]()).done({ (clientsWithAllTheirInformationCreated) in
          return result.fulfill(clientsWithAllTheirInformationCreated)
        }).catch({ (error) in
          return result.reject(error)
        })
      } catch {
        let error = NSError.init(domain: "Failed to load clients from coredata", code: -1, userInfo: nil)
        print("Failed to load clientes from coredata")
        return result.reject(error)
      }
    }
  }
  
  private func clientToCreate(index: Int, clients: [Cliente], createdClients: [CKRecord]) -> Promise<[CKRecord]> {
    return Promise { result in
      var finalCreatedClients = createdClients
      if index <= clients.count - 1 {
        let cuentas = clients[index].tieneMuchas?.array as! [Cuenta]
        let pagos = cuentas[0].tieneMuchos?.array as! [Pago]
        self.createClientWith(clientData: clients[index], accountData: cuentas[0], associatedPayments: pagos).done({ (recordsCreated) in
          print("Client number \(index + 1) created")
          finalCreatedClients = finalCreatedClients + recordsCreated
          self.clientToCreate(index: index + 1, clients: clients, createdClients: finalCreatedClients).done({ (clientsWithAllItsInformationCreated) in
            return result.fulfill(clientsWithAllItsInformationCreated)
          }).catch({ (error) in
            print("Error creating client number \(index). Last client created was: \(finalCreatedClients.last?.description ?? "No client created")")
            return result.reject(error)
          })
        }).catch({ (error) in
          print("Error creating clients. Last client created was: \(finalCreatedClients.last?.description ?? "No client created")")
          return result.reject(error)
        })
      } else {
        return result.fulfill(createdClients)
      }
    }
  }
  
  
  
  private func createClientWith(clientData: Cliente, accountData: Cuenta, associatedPayments: [Pago]) -> Promise<[CKRecord]>{
    return Promise { result in
      var arrayOfAllNewRecords: [CKRecord] = [CKRecord]()
      
      //Create new account
      let newAccount = CKRecord.init(recordType: CloudKitKeys.Account.Account.rawValue)
      newAccount[CloudKitKeys.Account.coreDataID.rawValue] = accountData.objectID.uriRepresentation().absoluteString as CKRecordValue
      newAccount[CloudKitKeys.Account.clientNameWhoIsOwner.rawValue] = (clientData.nombre ?? "") as CKRecordValue
      newAccount[CloudKitKeys.Account.finalDebt.rawValue] = (accountData.deudaFinal ?? "") as CKRecordValue
      newAccount[CloudKitKeys.Account.clientWarehouseNumber.rawValue] = (clientData.numeroDeBodega ?? "") as CKRecordValue
      let newAccountReference = CKReference.init(record: newAccount, action: CKReferenceAction.none)
      arrayOfAllNewRecords.append(newAccount)
      
      //Create new payments
      var arrayOfNewPaymentReferences: [CKReference] = Array<CKReference>()
      for pago in associatedPayments {
        let newPayment = CKRecord.init(recordType: CloudKitKeys.Payment.Payment.rawValue)
        newPayment[CloudKitKeys.Payment.ammountPaid.rawValue] = (pago.cantidadPagada ?? "") as CKRecordValue
        newPayment[CloudKitKeys.Payment.coreDataID.rawValue] = pago.objectID.uriRepresentation().absoluteString as CKRecordValue
        newPayment[CloudKitKeys.Payment.dayOfPayment.rawValue] = (pago.fechaDePagoRealizado ?? NSDate.init()) as CKRecordValue
        newPayment[CloudKitKeys.Payment.paymentComment.rawValue] = (pago.comentarioDePago ?? "") as CKRecordValue
        newPayment[CloudKitKeys.Payment.thisPaymentIsAssociatedWithThisAccount.rawValue] = newAccountReference
        
        let newPaymentReference = CKReference.init(record: newPayment, action: CKReferenceAction.none)
        arrayOfNewPaymentReferences.append(newPaymentReference)
        arrayOfAllNewRecords.append(newPayment)
      }
      newAccount[CloudKitKeys.Account.hasPayments.rawValue] = arrayOfNewPaymentReferences as CKRecordValue
      let isGoneValue: NSNumber = clientData.seFue != nil && (clientData.seFue?.lowercased() == "si" || clientData.seFue?.lowercased() == "sí") ? 1 : 0
      
      //Create new client
      let newClient = CKRecord.init(recordType: CloudKitKeys.Client.Client.rawValue)
      newClient[CloudKitKeys.Client.clientName.rawValue] = clientData.nombre! as CKRecordValue
      newClient[CloudKitKeys.Client.address.rawValue] = (clientData.direccion ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.cellphoneNumber.rawValue] = (clientData.numeroCelular ?? "")  as CKRecordValue
      newClient[CloudKitKeys.Client.comment.rawValue] = (clientData.comentario ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.coreDataID.rawValue] = clientData.objectID.uriRepresentation().absoluteString as CKRecordValue
      newClient[CloudKitKeys.Client.dayOfTheWeekThatPays.rawValue] = (clientData.diaDeLaSemanaQuePaga ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.isGone.rawValue] = isGoneValue as CKRecordValue
      newClient[CloudKitKeys.Client.nextDateToVisit.rawValue] = (clientData.siguienteFechaEnQueSeVisitara ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.phoneNumber.rawValue] = (clientData.telefonoCasa ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.stillHasDebt.rawValue] = (clientData.aunDebe ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.surnames.rawValue] = (clientData.apellidos ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.todayWasAlreadyVisited.rawValue] = (clientData.yaFueVisitadoElDiaDeHoy ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.warehouseNumber.rawValue] = (clientData.numeroDeBodega ?? "") as CKRecordValue
      if associatedPayments.last != nil {
        newClient[CloudKitKeys.Client.dateOfLastPayment.rawValue] = (associatedPayments.last!.fechaDePagoRealizado! as Date) as CKRecordValue
      } else {
        newClient[CloudKitKeys.Client.dateOfLastPayment.rawValue] = nil
      }
      
      newClient[CloudKitKeys.Client.hasOneAccount.rawValue] = CKReference.init(record: newAccount, action: CKReferenceAction.none)
      newAccount[CloudKitKeys.Account.clientOwner.rawValue] = CKReference.init(record: newClient, action: CKReferenceAction.none)
      arrayOfAllNewRecords.append(newClient)
      
      let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: arrayOfAllNewRecords, recordIDsToDelete: nil)
      operationToSaveSomeRecords.queuePriority = .veryHigh
      operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
        if let error = error {
          print("Error when updating: \(error)")
          result.reject(error)
        } else if let recordsChanged = recordsChanged {
          result.fulfill(recordsChanged)
        }
      }
      operationToSaveSomeRecords.completionBlock = {
        //                print("Function \"createClientWith\" finished")
      }
      self.selectedDatabase.add(operationToSaveSomeRecords)
    }
  }
  
  func updateClientInfo(request: ClientInfo.UpdateClientInfo.Request) -> Promise<[CKRecord]> {
    return Promise { result in
      
      self.selectedDatabase.fetch(withRecordID: request.rawDataFromServerThatWillBeChanged.recordID, completionHandler: { (recordFetched, error) in
        if let record = recordFetched {
          record[CloudKitKeys.Client.clientName.rawValue] = request.clientName! as CKRecordValue
          record[CloudKitKeys.Client.address.rawValue] = (request.clientAddress ?? "") as CKRecordValue
          record[CloudKitKeys.Client.cellphoneNumber.rawValue] = (request.clientCellPhone ?? "")  as CKRecordValue
          record[CloudKitKeys.Client.comment.rawValue] = (request.clientComment ?? "") as CKRecordValue
          record[CloudKitKeys.Client.dayOfTheWeekThatPays.rawValue] = (request.dayOfTheWeekThatPay ?? "") as CKRecordValue
          record[CloudKitKeys.Client.isGone.rawValue] = (request.clientIsGone ?? 0) as CKRecordValue
          //          record[CloudKitKeys.Client.nextDateToVisit.rawValue] = (request.nextDateToVisit ?? "") as CKRecordValue
          record[CloudKitKeys.Client.phoneNumber.rawValue] = (request.clientHomePhone ?? "") as CKRecordValue
          record[CloudKitKeys.Client.stillHasDebt.rawValue] = (request.stillHasDebt ?? "") as CKRecordValue
          record[CloudKitKeys.Client.surnames.rawValue] = (request.clientSurname ?? "") as CKRecordValue
          record[CloudKitKeys.Client.todayWasAlreadyVisited.rawValue] = (request.todayWasAlreadyVisited ?? "") as CKRecordValue
          record[CloudKitKeys.Client.warehouseNumber.rawValue] = (request.clientWarehouse ?? "") as CKRecordValue
          record[CloudKitKeys.Client.location.rawValue] = request.clientUbication != nil ? request.clientUbication! as CKRecordValue : nil
          self.selectedDatabase.save(record, completionHandler: { (updatedClientRecord, error) in
            if let updatedClientRecord = updatedClientRecord {
              let account = request.rawDataFromServerThatWillBeChanged[CloudKitKeys.Client.hasOneAccount.rawValue] as? CKReference
              if let accountCKReference = account {
                self.selectedDatabase.fetch(withRecordID: accountCKReference.recordID, completionHandler: { (fetchedAccountRecord, error) in
                  if let fetchedAccountRecord = fetchedAccountRecord {
                    fetchedAccountRecord[CloudKitKeys.Account.clientNameWhoIsOwner.rawValue] = request.clientName! as CKRecordValue
                    fetchedAccountRecord[CloudKitKeys.Account.clientWarehouseNumber.rawValue] = request.clientWarehouse! as CKRecordValue
                    self.selectedDatabase.save(fetchedAccountRecord, completionHandler: { (accountUpdated, error) in
                      if let accountUpdated = accountUpdated {
                        return result.fulfill([updatedClientRecord, accountUpdated])
                      } else
                        if let error = error {
                          return result.reject(error)
                      }
                    })
                  } else
                    if let error = error {
                      return result.reject(error)
                  }
                })
              }} else if let error = error {
              return result.reject(error)
            }
          })
        } else if let error = error {
          //check error
          return result.reject(error)
        }
      })
    }
  }
  
  func updateNextDayToVisit(request: DetailClientOptions.UpdateInfoToServer.Request)  -> Promise<CloudKitClient> {
    return Promise { result in
      
      if request.clientData.rawDataFromServer != nil {
        self.selectedDatabase.fetch(withRecordID: request.clientData.rawDataFromServer!.recordID, completionHandler: { (recordFetched, error) in
          if let record = recordFetched {
            record[CloudKitKeys.Client.nextDateToVisit.rawValue] = request.newNextDateToVisit as CKRecordValue
            self.selectedDatabase.save(record, completionHandler: { (record, error) in
              if let record = record {
                let clientData = self.getCloudKitClientFromServerData(recordFromCloudKit: record)
                return result.fulfill(clientData)
              } else if let error = error {
                return result.reject(error)
              }
            })
          } else if let error = error {
            return result.reject(error)
          }
        })
      } else {
        let error = NSError.init(domain: "Error al salvar la próxima fecha de visita al cliente", code: -1, userInfo: nil)
        return result.reject(error)
      }
    }
  }
  
  //    func deleteAllThisRecordsID(recordsIDToDelete: [CKRecordID]) -> Promise<[CKRecord]> {
  //        let maxNumberOfTransaction = 400
  //        var bottomLimit = 0
  //        var topLimit = maxNumberOfTransaction - 1
  //        var elementsToDelete: Array<CKRecordID> = [CKRecordID]()
  //        var deletedElements: Array<CKRecord> = [CKRecord]()
  //
  //        while topLimit <= recordsIDToDelete.count {
  //            for i in bottomLimit...topLimit {
  //                elementsToDelete.append(recordsIDToDelete[i])
  //            }
  //            //                delete
  //
  //            bottomLimit = topLimit + 1
  //            topLimit = topLimit + maxNumberOfTransaction
  //            elementsToDelete.removeAll()
  //            usleep(500000)
  //        }
  //
  //
  //        //We check if there are missing elements to delete. For example:
  //        //allClients.count = 700
  //        //after one loop in the while we have: bottomLimit = 400; topLimit = 799; so we still have 300 to delete
  //        //in the next "if" we compare 400 + 400 > 700; yes.
  //        //so we go from 400...(400 + (700-400)) that is equal to 400...700 that is the same of 400...allClients.count
  //        //
  //        if (bottomLimit + maxNumberOfTransaction) > recordsIDToDelete.count {
  //            for i in bottomLimit...(bottomLimit + (recordsIDToDelete.count - bottomLimit)) {
  //                elementsToDelete.append(recordsIDToDelete[i])
  //            }
  //            //                delete
  //
  //        } else {
  //            print("Deleted all elements")
  //        }
  //    }
  
  private func deleteArrayOfClients(arrayOfClientsToDelete: [CKRecord]) -> Promise<[CKRecord]> {
    
    return Promise { result in
      
      
      
      //            let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: nil, recordIDsToDelete: arrayOfClientsToDelete)
      //            operationToSaveSomeRecords.queuePriority = .veryHigh
      //            operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
      //                if let error = error {
      //                    print("Error when updating: \(error)")
      //                    result.reject(error)
      //                } else if let recordsChanged = recordsChanged {
      //                    print(recordsChanged)
      //                    result.fulfill(recordsChanged)
      //                }
      //            }
    }
  }
  
  
  func updateAllTheseRecordsPromising(initialValue: Int, rangeToUpdate: Int, recordsToUpdate: [CKRecord]) -> Promise<[CKRecord]> {
    return Promise { result in
      var arrayOfElementsToUpdate = [CKRecord]()
      if (initialValue > recordsToUpdate.count) || (recordsToUpdate.count == 0) {
        print("Finish deleting all elements")
        return result.fulfill([CKRecord]())
      }
      if initialValue + rangeToUpdate >= recordsToUpdate.count {
        print("Deleting from: \(initialValue) to \(recordsToUpdate.count - 1)")
        for i in initialValue...recordsToUpdate.count - 1 {
          arrayOfElementsToUpdate.append(recordsToUpdate[i])
        }
      } else {
        print("Deleting from: \(initialValue) to \(initialValue + rangeToUpdate - 1)")
        for i in initialValue...(initialValue + rangeToUpdate - 1) {
          arrayOfElementsToUpdate.append(recordsToUpdate[i])
        }
      }
      let operationToSomeRecords = CKModifyRecordsOperation.init(recordsToSave: recordsToUpdate, recordIDsToDelete: nil)
      operationToSomeRecords.queuePriority = .veryHigh
      operationToSomeRecords.modifyRecordsCompletionBlock = { _, recordsDeleted, error in
        if let recordsDeleted = recordsDeleted {
          //                    print("Deleted records: \(recordsDeleted)")
          self.updateAllTheseRecordsPromising(initialValue: initialValue + rangeToUpdate, rangeToUpdate: rangeToUpdate, recordsToUpdate: recordsToUpdate).done({ (recordsUpdatedFromPromise) in
            var finalRecordsUpdated = recordsToUpdate
            finalRecordsUpdated = finalRecordsUpdated + recordsUpdatedFromPromise
            return result.fulfill(finalRecordsUpdated)
          }).catch({ (error) in
            return result.reject(error)
          })
        } else if let error = error {
          print("Error trying to update from: \(initialValue) to \(initialValue + rangeToUpdate - 1)")
          print(error)
          return result.reject(error)
        } else {
          return result.fulfill([CKRecord]())
        }
      }
      self.selectedDatabase.add(operationToSomeRecords)
    }
  }
  
  func deleteAllTheseRecords(initialValue: Int, rangeToUpdate: Int, recordsToDelete: [CKRecordID]) {
    var arrayOfElementsToDelete = [CKRecordID]()
    if (initialValue > recordsToDelete.count) || (recordsToDelete.count == 0) {
      print("Finish deleting all elements")
      return
    }
    if initialValue + rangeToUpdate >= recordsToDelete.count {
      print("Deleting from: \(initialValue) to \(recordsToDelete.count - 1)")
      for i in initialValue...recordsToDelete.count - 1 {
        arrayOfElementsToDelete.append(recordsToDelete[i])
      }
    } else {
      print("Deleting from: \(initialValue) to \(initialValue + rangeToUpdate - 1)")
      for i in initialValue...(initialValue + rangeToUpdate - 1) {
        arrayOfElementsToDelete.append(recordsToDelete[i])
      }
    }
    let operationToSomeRecords = CKModifyRecordsOperation.init(recordsToSave: nil, recordIDsToDelete: arrayOfElementsToDelete)
    operationToSomeRecords.queuePriority = .veryHigh
    operationToSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
      if let error = error {
        print("Error trying to update from: \(initialValue) to \(initialValue + rangeToUpdate - 1)")
        print(error)
      } else if let recordsDeleted = recordsDeleted {
        print("Deleted records: \(recordsDeleted)")
        self.deleteAllTheseRecords(initialValue: initialValue + rangeToUpdate, rangeToUpdate: rangeToUpdate, recordsToDelete: recordsToDelete)
      }
    }
    self.selectedDatabase.add(operationToSomeRecords)
  }
  
  func deleteAllTheseRecordsPromising(initialValue: Int, rangeToUpdate: Int, recordsToDelete: [CKRecordID]) -> Promise<[CKRecordID]> {
    return Promise { result in
      var arrayOfElementsToDelete = [CKRecordID]()
      if (initialValue > recordsToDelete.count) || (recordsToDelete.count == 0) {
        print("Finish deleting all elements")
        return result.fulfill([CKRecordID]())
      }
      if initialValue + rangeToUpdate >= recordsToDelete.count {
        print("Deleting from: \(initialValue) to \(recordsToDelete.count - 1)")
        for i in initialValue...recordsToDelete.count - 1 {
          arrayOfElementsToDelete.append(recordsToDelete[i])
        }
      } else {
        print("Deleting from: \(initialValue) to \(initialValue + rangeToUpdate - 1)")
        for i in initialValue...(initialValue + rangeToUpdate - 1) {
          arrayOfElementsToDelete.append(recordsToDelete[i])
        }
      }
      let operationToSomeRecords = CKModifyRecordsOperation.init(recordsToSave: nil, recordIDsToDelete: arrayOfElementsToDelete)
      operationToSomeRecords.queuePriority = .veryHigh
      operationToSomeRecords.modifyRecordsCompletionBlock = { _, recordsDeleted, error in
        if let recordsDeleted = recordsDeleted {
          //                    print("Deleted records: \(recordsDeleted)")
          self.deleteAllTheseRecordsPromising(initialValue: initialValue + rangeToUpdate, rangeToUpdate: rangeToUpdate, recordsToDelete: recordsToDelete).done({ (recordsDeletedFromPromise) in
            var finalRecordsDeleted = recordsDeleted
            for record in recordsDeletedFromPromise {
              finalRecordsDeleted.append(record)
            }
            return result.fulfill(finalRecordsDeleted)
          }).catch({ (error) in
            return result.reject(error)
          })
        } else if let error = error {
          print("Error trying to delete from: \(initialValue) to \(initialValue + rangeToUpdate - 1)")
          print(error)
          return result.reject(error)
        } else {
          return result.fulfill([CKRecordID]())
        }
      }
      self.selectedDatabase.add(operationToSomeRecords)
    }
  }
  
  func fetchThisClient(clientData: CloudKitClient) -> Promise<CloudKitClient> {
    return Promise { result in
      self.selectedDatabase.fetch(withRecordID: clientData.rawDataFromServer!.recordID) { (record, error) in
        if let record = record {
          let clientData = self.getCloudKitClientFromServerData(recordFromCloudKit: record)
          return result.fulfill(clientData)
        } else if let error = error {
          return result.reject(error)
        }
      }
    }
  }
  
  func getClientOfThisAccount(account: CloudKitAccount) -> Promise<CloudKitClient> {
    return Promise { result in
      if account.clientOwner != nil {
        self.selectedDatabase.fetch(withRecordID: account.clientOwner!.recordID, completionHandler: { (clientCKRecord, error) in
          if let error = error {
            return result.reject(error)
          } else
            if let clientCKRecord = clientCKRecord {
              let cloudKitClientData = self.getCloudKitClientFromServerData(recordFromCloudKit: clientCKRecord)
              return result.fulfill(cloudKitClientData)
          }
        })
      } else {
        let error = NSError(domain: "This account doesn't have associated client", code: -1, userInfo: nil)
        return result.reject(error)
      }
    }
  }
  
  func createNewClient(request: ClientInfo.CreateNewClient.Request) -> Promise<[CKRecord]> {
    return Promise { result in
      
      let newClient = CKRecord.init(recordType: CloudKitKeys.Client.Client.rawValue)
      newClient[CloudKitKeys.Client.clientName.rawValue] = request.clientName.text! as CKRecordValue
      newClient[CloudKitKeys.Client.address.rawValue] = (request.clientAddress ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.cellphoneNumber.rawValue] = (request.clientCellPhone ?? "")  as CKRecordValue
      newClient[CloudKitKeys.Client.comment.rawValue] = (request.clientComment ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.coreDataID.rawValue] = "" as CKRecordValue
      newClient[CloudKitKeys.Client.dayOfTheWeekThatPays.rawValue] = request.dayThatClientWillPay as CKRecordValue
      newClient[CloudKitKeys.Client.dateOfLastPayment.rawValue] = nil
      newClient[CloudKitKeys.Client.location.rawValue] = request.clientLocation != nil ? request.clientLocation! as CKRecordValue : nil
      newClient[CloudKitKeys.Client.isGone.rawValue] = (request.clientIsGone == true ? 1 : 0) as CKRecordValue
      newClient[CloudKitKeys.Client.nextDateToVisit.rawValue] = "" as CKRecordValue
      newClient[CloudKitKeys.Client.phoneNumber.rawValue] = (request.clientHomePhone ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.stillHasDebt.rawValue] = "" as CKRecordValue
      newClient[CloudKitKeys.Client.surnames.rawValue] = (request.clientSurname ?? "") as CKRecordValue
      newClient[CloudKitKeys.Client.todayWasAlreadyVisited.rawValue] = "" as CKRecordValue
      newClient[CloudKitKeys.Client.warehouseNumber.rawValue] = request.clientWarehouse.text! as CKRecordValue
      let newClientReference = CKReference(record: newClient, action: .none)
      
      let newAccount = CKRecord(recordType: CloudKitKeys.Account.Account.rawValue)
      newAccount[CloudKitKeys.Account.clientNameWhoIsOwner.rawValue] = request.clientName.text! as CKRecordValue
      newAccount[CloudKitKeys.Account.coreDataID.rawValue] = "" as CKRecordValue
      newAccount[CloudKitKeys.Account.finalDebt.rawValue] = "0" as CKRecordValue
      newAccount[CloudKitKeys.Account.clientOwner.rawValue] = newClientReference as CKRecordValue
      newAccount[CloudKitKeys.Account.clientWarehouseNumber.rawValue] = request.clientWarehouse.text! as CKRecordValue
      let newAccountReference = CKReference(record: newAccount, action: .none)
      
      newClient[CloudKitKeys.Client.hasOneAccount.rawValue] = newAccountReference as CKRecordValue
      
      let recordsToCreate = [newClient, newAccount]
      let operationToSomeRecords = CKModifyRecordsOperation.init(recordsToSave: recordsToCreate, recordIDsToDelete: nil)
      operationToSomeRecords.queuePriority = .veryHigh
      operationToSomeRecords.modifyRecordsCompletionBlock = { recordsCreated, _, error in
        if let recordsCreated = recordsCreated {
          return result.fulfill(recordsCreated)
        } else
          if let error = error {
            return result.reject(error)
        }
      }
      self.selectedDatabase.add(operationToSomeRecords)
    }
  }
  
  func changeLocationToClient(clientData: CloudKitClient, newLocation: CLLocation) -> Promise<[CKRecord]> {
    return Promise { result in
      self.fetchThisClient(clientData: clientData).done({ (clientData) in
        clientData.rawDataFromServer![CloudKitKeys.Client.location.rawValue] = newLocation as CKRecordValue
        let recordsToEdit = [clientData.rawDataFromServer!]
        let operationToSomeRecords = CKModifyRecordsOperation.init(recordsToSave: recordsToEdit, recordIDsToDelete: nil)
        operationToSomeRecords.queuePriority = .veryHigh
        operationToSomeRecords.modifyRecordsCompletionBlock = { recordsEdited, _, error in
          if let recordsEdited = recordsEdited {
            return result.fulfill(recordsEdited)
          } else
            if let error = error {
              return result.reject(error)
          }
        }
        self.selectedDatabase.add(operationToSomeRecords)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  func getAllClientsThatDontPaySinceSevenOrMoreDays() -> Promise<[CloudKitClient]> {
    return Promise { result in
      let today = Date()
      let sevenDaysAgo = Calendar.current.date(byAdding: .day, value: -7, to: today)
      let predicateDateLastPayment = NSPredicate(format: "\(CloudKitKeys.Client.dateOfLastPayment.rawValue) < %@ ", sevenDaysAgo! as CVarArg)
      let predicateValidClient = NSPredicate(format: "\(CloudKitKeys.Client.isGone.rawValue) == %i", 0)
      let finalPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateValidClient, predicateDateLastPayment])
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Client.clientName.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: finalPredicate).done({ (allClients) in
        let arrayOfClients: [CloudKitClient] = allClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
        return result.fulfill(arrayOfClients)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  func getAllActiveClients() -> Promise<[CloudKitClient]> {
    return Promise { result in
      let predicateValidClient = NSPredicate(format: "\(CloudKitKeys.Client.isGone.rawValue) == %i", 0)
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Client.clientName.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: predicateValidClient).done({ (allClients) in
        let arrayOfClients: [CloudKitClient] = allClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
        return result.fulfill(arrayOfClients)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  func searchClientsWithText(searchingText: String) -> Promise<[CloudKitClient]> {
    return Promise { result in
      var foundClientsByName: Array<CloudKitClient> = Array()
      var foundClientsBySurnames: Array<CloudKitClient> = Array()
      var foundClientsByWarehouseName: Array<CloudKitClient> = Array()

      self.searchClientWithName(name: searchingText).then({ (clientsByName) -> Promise<[CloudKitClient]> in
        foundClientsByName = clientsByName
        return self.searchClientWithSurnames(surnames: searchingText)
      }).then({ (clientsBySurname) -> Promise<[CloudKitClient]> in
        foundClientsBySurnames = clientsBySurname
        return self.searchClientInWarehouseName(warehouseName: searchingText)
      }).done({ (clientsByWarehouseName) in
        foundClientsByWarehouseName = clientsByWarehouseName
        return result.fulfill(Array(Set(foundClientsByName + foundClientsBySurnames + foundClientsByWarehouseName)))
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  private func searchClientWithName(name: String) -> Promise<[CloudKitClient]> {
    return Promise { result in
      var arrayOfClients: [CloudKitClient] = Array()
      let predicate = NSPredicate(format: "\(CloudKitKeys.Client.clientName.rawValue) BEGINSWITH  %@", name)
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Client.clientName.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: predicate).done({ (allClients) in
        arrayOfClients = arrayOfClients + allClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
        let lowercasedPredicate = NSPredicate(format: "\(CloudKitKeys.Client.clientName.rawValue) BEGINSWITH  %@", name.lowercased())
        self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: lowercasedPredicate).done({ (lowercasedClients) in
          arrayOfClients = arrayOfClients + lowercasedClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
          return result.fulfill(arrayOfClients)
        }).catch({ (error) in
          return result.reject(error)
        })
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  private func searchClientWithSurnames(surnames: String) -> Promise<[CloudKitClient]> {
    return Promise { result in
      var arrayOfClients: [CloudKitClient] = Array()
      let predicate = NSPredicate(format: "\(CloudKitKeys.Client.surnames.rawValue) BEGINSWITH  %@", surnames)
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Client.clientName.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: predicate).done({ (allClients) in
        arrayOfClients = arrayOfClients + allClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
        let lowercasedPredicate = NSPredicate(format: "\(CloudKitKeys.Client.surnames.rawValue) BEGINSWITH  %@", surnames.lowercased())
        self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: lowercasedPredicate).done({ (lowercasedClients) in
          arrayOfClients = arrayOfClients + lowercasedClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
          return result.fulfill(arrayOfClients)
        }).catch({ (error) in
          return result.reject(error)
        })
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  private func searchClientInWarehouseName(warehouseName: String) -> Promise<[CloudKitClient]> {
    return Promise { result in
      var arrayOfClients: [CloudKitClient] = Array()
      let predicate = NSPredicate(format: "\(CloudKitKeys.Client.warehouseNumber.rawValue) BEGINSWITH %@", warehouseName)
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Client.clientName.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: predicate).done({ (allClients) in
        arrayOfClients = arrayOfClients + allClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
        let lowercasedPredicate = NSPredicate(format: "\(CloudKitKeys.Client.warehouseNumber.rawValue) BEGINSWITH %@", warehouseName.lowercased())
        self.fetchRecords(forType: CloudKitKeys.Client.Client.rawValue, sortDescriptors: [sortDescriptor], predicate: lowercasedPredicate).done({ (lowercasedClients) in
          arrayOfClients = arrayOfClients + lowercasedClients.map{self.getCloudKitClientFromServerData(recordFromCloudKit: $0)}
          return result.fulfill(arrayOfClients)
        }).catch({ (error) in
          return result.reject(error)
        })
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
}

class CloudKitClient {
  
  var address: String = ""
  var cellphoneNumber: String = ""
  var clientName: String = ""
  var comment: String = ""
  var coreDataID: String = ""
  var dayOfTheWeekThatPays: String = ""
  var dateOfLastPayment: Date?
  var hasOneAccount: CKReference?
  var isGone: NSNumber = 0 // 0 = no, 1 = true
  var location: CLLocation?
  var nextDateToVisit: String = ""
  var phoneNumber: String = ""
  var stillHasDebt: String = ""
  var surnames: String = ""
  var todayWasAlreadyVisited: String = ""
  var wareHouseNumber: String = ""
  var rawDataFromServer: CKRecord?
  
  init() {}
  
}

extension CloudKitClient {
  @objc func value(forKey key: String) -> Any? {
    switch key {
    case "address":
      return self.address
    case "cellphoneNumber":
      return self.cellphoneNumber
    case "clientName":
      return self.clientName
    case "comment":
      return self.comment
    case "coreDataID":
      return self.coreDataID
    case "dayOfTheWeekThatPays":
      return self.dayOfTheWeekThatPays
    case "dateOfLastPayment":
      return self.dateOfLastPayment
    case "isGone":
      return self.isGone
    case "nextDateToVisit":
      return self.nextDateToVisit
    case "phoneNumber":
      return self.phoneNumber
    case "stillHasDebt":
      return self.stillHasDebt
    case "surnames":
      return self.surnames
    case "todayWasAlreadyVisited":
      return self.todayWasAlreadyVisited
    case "wareHouseNumber":
      return self.wareHouseNumber
    default:
      return nil
    }
  }
}

extension CloudKitClient: Hashable {
  var hashValue: Int {
    return self.clientName.hashValue
  }
  
  static func == (lhs: CloudKitClient, rhs: CloudKitClient) -> Bool {
    return lhs.clientName == rhs.clientName && lhs.wareHouseNumber == rhs.wareHouseNumber && lhs.surnames == rhs.surnames
  }
  
  
}
