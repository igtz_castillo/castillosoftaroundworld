//
//  CloudKitArticle.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 09/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CoreData
import CloudKit
import PromiseKit

extension CloudKitModel {
    
    func uploadAllArticlesWithPromises() -> Promise<[CKRecord]> {
        return Promise { result in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Articulo")
            //request.predicate = NSPredicate(format: "age = %@", "12")
            request.returnsObjectsAsFaults = false
            do {
                let dataArray = try context.fetch(request)
                let articulos = dataArray as! [Articulo]
                let index = 1566  //articulos.count - 50  //Change to zero when want to update all the info
                var allAccountsFromServer = [CKRecord]()
                CloudKitModel.sharedInstance.fetchRecords(forType: CloudKitKeys.Account.Account.rawValue).then({ (allAccounts) -> Promise<[CKRecord]> in
                    allAccountsFromServer = allAccounts
                    return CloudKitModel.sharedInstance.fetchRecords(forType: CloudKitKeys.ArticleType.ArticleType.rawValue)
                }).done({ (allArticleTypesFromServer) in
                    self.articleToCreate(index: index, articles: articulos, articlesCreated: [CKRecord](), allAccounts: allAccountsFromServer, allArticleType: allArticleTypesFromServer).done({ (allArticlesWithTheirInformationCreated) in
                        return result.fulfill(allArticlesWithTheirInformationCreated)
                    }).catch({ (error) in
                        return result.reject(error)
                    })
                }).catch({ (error) in
                    print("Error fetching accounts or articleTypes from server")
                    return result.reject(error)
                })
            } catch {
                print("Error fetching Articulos from CoreData")
            }
        }
    }
    
    private func articleToCreate(index: Int, articles: [Articulo], articlesCreated: [CKRecord], allAccounts: [CKRecord], allArticleType: [CKRecord]) -> Promise<[CKRecord]> {
        return Promise { result in
            var finalArticlesCreated = articlesCreated
            if index <= articles.count - 1 {
                let articleTypeOfThisArticle = articles[index].esteArticuloEsDeUntipoArticulo
                let accountInWhichIsAdded = articles[index].esteArticuloEstaCargadoEnLaCuenta
                var articleTypeOfThisArticleCKRecord: CKRecord? = nil
                var accountWhichIsAddedCKRecord: CKRecord? = nil
                
                if let articleTypeOfThisArticle = articleTypeOfThisArticle {
                    for articleType in allArticleType {
                        let articleTypeCoreDataString = articleType[CloudKitKeys.ArticleType.coreDataID.rawValue] as! String
                        if articleTypeCoreDataString == articleTypeOfThisArticle.objectID.uriRepresentation().absoluteString {
                            articleTypeOfThisArticleCKRecord = articleType
                            break
                        }
                    }
                }
                if let accountInWhichIsAdded = accountInWhichIsAdded {
                    for account in allAccounts {
                        let accountCoreDataString = account[CloudKitKeys.ArticleType.coreDataID.rawValue] as! String
                        if accountCoreDataString == accountInWhichIsAdded.objectID.uriRepresentation().absoluteString {
                            accountWhichIsAddedCKRecord = account
                            break
                        }
                    }
                }
                self.createArticleWith(articleData: articles[index], typeArticleWhichIsFrom: articleTypeOfThisArticleCKRecord, accountInWhichIsAdded: accountWhichIsAddedCKRecord).done({ (recordsCreated) in
                    print("Article number \(index + 1) created")
                    finalArticlesCreated = finalArticlesCreated + recordsCreated
                    self.articleToCreate(index: index + 1, articles: articles, articlesCreated: finalArticlesCreated, allAccounts: allAccounts, allArticleType: allArticleType).done({ (articlesWithAllItsInformationCreated) in
                        return result.fulfill(articlesWithAllItsInformationCreated)
                    }).catch({ (error) in
                        print("Error creating article number \(index). Last article created was: \(finalArticlesCreated.last?.description ?? "No client created")")
                        return result.reject(error)
                    })
                }).catch({ (error) in
                    print("Error creating articles. Last article created was: \(finalArticlesCreated.last?.description ?? "No client created")")
                    return result.reject(error)
                })
            } else {
                return result.fulfill(articlesCreated)
            }
        }
    }
    
    private func createArticleWith(articleData: Articulo, typeArticleWhichIsFrom: CKRecord?, accountInWhichIsAdded: CKRecord?) -> Promise<[CKRecord]> {
        return Promise { result in
            
            //Create new article
          
          let characteristics = articleData.selectedCharacteristics as? [String: String]
          var arrayOfStringCharacteristicValueSelected: Array<String> = Array<String>()
          var arrayOfStringCharacteristicKey: Array<String> = Array()
          if characteristics != nil {
            arrayOfStringCharacteristicKey = Array(characteristics!.keys)
            for key in arrayOfStringCharacteristicKey {
              arrayOfStringCharacteristicValueSelected.append((articleData.selectedCharacteristics! as! [String: String])[key]!)
            }
          }
          
            let newArticle = CKRecord.init(recordType: CloudKitKeys.Article.Article.rawValue)
            newArticle[CloudKitKeys.Article.coreDataID.rawValue] = articleData.objectID.description as CKRecordValue
            newArticle[CloudKitKeys.Article.brandName.rawValue] = articleData.nombreDeMarca! as CKRecordValue
            newArticle[CloudKitKeys.Article.cost.rawValue] = articleData.costo! as CKRecordValue
            newArticle[CloudKitKeys.Article.nameArticle.rawValue] = articleData.nombreArticulo! as CKRecordValue
            if articleData.fechaDeCompraDeEsteArticulo != nil {
                newArticle[CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue] = articleData.fechaDeCompraDeEsteArticulo! as CKRecordValue
            }
            newArticle[CloudKitKeys.Article.searchingWords.rawValue] = (articleData.detallesPorBuscar ?? "") as CKRecordValue
            newArticle[CloudKitKeys.Article.selectedCharacteristicsDictionaryKeys.rawValue] = arrayOfStringCharacteristicKey as CKRecordValue
            newArticle[CloudKitKeys.Article.selectedCharacteristicsDictionaryValues.rawValue] = arrayOfStringCharacteristicValueSelected as CKRecordValue
            var recordsToSaveOrUpdate: Array<CKRecord> = [CKRecord]()
            recordsToSaveOrUpdate.append(newArticle)
            if let typeArticleWhichIsFrom = typeArticleWhichIsFrom {
                newArticle[CloudKitKeys.Article.thisArticleIsThisArticleType.rawValue] = CKReference.init(record: typeArticleWhichIsFrom, action: CKReferenceAction.none)
                var allArticlesWhichAreThisArticleType = [CKReference]()
                if typeArticleWhichIsFrom[CloudKitKeys.ArticleType.articleTypeOfManyArticles.rawValue] == nil {
                    allArticlesWhichAreThisArticleType.append(CKReference.init(record: newArticle, action: CKReferenceAction.none))
                } else {
                    allArticlesWhichAreThisArticleType = typeArticleWhichIsFrom[CloudKitKeys.ArticleType.articleTypeOfManyArticles.rawValue] as! [CKReference]
                    allArticlesWhichAreThisArticleType.append(CKReference.init(record: newArticle, action: CKReferenceAction.none))
                }
                typeArticleWhichIsFrom[CloudKitKeys.ArticleType.articleTypeOfManyArticles.rawValue] = allArticlesWhichAreThisArticleType as CKRecordValue
                recordsToSaveOrUpdate.append(typeArticleWhichIsFrom)
            }
            if let accountInWhichIsAdded = accountInWhichIsAdded {
                newArticle[CloudKitKeys.Article.thisArticleIsAddedIntoThisAccount.rawValue] = CKReference.init(record: accountInWhichIsAdded, action: CKReferenceAction.none)
                newArticle[CloudKitKeys.Article.isAddedInAnAccount.rawValue] = 1 as CKRecordValue
                var articlesReferenceID = [CKReference]()
                if accountInWhichIsAdded[CloudKitKeys.Account.thisAccountHasThisArticles.rawValue] == nil {
                    articlesReferenceID.append(CKReference.init(record: newArticle, action: CKReferenceAction.none))
                } else {
                    articlesReferenceID = accountInWhichIsAdded[CloudKitKeys.Account.thisAccountHasThisArticles.rawValue] as! [CKReference]
                    articlesReferenceID.append(CKReference.init(record: newArticle, action: CKReferenceAction.none))
                }
                accountInWhichIsAdded[CloudKitKeys.Account.thisAccountHasThisArticles.rawValue] = articlesReferenceID as CKRecordValue
                recordsToSaveOrUpdate.append(accountInWhichIsAdded)
            } else {
                newArticle[CloudKitKeys.Article.isAddedInAnAccount.rawValue] = 0 as CKRecordValue
            }
            let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: recordsToSaveOrUpdate, recordIDsToDelete: nil)
            operationToSaveSomeRecords.queuePriority = .veryHigh
            operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
                if let error = error {
                    print("Error when updating: \(error)")
                    result.reject(error)
                } else if let recordsChanged = recordsChanged {
                    result.fulfill(recordsChanged)
                }
            }
            operationToSaveSomeRecords.completionBlock = {
               //print("Function \"createClientWith\" finished")
            }
            self.selectedDatabase.add(operationToSaveSomeRecords)
        }
    }
  
  func deleteAllArticles() -> Promise<[CKRecordID]> {
    return Promise { result in
      
      self.fetchRecords(forType: CloudKitKeys.Article.Article.rawValue).then({ (recordsToDelete) -> Promise<[CKRecordID]> in
        let articlesIDToDelete = recordsToDelete.map({$0.recordID})
        return self.deleteAllTheseRecordsPromising(initialValue: 0, rangeToUpdate: 350, recordsToDelete: articlesIDToDelete)
      }).done({ (articlesDeletedID) in
        return result.fulfill(articlesDeletedID)
      }).catch({ (error) in
        return result.reject(error)
      })
      
    }
  }
  
  func getAllArticlesInThisAccount(account: CKReference) -> Promise<[CKRecord]> {
    return Promise { result in
      let predicate = NSPredicate(format: "\(CloudKitKeys.Article.thisArticleIsAddedIntoThisAccount.rawValue) == %@", account)
      let predicateNotBuyed = NSPredicate(format: "\(CloudKitKeys.Article.isAddedInAnAccount.rawValue) == %i", 1)
      let finalPredicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicate, predicateNotBuyed])
      let query = CKQuery(recordType: CloudKitKeys.Article.Article.rawValue, predicate: finalPredicate)
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue, ascending: true)
      query.sortDescriptors = [sortDescriptor]
      self.fetchRecords(forType: CloudKitKeys.Article.Article.rawValue, sortDescriptors: [sortDescriptor], predicate: predicate).done({ (allArticles) in
        return result.fulfill(allArticles)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  
//  func getAllArticlesInThisAccount(account: CKReference) -> Promise<[CKRecord]> {
//    return Promise { result in
//      let predicate = NSPredicate(format: "\(CloudKitKeys.Article.thisArticleIsAddedIntoThisAccount.rawValue) == %@", account)
//      let query = CKQuery(recordType: CloudKitKeys.Article.Article.rawValue, predicate: predicate)
//      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue, ascending: true)
//      query.sortDescriptors = [sortDescriptor]
//      privateDB.perform(query, inZoneWith: nil) { (articles, error) in
//        if let articles = articles {
//          result.fulfill(articles)
//        } else if let error = error {
//          result.reject(error)
//        }
//      }
//    }
//  }
  
  func getCloudKitArticleFromServerData(recordFromCloudKit: CKRecord) -> CloudKitArticle {
    let newArticle = CloudKitArticle()
    newArticle.brandName = recordFromCloudKit[CloudKitKeys.Article.brandName.rawValue] as! String
    newArticle.coreDataID = recordFromCloudKit[CloudKitKeys.Article.coreDataID.rawValue] as! String
    newArticle.cost = recordFromCloudKit[CloudKitKeys.Article.cost.rawValue] as! String
    newArticle.nameArticle = recordFromCloudKit[CloudKitKeys.Article.nameArticle.rawValue] as! String
    newArticle.purchaseDateOfThisArticle = recordFromCloudKit[CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue] as? Date
    newArticle.searchingWords = recordFromCloudKit[CloudKitKeys.Article.searchingWords.rawValue] as! String
    newArticle.isAddedInAnAccount = recordFromCloudKit[CloudKitKeys.Article.isAddedInAnAccount.rawValue] as! NSNumber
    newArticle.selectedCharacteristicsDictionaryKeys = recordFromCloudKit[CloudKitKeys.Article.selectedCharacteristicsDictionaryKeys.rawValue] as! [String]
    newArticle.selectedCharacteristicsDictionaryValues = recordFromCloudKit[CloudKitKeys.Article.selectedCharacteristicsDictionaryValues.rawValue] as! [String]
    newArticle.thisArticleIsAddedIntoThisAccount = recordFromCloudKit[CloudKitKeys.Article.thisArticleIsAddedIntoThisAccount.rawValue] as? CKReference
    newArticle.thisArticleIsThisArticleType = recordFromCloudKit[CloudKitKeys.Article.thisArticleIsThisArticleType.rawValue] as? CKReference
    newArticle.rawDataFromServer = recordFromCloudKit
    return newArticle
  }
  
  func getAllArticlesNotBuyedOfThisTypeFromServer(articleType: CloudKitArticleType) -> Promise<[CloudKitArticle]> {
    return Promise { result in
      
      let articleTypeCKReference = CKReference.init(record: articleType.rawDataFromServer!, action: .none)
      let predicateArticleType = NSPredicate(format: "\(CloudKitKeys.Article.thisArticleIsThisArticleType.rawValue) == %@ ", articleTypeCKReference)
      let predicateNotBuyed = NSPredicate(format: "\(CloudKitKeys.Article.isAddedInAnAccount.rawValue) == %i", 0)
      let finalPredicate = NSCompoundPredicate.init(andPredicateWithSubpredicates: [predicateArticleType, predicateNotBuyed])
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.Article.nameArticle.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.Article.Article.rawValue, sortDescriptors: [sortDescriptor], predicate: finalPredicate).done({ (allArticlesOfThisTypeAndNotBuyed) in
      let arrayOfCloudKitArticle: [CloudKitArticle] = allArticlesOfThisTypeAndNotBuyed.map{self.getCloudKitArticleFromServerData(recordFromCloudKit: $0)}
//        arrayOfCloudKitArticle = arrayOfCloudKitArticle.filter{ $0.thisArticleIsAddedIntoThisAccount == nil}
        return result.fulfill(arrayOfCloudKitArticle)
      }).catch({ (error) in
        return result.reject(error)
      })
    }
  }
  
  func createNewArticle(request: CreateNewArticle.CreationOfNewArticle.Request) -> Promise<CloudKitArticle> {
    return Promise { result in
      
      self.selectedDatabase.fetch(withRecordID: request.articleType.rawDataFromServer!.recordID, completionHandler: { (articleTypeRecord, error) in
        if let articleTypeRecord = articleTypeRecord {
          var arrayOfArticlesOfThisType = articleTypeRecord[CloudKitKeys.ArticleType.articleTypeOfManyArticles.rawValue] as? [CKReference] ?? Array()
        
          let newArticle = CKRecord.init(recordType: CloudKitKeys.Article.Article.rawValue)
          newArticle[CloudKitKeys.Article.coreDataID.rawValue] = "" as CKRecordValue
          newArticle[CloudKitKeys.Article.brandName.rawValue] = (request.brandNameString ?? "") as CKRecordValue
          newArticle[CloudKitKeys.Article.cost.rawValue] = request.articleCostString as CKRecordValue
          newArticle[CloudKitKeys.Article.nameArticle.rawValue] = request.articleNameString as CKRecordValue
          newArticle[CloudKitKeys.Article.searchingWords.rawValue] = request.searchingWords as CKRecordValue
          newArticle[CloudKitKeys.Article.selectedCharacteristicsDictionaryKeys.rawValue] = (request.arrayOfStringCharacteristicKey ?? Array()) as CKRecordValue
          newArticle[CloudKitKeys.Article.selectedCharacteristicsDictionaryValues.rawValue] = (request.arrayOfStringCharacteristicValueSelected ?? Array()) as CKRecordValue
          newArticle[CloudKitKeys.Article.isAddedInAnAccount.rawValue] = 0 as CKRecordValue
          newArticle[CloudKitKeys.Article.thisArticleIsThisArticleType.rawValue] =  CKReference.init(record: request.articleType.rawDataFromServer!, action: .none)
          
          arrayOfArticlesOfThisType.append(CKReference.init(record: newArticle, action: .none))
          articleTypeRecord[CloudKitKeys.ArticleType.articleTypeOfManyArticles.rawValue] = arrayOfArticlesOfThisType as CKRecordValue
          let recordsToSaveOrUpdate = [newArticle, articleTypeRecord]
          let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: recordsToSaveOrUpdate, recordIDsToDelete: nil)
          operationToSaveSomeRecords.queuePriority = .veryHigh
          operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
            if let error = error {
              return result.reject(error)
            } else
              if let recordsChanged = recordsChanged {
                let newArticleCreated = recordsChanged.filter{ $0.recordID.recordName == newArticle.recordID.recordName }
                if newArticleCreated.count > 0 {
                  let cloudKitArticle = self.getCloudKitArticleFromServerData(recordFromCloudKit: newArticleCreated.first!)
                  return result.fulfill(cloudKitArticle)
                } else {
                  let error = NSError(domain: "No fue creado ningún artículo", code: -1, userInfo: nil)
                  return result.reject(error)
                }
            }
          }
          self.selectedDatabase.add(operationToSaveSomeRecords)
        } else
        if let error = error {
          return result.reject(error)
        }
      })
    }
  }

  
}

class CloudKitArticle {
  
  var brandName: String = ""
  var coreDataID: String = ""
  var cost: String = ""
  var isAddedInAnAccount: NSNumber = 0
  var nameArticle: String = ""
  var purchaseDateOfThisArticle: Date?
  var searchingWords: String = ""
  var selectedCharacteristicsDictionaryKeys: [String] = Array()
  var selectedCharacteristicsDictionaryValues: [String] = Array()
  var thisArticleIsAddedIntoThisAccount: CKReference?
  var thisArticleIsThisArticleType: CKReference?
  var rawDataFromServer: CKRecord?
  
  init() {}
  
}

extension CloudKitArticle {
  @objc func value(forKey key: String) -> Any? {
    switch key {
    case "brandName":
      return self.brandName
    case "cost":
      return self.cost
    case "nameArticle":
      return self.nameArticle
    case "searchingWords":
      return self.searchingWords
    default:
      return nil
    }
  }
}
