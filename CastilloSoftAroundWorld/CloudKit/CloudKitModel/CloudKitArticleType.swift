//
//  CloudKitArticleType.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 09/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CoreData
import CloudKit
import PromiseKit

extension CloudKitModel {
    
    func getAllArticleTypesFromCloudKit() -> Promise<[CKRecord]> {
        return Promise { result in
            let predicate = NSPredicate(value: true)
            let query = CKQuery(recordType: CloudKitKeys.ArticleType.ArticleType.rawValue, predicate: predicate)
            self.selectedDatabase.perform(query, inZoneWith: nil) { (articleTypes, error) in
                if let articleTypes = articleTypes {
                    result.fulfill(articleTypes)
                } else if let error = error {
                    result.reject(error)
                }
            }
        }
    }
    
    private func uploadTypeArticleWithCharacteristicsAndOptions() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TipoArticulo")
        request.returnsObjectsAsFaults = false
        do {
            let resultRequest = try context.fetch(request)
            let tiposArticulo = resultRequest as! [TipoArticulo]
            for i in 0...19 {
                print("begin")
                let tipoArticulo = tiposArticulo[i]
                let caracteristicasDeTipoArticulo = tipoArticulo.tieneMuchas?.array as! [CaracteristicaTipoArticulo]
                self.createTypeArticleWith(typeArticleData: tipoArticulo, characteristicsOfType: caracteristicasDeTipoArticulo).done({ (allRecordsChanged) in
                    print(allRecordsChanged)
                }).ensure({
                    print("finished everything")
                }).catch({ (error) in
                    print(error)
                })
                usleep(500000)
                print("end")
            }
        } catch {
            print("Failed")
        }
    }
    
    func uploadTypeArticleWithCharacteristicsAndOptionsWithPromises() -> Promise<[CKRecord]> {
        return Promise { result in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "TipoArticulo")
            request.returnsObjectsAsFaults = false
            do {
                let dataArray = try context.fetch(request)
                let articleTypes = dataArray as! [TipoArticulo]
                let index = 0
                self.articleTypeToCreateWithPromises(index: index, articleTypes: articleTypes, createdArticleTypes: [CKRecord]()).done({ (articleTypesWithAllTheirInformationCreated) in
                    return result.fulfill(articleTypesWithAllTheirInformationCreated)
                }).catch({ (error) in
                    return result.reject(error)
                })
            } catch {
                let error = NSError.init(domain: "No se pudo cargar el tipo de artículo de coredata", code: -1, userInfo: nil)
                print("Failed to load articleType from coredata")
                return result.reject(error)
            }
        }
    }
    
    private func articleTypeToCreateWithPromises(index: Int, articleTypes: [TipoArticulo], createdArticleTypes: [CKRecord]) -> Promise<[CKRecord]> {
        return Promise { result in
            var finalCreatedArticleTypes = createdArticleTypes
            if index <= articleTypes.count - 1 {
                let characteristics = articleTypes[index].tieneMuchas?.array as! [CaracteristicaTipoArticulo]
                self.createTypeArticleWith(typeArticleData: articleTypes[index], characteristicsOfType: characteristics).done({ (recordsCreated) in
                    print("Article type number \(index + 1) created")
                    finalCreatedArticleTypes = finalCreatedArticleTypes + recordsCreated
                    self.articleTypeToCreateWithPromises(index: index + 1, articleTypes: articleTypes, createdArticleTypes: finalCreatedArticleTypes).done({ (articleTypesWithAllTheirInformationCreated) in
                        return result.fulfill(articleTypesWithAllTheirInformationCreated)
                    }).catch({ (error) in
                        print("Error creating artcileType number \(index). Last articleType created was: \(finalCreatedArticleTypes.last?.description ?? "No articleType created")")
                        return result.reject(error)
                    })
                }).catch({ (error) in
                    print("Error creating articleTypes. Last articleType created was: \(finalCreatedArticleTypes.last?.description ?? "No articleType created")")
                    return result.reject(error)
                })
            } else {
                return result.fulfill(createdArticleTypes)
            }
        }
    }
    
    private func createTypeArticleWith(typeArticleData: TipoArticulo, characteristicsOfType: [CaracteristicaTipoArticulo]) -> Promise<[CKRecord]> {
        return Promise { result in
            var arrayOfAllNewRecords: [CKRecord] = [CKRecord]()
            
            //Create new article type
            let newArticleType = CKRecord.init(recordType: CloudKitKeys.ArticleType.ArticleType.rawValue)
            newArticleType[CloudKitKeys.ArticleType.articleTypeName.rawValue] = (typeArticleData.nombreDeTipoArticulo ?? "") as CKRecordValue
            newArticleType[CloudKitKeys.ArticleType.articleTypeComment.rawValue] = (typeArticleData.comentarioDeTipoArticulo ?? "") as CKRecordValue
            newArticleType[CloudKitKeys.ArticleType.coreDataID.rawValue] = typeArticleData.objectID.uriRepresentation().absoluteString as CKRecordValue
            let newArticleTypeReference = CKReference.init(record: newArticleType, action: CKReferenceAction.none)
            arrayOfAllNewRecords.append(newArticleType)
            
            var arrayOfCharacteristicsReference: [CKReference] = Array<CKReference>()
            //Create characteristics of article type
            for characteristic in characteristicsOfType {
                let newCharacteristic = CKRecord.init(recordType: CloudKitKeys.ArticleTypeCharacteristic.ArticleTypeCharacteristic.rawValue)
                newCharacteristic[CloudKitKeys.ArticleTypeCharacteristic.articleTypeCharacteristicComment.rawValue] = (characteristic.comentarioDeCaracteristicaDeTipoArticulo ?? "") as CKRecordValue
                newCharacteristic[CloudKitKeys.ArticleTypeCharacteristic.articleTypeCharacteristicName.rawValue] = (characteristic.nombreDeCaracteristicaDeTipoArticulo ?? "") as CKRecordValue
                newCharacteristic[CloudKitKeys.ArticleTypeCharacteristic.coreDataID.rawValue] = characteristic.objectID.uriRepresentation().absoluteString as CKRecordValue
                newCharacteristic[CloudKitKeys.ArticleTypeCharacteristic.thisCharacteristicIsForThisArticleType.rawValue] = newArticleTypeReference
                
                arrayOfAllNewRecords.append(newCharacteristic)
                let newCharacteristicReference = CKReference.init(record: newCharacteristic, action: CKReferenceAction.none)
                arrayOfCharacteristicsReference.append(newCharacteristicReference)
                
                //Create all options for this new characteristic
                var arrayOfOptionsOfThisCharacteristicReference: [CKReference] = Array<CKReference>()
                let optionsOfThisCharacteristic = characteristic.tieneMuchas?.array as! [OpcionCaracteristica]
                for option in optionsOfThisCharacteristic {
                    let newCharacteristicOption = CKRecord.init(recordType: CloudKitKeys.ArticleTypeCharacteristicOption.ArticleTypeCharacteristicOption.rawValue)
                    newCharacteristicOption[CloudKitKeys.ArticleTypeCharacteristicOption.articleTypeCharacteristicOptionName.rawValue] = (option.nombreDeOpcionDeCaracteristica ?? "") as CKRecordValue
                    newCharacteristicOption[CloudKitKeys.ArticleTypeCharacteristicOption.coreDataID.rawValue] = option.objectID.uriRepresentation().absoluteString as CKRecordValue
                    newCharacteristicOption[CloudKitKeys.ArticleTypeCharacteristicOption.optionOfThisArticleTypeCharacteristic.rawValue] = newCharacteristicReference
                    arrayOfAllNewRecords.append(newCharacteristicOption)
                    
                    let newCharacteristicOptionReference = CKReference.init(record: newCharacteristicOption, action: CKReferenceAction.none)
                    arrayOfOptionsOfThisCharacteristicReference.append(newCharacteristicOptionReference)
                }
                newCharacteristic[CloudKitKeys.ArticleTypeCharacteristic.thisCharacteristicHasTheseOptions.rawValue] = arrayOfOptionsOfThisCharacteristicReference as CKRecordValue
            }
            newArticleType[CloudKitKeys.ArticleType.thisArticleTypeHasTheseCharacteristics.rawValue] = arrayOfCharacteristicsReference as CKRecordValue
            
            let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: arrayOfAllNewRecords, recordIDsToDelete: nil)
            operationToSaveSomeRecords.queuePriority = .veryHigh
            operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
                if let error = error {
                    print("Error when updating: \(error)")
                    result.reject(error)
                } else if let recordsChanged = recordsChanged {
                    result.fulfill(recordsChanged)
                }
            }
            operationToSaveSomeRecords.completionBlock = {
                print("Function \"createAllArticleType\" finished")
            }
            self.selectedDatabase.add(operationToSaveSomeRecords)
        }
    }
  
  
  func getAllCloudKitArticleType() -> Promise<[CloudKitArticleType]> {
    return Promise { result in
      let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.ArticleType.articleTypeName.rawValue, ascending: true)
      self.fetchRecords(forType: CloudKitKeys.ArticleType.ArticleType.rawValue, sortDescriptors: [sortDescriptor]).done({ (recordsFromCloudKit) in
        let allCloudKitArticlesType: [CloudKitArticleType] = recordsFromCloudKit.map({ (recordFromCloudKit) -> CloudKitArticleType in
          return self.getCloudKitArticleTypeFromServerData(recordFromCloudKit: recordFromCloudKit)
        })
        return result.fulfill(allCloudKitArticlesType)
      }).catch({ (error) in
        print("Error trying fetching all Article Type. Error: \(error) ")
        return result.reject(error)
      })
    }
  }
  
  func getCloudKitArticleTypeFromServerData(recordFromCloudKit: CKRecord) -> CloudKitArticleType {
    let newArticleType = CloudKitArticleType()
    newArticleType.articleTypeComment = recordFromCloudKit[CloudKitKeys.ArticleType.articleTypeComment.rawValue] as! String
    newArticleType.articleTypeName = recordFromCloudKit[CloudKitKeys.ArticleType.articleTypeName.rawValue] as! String
    newArticleType.articleTypeOfManyArticles = recordFromCloudKit[CloudKitKeys.ArticleType.articleTypeOfManyArticles.rawValue] as? [CKReference]
    newArticleType.coreDataID = recordFromCloudKit[CloudKitKeys.ArticleType.coreDataID.rawValue] as! String
    newArticleType.thisArticleTypeHasTheseCharacteristics = recordFromCloudKit[CloudKitKeys.ArticleType.thisArticleTypeHasTheseCharacteristics.rawValue] as? [CKReference]
    newArticleType.rawDataFromServer = recordFromCloudKit
    return newArticleType
  }
  
  func fetchThisArticleType(articleType: CloudKitArticleType) -> Promise<CloudKitArticleType> {
    return Promise { result in
      if articleType.rawDataFromServer != nil {
        self.selectedDatabase.fetch(withRecordID: articleType.rawDataFromServer!.recordID, completionHandler: { (fetchedRecord, error) in
          if let fetchedRecord = fetchedRecord {
            let articleTypeCloudKit = self.getCloudKitArticleTypeFromServerData(recordFromCloudKit: fetchedRecord)
            return result.fulfill(articleTypeCloudKit)
          } else
          if let error = error {
            return result.reject(error)
          }
        })
      } else {
        let error = NSError(domain: "El tipo de artículo no tiene información de tipo CKRecord", code: -1, userInfo: nil)
        return result.reject(error)
      }
    }
  }
  
  func createNewArticleType(articleTypeName: String, articleTypeComment: String) -> Promise<CloudKitArticleType> {
    return Promise { result in
      let newArticleType = CKRecord(recordType: CloudKitKeys.ArticleType.ArticleType.rawValue)
      newArticleType[CloudKitKeys.ArticleType.articleTypeName.rawValue] = articleTypeName as CKRecordValue
      newArticleType[CloudKitKeys.ArticleType.articleTypeComment.rawValue] = articleTypeComment as CKRecordValue
      newArticleType[CloudKitKeys.ArticleType.coreDataID.rawValue] = "" as CKRecordValue
      let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: [newArticleType], recordIDsToDelete: nil)
      operationToSaveSomeRecords.queuePriority = .veryHigh
      operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
        if let recordsChanged = recordsChanged, recordsChanged.count > 0 {
          let newCloudKitArticleType = self.getCloudKitArticleTypeFromServerData(recordFromCloudKit: recordsChanged.first!)
          result.fulfill(newCloudKitArticleType)
        } else
        if let error = error {
          print("Error when updating: \(error)")
          result.reject(error)
        }
      }
      self.selectedDatabase.add(operationToSaveSomeRecords)
    }
  }
    
}

class CloudKitArticleType {
  
  var articleTypeComment: String = ""
  var articleTypeName: String = ""
  var articleTypeOfManyArticles: [CKReference]?
  var coreDataID: String = ""
  var thisArticleTypeHasTheseCharacteristics: [CKReference]?
  var rawDataFromServer: CKRecord?
  
  init() {}
  
}

extension CloudKitArticleType {
  
  @objc func value(forKey key: String) -> Any? {
    switch key {
    case "articleTypeComment":
      return self.articleTypeComment
    case "articleTypeName":
      return self.articleTypeName
    default:
      return nil
    }
  }
  
}
