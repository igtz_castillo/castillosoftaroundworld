//
//  CloudKitArticleTypeCharacteristic.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 10/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CloudKit
import PromiseKit

extension CloudKitModel {
    
    //MARK: - ArticleTypeCharacteristics Section
    func getAllArticleTypeCharacteristicsFromCloudKit() -> Promise<[CKRecord]> {
        return Promise { result in
            let predicate = NSPredicate(value: true)
            let query = CKQuery(recordType: CloudKitKeys.ArticleTypeCharacteristic.ArticleTypeCharacteristic.rawValue, predicate: predicate)
            self.selectedDatabase.perform(query, inZoneWith: nil) { (articleTypesCharacteristicArray, error) in
                if let articleTypesCharacteristics = articleTypesCharacteristicArray {
                    result.fulfill(articleTypesCharacteristics)
                } else if let error = error {
                    result.reject(error)
                }
            }
        }
    }
  
  func getAllCharacteristicsAndOptionsForThisArticleType(articleType: CloudKitArticleType) -> Promise<CloudKitArticleTypeCharacteristicsAndOptions.GetAllCharacteristicsAndOptions.Response> {
    return Promise { result in
      if articleType.rawDataFromServer == nil {
        let error = NSError.init(domain: "Error. El tipo artículo no viene con la información del servidor", code: -1, userInfo: nil)
        return result.reject(error)
      }
      self.selectedDatabase.fetch(withRecordID: articleType.rawDataFromServer!.recordID, completionHandler: { (articleTypeRecord, error) in
        if let articleTypeRecord = articleTypeRecord {
          let articleTypeCKReference = CKReference.init(record: articleTypeRecord, action: .none)
          let predicate = NSPredicate(format: "\(CloudKitKeys.ArticleTypeCharacteristic.thisCharacteristicIsForThisArticleType.rawValue) == %@ ", articleTypeCKReference)
          let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.ArticleTypeCharacteristic.articleTypeCharacteristicName.rawValue, ascending: true)
          self.fetchRecords(forType: CloudKitKeys.ArticleTypeCharacteristic.ArticleTypeCharacteristic.rawValue, sortDescriptors: [sortDescriptor], predicate: predicate).done({ (allCharacteristics) in
            let allCharacteristicsCloudKit = allCharacteristics.map{self.getCloudkitArticleTypeCharacteristicCloudKit(from: $0)}
            self.getAllOptionsOfTheseArticleTypeCharacteristics(index: 0, characteristics: allCharacteristicsCloudKit).done({ (allCharacteristicsWithAllOptions) in
              let response = CloudKitArticleTypeCharacteristicsAndOptions.GetAllCharacteristicsAndOptions.Response(allCharacteristicsWithOptions: allCharacteristicsWithAllOptions)
              return result.fulfill(response)
            }).catch({ (error) in
              return result.reject(error)
            })
          }).catch({ (error) in
            return result.reject(error)
          })
        } else
          if let error = error {
            result.reject(error)
        }
      })
    }
  }
  
  
  
  private func getAllOptionsOfTheseArticleTypeCharacteristics(index: Int, characteristics: [CloudKitArticleTypeCharacteristic]) -> Promise<[CloudKitArticleTypeCharacteristic]> {
    return Promise { result in
      if index <= characteristics.count - 1 {
        let characteristic = characteristics[index]
        if characteristic.rawDataFromServer != nil {
          let characteristicCKReference = CKReference.init(record: characteristic.rawDataFromServer!, action: .none)
          let predicate = NSPredicate(format: "\(CloudKitKeys.ArticleTypeCharacteristicOption.optionOfThisArticleTypeCharacteristic.rawValue) == %@ ", characteristicCKReference)
          let sortDescriptor = NSSortDescriptor(key: CloudKitKeys.ArticleTypeCharacteristicOption.articleTypeCharacteristicOptionName.rawValue, ascending: true)
          self.fetchRecords(forType: CloudKitKeys.ArticleTypeCharacteristicOption.ArticleTypeCharacteristicOption.rawValue, sortDescriptors: [sortDescriptor], predicate: predicate).done({ (allOptionsOfTheCharacteristic) in
            let allOptions = allOptionsOfTheCharacteristic.map{self.getAllCloudKitOptionsFromServerData(from: $0)}
            characteristic.thisCharacteristicHasTheseOptions = allOptions
            self.getAllOptionsOfTheseArticleTypeCharacteristics(index: index + 1, characteristics: characteristics).done({ (allCharacteristicsObtainedAfterRecursive) in
              return result.fulfill(allCharacteristicsObtainedAfterRecursive)
            }).catch({ (error) in
              return result.reject(error)
            })
          }).catch({ (error) in
            return result.reject(error)
          })
        } else {
          let error = NSError.init(domain: "Una característica trae la información 'rawDataFromServer' como 'nil' ", code: -1, userInfo: nil)
          return result.reject(error)
        }
      } else {
        return result.fulfill(characteristics)
      }
    }
  }
  
  private func getCloudkitArticleTypeCharacteristicCloudKit(from record: CKRecord) -> CloudKitArticleTypeCharacteristic {
    let newArticleTypeCharacteristic = CloudKitArticleTypeCharacteristic()
    newArticleTypeCharacteristic.articleTypeCharacteristicName = record[CloudKitKeys.ArticleTypeCharacteristic.articleTypeCharacteristicName.rawValue] as! String
    newArticleTypeCharacteristic.articleTypeCharacteristicComment = record[CloudKitKeys.ArticleTypeCharacteristic.articleTypeCharacteristicComment.rawValue] as! String
    newArticleTypeCharacteristic.coreDataID = record[CloudKitKeys.ArticleTypeCharacteristic.coreDataID.rawValue] as! String
//    newArticleTypeCharacteristic.thisCharacteristicHasTheseOptions = WE HAVE TO FETCH ALL THE OPTIONS AND AFTER FETCHING CONVERT TO CLOUDKIT DATA TYPE AND SAVE IN THIS VARIABLE
    newArticleTypeCharacteristic.thisCharacteristicIsForThisArticleType = record[CloudKitKeys.ArticleTypeCharacteristic.thisCharacteristicIsForThisArticleType.rawValue] as! CKReference
    newArticleTypeCharacteristic.rawDataFromServer = record
    return newArticleTypeCharacteristic
  }
  
  
    
}

class CloudKitArticleTypeCharacteristic {
  
  var articleTypeCharacteristicComment: String = ""
  var articleTypeCharacteristicName: String = ""
  var coreDataID: String = ""
  var thisCharacteristicHasTheseOptions: [CloudKitArticleTypeCharacteristicOption]?
  var thisCharacteristicIsForThisArticleType: CKReference!
  var rawDataFromServer: CKRecord?
  
  init() {}
  
}
