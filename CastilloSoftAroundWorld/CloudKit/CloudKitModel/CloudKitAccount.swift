//
//  CloudKitAccount.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 09/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CloudKit
import PromiseKit

extension CloudKitModel {
    
    //MARK: - Accounts Section
    func getAllAccountsFromCloudKit() -> Promise<[CKRecord]> {
        return Promise { result in
            let predicate = NSPredicate(value: true)
            let query = CKQuery(recordType: CloudKitKeys.Account.Account.rawValue, predicate: predicate)
            self.selectedDatabase.perform(query, inZoneWith: nil) { (accounts, error) in
                if let accounts = accounts {
                    result.fulfill(accounts)
                } else if let error = error {
                    result.reject(error)
                }
            }
        }
    }
  
  func getAccountFromThisClient(client: CloudKitClient) -> Promise<CloudKitAccount>{
    return Promise { result in
      if client.hasOneAccount != nil {
        self.selectedDatabase.fetch(withRecordID: client.hasOneAccount!.recordID, completionHandler: { (account, error) in
          if let account = account {
            let cloudKitAccount = self.getCloudKitAccountFromServerData(recordFromCloudKit: account)
            result.fulfill(cloudKitAccount)
          } else
          if let error = error {
            print(error)
            result.reject(error)
          }
        })
      } else {
        let error = NSError.init(domain: "No se pudo obtener la cuenta de dicho cliente", code: -1, userInfo: nil)
        result.reject(error)
      }
    }
  }
  
  private func getCloudKitAccountFromServerData(recordFromCloudKit: CKRecord) -> CloudKitAccount {
    let newAccount = CloudKitAccount()
    newAccount.clientNameWhoIsOwner = recordFromCloudKit[CloudKitKeys.Account.clientNameWhoIsOwner.rawValue] as! String
    newAccount.clientOwner = recordFromCloudKit[CloudKitKeys.Account.clientOwner.rawValue] as? CKReference ?? nil
    newAccount.clientWarehouseNumber = (recordFromCloudKit[CloudKitKeys.Account.clientWarehouseNumber.rawValue] as? String) ?? ""
    newAccount.coreDataID = recordFromCloudKit[CloudKitKeys.Account.coreDataID.rawValue] as! String
    newAccount.finalDebt = recordFromCloudKit[CloudKitKeys.Account.finalDebt.rawValue] as! String
    newAccount.hasPayments = recordFromCloudKit[CloudKitKeys.Account.hasPayments.rawValue] as? [CKReference] ?? nil
    newAccount.thisAccountHasThisArticles = recordFromCloudKit[CloudKitKeys.Account.thisAccountHasThisArticles.rawValue] as? [CKReference] ?? nil
    newAccount.rawDataFromServer = recordFromCloudKit
    
    return newAccount
  }
  
  func add(this article: CloudKitArticle, toThisAccount: CloudKitAccount) -> Promise<[CKRecord]> {
    return Promise { result in
      let articleReference = CKReference.init(record: article.rawDataFromServer!, action: .none)
      let accountReference = CKReference.init(record: toThisAccount.rawDataFromServer!, action: .none)
      var arrayOfArticlesInAccount: [CKReference] = (toThisAccount.rawDataFromServer![CloudKitKeys.Account.thisAccountHasThisArticles.rawValue] as? [CKReference]) ?? [CKReference]()
      arrayOfArticlesInAccount.append(articleReference)
      
      toThisAccount.rawDataFromServer![CloudKitKeys.Account.thisAccountHasThisArticles.rawValue] = arrayOfArticlesInAccount as CKRecordValue
      article.rawDataFromServer![CloudKitKeys.Article.thisArticleIsAddedIntoThisAccount.rawValue] = accountReference as CKRecordValue
      article.rawDataFromServer![CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue] = Date.init() as CKRecordValue
      article.rawDataFromServer![CloudKitKeys.Article.isAddedInAnAccount.rawValue] = 1 as CKRecordValue
      
      let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: [toThisAccount.rawDataFromServer!, article.rawDataFromServer!], recordIDsToDelete: nil)
      operationToSaveSomeRecords.queuePriority = .veryHigh
      operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
        if let error = error {
          print("Error when updating: \(error)")
          result.reject(error)
        } else if let recordsChanged = recordsChanged, recordsChanged.count > 0 {
          result.fulfill(recordsChanged)
        } else {
          let error = NSError(domain: "No Se modificó ninguna cuenta", code: -1, userInfo: nil)
          result.reject(error)
        }
      }
      operationToSaveSomeRecords.completionBlock = { }
      self.selectedDatabase.add(operationToSaveSomeRecords)
    }
  }
  
  func remove(this article: CloudKitArticle, toThisAccount: CloudKitAccount) -> Promise<[CKRecord]> {
    return Promise { result in
      let articleReference = CKReference.init(record: article.rawDataFromServer!, action: .none)
      var arrayOfArticlesInAccount = toThisAccount.rawDataFromServer![CloudKitKeys.Account.thisAccountHasThisArticles.rawValue] as? [CKReference]
      if arrayOfArticlesInAccount != nil {
        if arrayOfArticlesInAccount!.count > 0 {
          for index in 0...arrayOfArticlesInAccount!.count - 1 {
            if arrayOfArticlesInAccount![index].recordID.recordName == articleReference.recordID.recordName {
              arrayOfArticlesInAccount!.remove(at: index)
              break
            }
          }
          toThisAccount.rawDataFromServer![CloudKitKeys.Account.thisAccountHasThisArticles.rawValue] = arrayOfArticlesInAccount! as CKRecordValue
          article.rawDataFromServer![CloudKitKeys.Article.thisArticleIsAddedIntoThisAccount.rawValue] = nil
          article.rawDataFromServer![CloudKitKeys.Article.purchaseDateOfThisArticle.rawValue] = nil
          article.rawDataFromServer![CloudKitKeys.Article.isAddedInAnAccount.rawValue] = 0 as CKRecordValue
          
        } else {
          let error = NSError.init(domain: "No hay artículos en dicha cuenta", code: -1, userInfo: nil)
          result.reject(error)
        }
      } else {
        let error = NSError.init(domain: "No hay artículos en dicha cuenta", code: -1, userInfo: nil)
        result.reject(error)
      }
      let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: [toThisAccount.rawDataFromServer!, article.rawDataFromServer!], recordIDsToDelete: nil)
      operationToSaveSomeRecords.queuePriority = .veryHigh
      operationToSaveSomeRecords.qualityOfService = QualityOfService.userInteractive
      operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
        print("modifyRecordsCompletionBlock was first")
        if let error = error {
          print("Error when updating: \(error)")
          result.reject(error)
        } else if let recordsChanged = recordsChanged, recordsChanged.count > 0 {
          result.fulfill(recordsChanged)
        } else {
          let error = NSError(domain: "No se modificó nada el remover artículo de cuenta", code: -1, userInfo: nil)
          result.reject(error)
        }
      }
      operationToSaveSomeRecords.completionBlock = { print("completionBlock was first") }
      self.selectedDatabase.add(operationToSaveSomeRecords)
    }
  }
  
  func createAndAdd(this quantityOfPayment: String, withThis comment: String, toThis account: CloudKitAccount) -> Promise<[CKRecord]> {
    return Promise { result in
      self.selectedDatabase.fetch(withRecordID: account.rawDataFromServer!.recordID, completionHandler: { (fetchedAccount, error) in
        if let fetchedAccount = fetchedAccount {
          let clientDataReference = fetchedAccount[CloudKitKeys.Account.clientOwner.rawValue] as! CKReference
          self.selectedDatabase.fetch(withRecordID: clientDataReference.recordID, completionHandler: { (fetchedClient, error) in
            if let fetchedClient = fetchedClient {
              let accountReference = CKReference(record: fetchedAccount, action: .none)
              let dateOfPayment = Date()
              let newPayment = CKRecord(recordType: CloudKitKeys.Payment.Payment.rawValue)
              newPayment[CloudKitKeys.Payment.coreDataID.rawValue] = "" as CKRecordValue
              newPayment[CloudKitKeys.Payment.ammountPaid.rawValue] = quantityOfPayment as CKRecordValue
              newPayment[CloudKitKeys.Payment.paymentComment.rawValue] = comment as CKRecordValue
              newPayment[CloudKitKeys.Payment.dayOfPayment.rawValue] = dateOfPayment as CKRecordValue
              newPayment[CloudKitKeys.Payment.thisPaymentIsAssociatedWithThisAccount.rawValue] = accountReference as CKRecordValue
              
              let newPaymentReference = CKReference(record: newPayment, action: .none)
              var paymentsInAccount: [CKReference] = (account.rawDataFromServer![CloudKitKeys.Account.hasPayments.rawValue] as? [CKReference]) ?? [CKReference]()
              paymentsInAccount.append(newPaymentReference)
              fetchedAccount[CloudKitKeys.Account.hasPayments.rawValue] = paymentsInAccount as CKRecordValue
              fetchedClient[CloudKitKeys.Client.dateOfLastPayment.rawValue] = dateOfPayment as CKRecordValue
              let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: [fetchedAccount, newPayment, fetchedClient], recordIDsToDelete: nil)
              operationToSaveSomeRecords.queuePriority = .veryHigh
              operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
                if let error = error {
                  print("Error when updating: \(error)")
                  return result.reject(error)
                } else if let recordsChanged = recordsChanged, recordsChanged.count > 0 {
                  return result.fulfill(recordsChanged)
                } else {
                  let error = NSError(domain: "No se modificó nada al agregar pago en cuenta", code: -1, userInfo: nil)
                  return result.reject(error)
                }
              }
              self.selectedDatabase.add(operationToSaveSomeRecords)
            } else
            if let error = error {
              return result.reject(error)
            }
          })
        } else
        if let error = error {
          return result.reject(error)
        }
      })
    }
  }
  
  func remove(this payment: CloudKitPayment, toThisAccount: CloudKitAccount) -> Promise<[CKRecord]> {
    return Promise { result in
      self.selectedDatabase.fetch(withRecordID: toThisAccount.rawDataFromServer!.recordID, completionHandler: { (fetchedAccount, error) in
        if let fetchedAccount = fetchedAccount {
          let clientReference = fetchedAccount[CloudKitKeys.Account.clientOwner.rawValue] as! CKReference
          var lastPayment: CKReference? = nil
          let paymentReference = CKReference.init(record: payment.rawDataFromServer!, action: .none)
          var arrayOfPaymentsInAccount = fetchedAccount[CloudKitKeys.Account.hasPayments.rawValue] as? [CKReference]
          if arrayOfPaymentsInAccount != nil {
            if arrayOfPaymentsInAccount!.count > 0 {
              for index in 0...arrayOfPaymentsInAccount!.count - 1 {
                if arrayOfPaymentsInAccount![index].recordID.recordName == paymentReference.recordID.recordName {
                  arrayOfPaymentsInAccount!.remove(at: index)
                  break
                }
              }
              lastPayment = arrayOfPaymentsInAccount!.last ?? nil
              fetchedAccount[CloudKitKeys.Account.hasPayments.rawValue] = arrayOfPaymentsInAccount! as CKRecordValue
              payment.rawDataFromServer![CloudKitKeys.Payment.thisPaymentIsAssociatedWithThisAccount.rawValue] = nil
              payment.rawDataFromServer![CloudKitKeys.Payment.dayOfPayment.rawValue] = nil
            } else {
              let error = NSError.init(domain: "No hay pagos en dicha cuenta", code: -1, userInfo: nil)
              result.reject(error)
            }
          } else {
            let error = NSError.init(domain: "No hay pagos en dicha cuenta", code: -1, userInfo: nil)
            result.reject(error)
          }
          let operationToSaveSomeRecords = CKModifyRecordsOperation.init(recordsToSave: [fetchedAccount], recordIDsToDelete: [payment.rawDataFromServer!.recordID])
          operationToSaveSomeRecords.queuePriority = .veryHigh
          operationToSaveSomeRecords.modifyRecordsCompletionBlock = { recordsChanged, recordsDeleted, error in
            if let error = error {
              print("Error when updating: \(error)")
              result.reject(error)
            } else if let recordsChanged = recordsChanged, recordsChanged.count > 0 {
              if lastPayment != nil {
                self.selectedDatabase.fetch(withRecordID: lastPayment!.recordID, completionHandler: { (fetchedLastPayment, error) in
                  if let error = error {
                    return result.reject(error)
                  } else
                  if let fetchedLastPayment = fetchedLastPayment {
                    let fetchedPaymentData = self.getCloudKitPaymentFromServerData(recordFromCloudKit: fetchedLastPayment)
                    self.selectedDatabase.fetch(withRecordID: clientReference.recordID, completionHandler: { (fetchedClient, error) in
                      if let error = error {
                        result.reject(error)
                      } else
                      if let fetchedClient = fetchedClient {
                        fetchedClient[CloudKitKeys.Client.dateOfLastPayment.rawValue] = fetchedPaymentData.dayOfPayment as CKRecordValue
                        self.selectedDatabase.save(fetchedClient, completionHandler: { (updatedClient, error) in
                          if let error = error {
                            return result.reject(error)
                          } else
                          if let updatedClient = updatedClient {
                            return result.fulfill([updatedClient])
                          }
                        })
                      }
                    })
                  }
                })
              } else {
                self.selectedDatabase.fetch(withRecordID: clientReference.recordID, completionHandler: { (fetchedClient, error) in
                  if let error = error {
                    return result.reject(error)
                  } else
                  if let fetchedClient = fetchedClient {
                    fetchedClient[CloudKitKeys.Client.dateOfLastPayment.rawValue] = nil
                    self.selectedDatabase.save(fetchedClient, completionHandler: { (updatedClient, error) in
                      if let error = error {
                        return result.reject(error)
                      } else
                      if let updatedClient = updatedClient {
                        return result.fulfill([updatedClient])
                      }
                    })
                  }
                })
              }
              //supposedly the next line will never occur
              result.fulfill(recordsChanged)
            } else {
              let error = NSError(domain: "No se modificó nada al remover pago de cuenta", code: -1, userInfo: nil)
              result.reject(error)
            }
          }
          operationToSaveSomeRecords.completionBlock = { }
          self.selectedDatabase.add(operationToSaveSomeRecords)
        } else
        if let error = error {
          return result.reject(error)
        }
      })
    }
  }
  
  func getAccountOfThisPayment(payment: CloudKitPayment) -> Promise<CloudKitAccount> {
    return Promise { result in
      if payment.thisPaymentIsAssociatedWithThisAccount != nil {
        self.selectedDatabase.fetch(withRecordID: payment.thisPaymentIsAssociatedWithThisAccount!.recordID, completionHandler: { (accountCKRecord, error) in
          if let error = error {
            return result.reject(error)
          } else
          if let accountCKRecord = accountCKRecord {
            let accountData = self.getCloudKitAccountFromServerData(recordFromCloudKit: accountCKRecord)
            return result.fulfill(accountData)
          }
        })
      } else {
        let error = NSError(domain: "Este pago no está asignada a ninguna cuenta", code: -1, userInfo: nil)
        return result.reject(error)
      }
    }
  }
  
  func getAccountWhichIsAddedThisArticle(articleData: CloudKitArticle) -> Promise<CloudKitAccount> {
    return Promise { result in
      if articleData.thisArticleIsAddedIntoThisAccount != nil {
        self.selectedDatabase.fetch(withRecordID: articleData.thisArticleIsAddedIntoThisAccount!.recordID, completionHandler: { (accountCKRecord, error) in
          if let error = error {
            return result.reject(error)
          } else
          if let accountCKRecord = accountCKRecord {
            let accountData = self.getCloudKitAccountFromServerData(recordFromCloudKit: accountCKRecord)
            return result.fulfill(accountData)
          }
        })
      } else {
        let error = NSError(domain: "Éste artículo no está añadido a ninguna cuenta", code: -1, userInfo: nil)
        return result.reject(error)
      }
    }
  }
  
}

class CloudKitAccount {
  
  var clientNameWhoIsOwner: String = ""
  var clientOwner: CKReference?
  var clientWarehouseNumber: String = ""
  var coreDataID: String = ""
  var finalDebt: String = ""
  var hasPayments: [CKReference]?
  var thisAccountHasThisArticles: [CKReference]?
  var rawDataFromServer: CKRecord?
  
  init() {}
  
}
