//
//  CloudKitArticleTypeCharacteristicOption.swift
//  CastilloSoftAroundWorld
//
//  Created by Israel on 10/04/18.
//  Copyright © 2018 IsraelGutierrez. All rights reserved.
//

import Foundation
import CloudKit
import PromiseKit

extension CloudKitModel {
    
    //MARK: - ArticleTypeCharacteristic
    func getAllArticleTypeCharacteristicOptionsFromCloudKit() -> Promise<[CKRecord]> {
        return Promise { result in
            let predicate = NSPredicate(value: true)
            let query = CKQuery(recordType: CloudKitKeys.ArticleTypeCharacteristicOption.ArticleTypeCharacteristicOption.rawValue, predicate: predicate)
            self.selectedDatabase.perform(query, inZoneWith: nil) { (articleTypesCharacteristicOptionArray, error) in
                if let articleTypesCharacteristicOptions = articleTypesCharacteristicOptionArray {
                    result.fulfill(articleTypesCharacteristicOptions)
                } else if let error = error {
                    result.reject(error)
                }
            }
        }
    }
  
  func getAllCloudKitOptionsFromServerData(from record: CKRecord) -> CloudKitArticleTypeCharacteristicOption {
    let newOption = CloudKitArticleTypeCharacteristicOption()
    newOption.articleTypeCharacteristicOptionName = record[CloudKitKeys.ArticleTypeCharacteristicOption.articleTypeCharacteristicOptionName.rawValue] as! String
    newOption.coreDataID = record[CloudKitKeys.ArticleTypeCharacteristicOption.coreDataID.rawValue] as! String
    newOption.optionOfThisArticleTypeCharacteristic = record[CloudKitKeys.ArticleTypeCharacteristicOption.optionOfThisArticleTypeCharacteristic.rawValue] as? CKReference
    newOption.rawDataFromServer = record
    return newOption
  }
  
}

class CloudKitArticleTypeCharacteristicOption {
  
  var articleTypeCharacteristicOptionName: String = ""
  var coreDataID: String = ""
  var optionOfThisArticleTypeCharacteristic: CKReference?
  var rawDataFromServer: CKRecord?
  
  init() {}
  
}

